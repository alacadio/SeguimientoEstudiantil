﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CapaNegocio;
using CapaEntidad;
using System.Web.Http.Results;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace WebApi.Controllers
{
    public class TraerCaracterizacionRespuestasController : ApiController
    {


        private Sto_TraerCaracterizacionRespuestas_NE STCRNE = null;

        private List<Sto_TraerCaracterizacionRespuestas> lista;

        public TraerCaracterizacionRespuestasController()
        {
            STCRNE = new Sto_TraerCaracterizacionRespuestas_NE();

        }

        // GET: api/TraerCaracterizacionRespuestas
        public IEnumerable<Sto_TraerCaracterizacionRespuestas> Get()
        {
            lista = STCRNE.Lista_TraerCaracterizacionRespuestas("DPTO_EXP", "");
            return lista;
        }


        // GET: api/TraerCaracterizacionRespuestas
        public string getAllCaracterizacion(string cod, string mod)
        {
            lista = STCRNE.Lista_TraerCaracterizacionRespuestas(cod, mod);
            return JsonConvert.SerializeObject(lista);
        }

        // GET: api/TraerCaracterizacionRespuestas/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/TraerCaracterizacionRespuestas
        public void Post([FromBody]string value)
        {

        }

        // PUT: api/TraerCaracterizacionRespuestas/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/TraerCaracterizacionRespuestas/5
        public void Delete(int id)
        {
        }
    }
}
