﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class Sto_TablaTemporal_Preguntas
    {


        private string codigoPrgunta;
        private string detallePregunta;
        private Boolean registrado;
        public Sto_TablaTemporal_Preguntas()
        {

        }
        public Sto_TablaTemporal_Preguntas(string codigoPrgunta, string detallePregunta, bool registrado)
        {
            this.codigoPrgunta = codigoPrgunta;
            this.detallePregunta = detallePregunta;
            this.registrado = registrado;
        }

        public string CodigoPrgunta
        {
            get
            {
                return codigoPrgunta;
            }

            set
            {
                codigoPrgunta = value;
            }
        }

        public string DetallePregunta
        {
            get
            {
                return detallePregunta;
            }

            set
            {
                detallePregunta = value;
            }
        }

        public bool Registrado
        {
            get
            {
                return registrado;
            }

            set
            {
                registrado = value;
            }
        }
    }
}
