﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class Sto_FuentesDatos
    {
        private int fuenteDato;
        private string nombre;
        private string descripcion;
        public Sto_FuentesDatos()
        {

        }
        public Sto_FuentesDatos(int fuenteDato)
        {
            this.fuenteDato = fuenteDato;
        }
        public Sto_FuentesDatos(int fuenteDato, string nombre, string descripcion)
        {
            this.fuenteDato = fuenteDato;
            this.nombre = nombre;
            this.descripcion = descripcion;
        }

        public int FuenteDato
        {
            get
            {
                return fuenteDato;
            }

            set
            {
                fuenteDato = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Descripcion
        {
            get
            {
                return descripcion;
            }

            set
            {
                descripcion = value;
            }
        }
    }
}
