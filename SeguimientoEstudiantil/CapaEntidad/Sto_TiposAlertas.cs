﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class Sto_TiposAlertas
    {
        private int TipoAlerta;
        private string Nombre;
        private int alertaBase;
        public Sto_TiposAlertas()
        {

        }
        public Sto_TiposAlertas(int tipoAlerta)
        {
            TipoAlerta = tipoAlerta;
        }
        public Sto_TiposAlertas(int tipoAlerta, string nombre, int alertaBase)
        {
            TipoAlerta = tipoAlerta;
            Nombre = nombre;
            this.alertaBase = alertaBase;
        }

        public int TipoAlerta1
        {
            get
            {
                return TipoAlerta;
            }

            set
            {
                TipoAlerta = value;
            }
        }

        public string Nombre1
        {
            get
            {
                return Nombre;
            }

            set
            {
                Nombre = value;
            }
        }

        public int AlertaBase
        {
            get
            {
                return alertaBase;
            }

            set
            {
                alertaBase = value;
            }
        }
    }
}
