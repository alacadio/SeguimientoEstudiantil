﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class Sto_AlertaBase
    {
        private int alertaBase;
        private string nombre;
        private string descripcion;
        private int fuenteDato;
        private Boolean usuarioConfigura;
        public Sto_AlertaBase()
        {

        }
        public Sto_AlertaBase(int alertaBase)
        {
            this.alertaBase = alertaBase;
        }
        public Sto_AlertaBase(int alertaBase, string nombre, string descripcion, int fuenteDato, bool usuarioConfigura)
        {
            this.alertaBase = alertaBase;
            this.nombre = nombre;
            this.descripcion = descripcion;
            this.fuenteDato = fuenteDato;
            this.usuarioConfigura = usuarioConfigura;
        }

        public int AlertaBase
        {
            get
            {
                return alertaBase;
            }

            set
            {
                alertaBase = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Descripcion
        {
            get
            {
                return descripcion;
            }

            set
            {
                descripcion = value;
            }
        }

        public int FuenteDato
        {
            get
            {
                return fuenteDato;
            }

            set
            {
                fuenteDato = value;
            }
        }

        public bool UsuarioConfigura
        {
            get
            {
                return usuarioConfigura;
            }

            set
            {
                usuarioConfigura = value;
            }
        }
    }
}
