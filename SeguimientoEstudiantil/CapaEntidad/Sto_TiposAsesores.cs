﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class Sto_TiposAsesores
    {
        private int sto_TipoAsesor;
        private string nombre;
        private string email;

        public Sto_TiposAsesores()
        {

        }
        public Sto_TiposAsesores(int sto_TipoAsesor)
        {
            this.sto_TipoAsesor = sto_TipoAsesor;
        }
        public Sto_TiposAsesores(int sto_TipoAsesor, string nombre, string email)
        {
            this.sto_TipoAsesor = sto_TipoAsesor;
            this.nombre = nombre;
            this.email = email;
        }

        public int Sto_TipoAsesor
        {
            get
            {
                return sto_TipoAsesor;
            }

            set
            {
                sto_TipoAsesor = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }

            set
            {
                email = value;
            }
        }
    }
}
