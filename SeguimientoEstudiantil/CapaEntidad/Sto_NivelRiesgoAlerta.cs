﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class Sto_NivelRiesgoAlerta
    {
        private int NivelRiesgoAlerta;
        private string Nombre;
        public Sto_NivelRiesgoAlerta()
        {

        }
        public Sto_NivelRiesgoAlerta(int nivelRiesgoAlerta)
        {
            NivelRiesgoAlerta = nivelRiesgoAlerta;
        }
        public Sto_NivelRiesgoAlerta(int nivelRiesgoAlerta, string nombre)
        {
            NivelRiesgoAlerta = nivelRiesgoAlerta;
            Nombre = nombre;
        }
        public int NivelRiesgoAlerta1
        {
            get
            {
                return NivelRiesgoAlerta;
            }

            set
            {
                NivelRiesgoAlerta = value;
            }
        }

        public string Nombre1
        {
            get
            {
                return Nombre;
            }

            set
            {
                Nombre = value;
            }
        }
    }
}
