﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class Sto_TipoAleta_NivelRiesgo
    {
        private long Id_tipoAlerta_NivelRiesgo;
        private int TipoAlerta;
        private decimal Min_Puntaje;
        private decimal Max_Puntaje;
        private string Referencia;
        private int NivelRiesgoAlerta;
        public Sto_TipoAleta_NivelRiesgo()
        {

        }
        public Sto_TipoAleta_NivelRiesgo(long id_tipoAlerta_NivelRiesgo)
        {
            Id_tipoAlerta_NivelRiesgo = id_tipoAlerta_NivelRiesgo;
        }
        public Sto_TipoAleta_NivelRiesgo(long id_tipoAlerta_NivelRiesgo, int tipoAlerta, decimal min_Puntaje, decimal max_Puntaje, string referencia, int nivelRiesgoAlerta)
        {
            Id_tipoAlerta_NivelRiesgo = id_tipoAlerta_NivelRiesgo;
            TipoAlerta = tipoAlerta;
            Min_Puntaje = min_Puntaje;
            Max_Puntaje = max_Puntaje;
            Referencia = referencia;
            NivelRiesgoAlerta = nivelRiesgoAlerta;
        }

        public long Id_tipoAlerta_NivelRiesgo1
        {
            get
            {
                return Id_tipoAlerta_NivelRiesgo;
            }

            set
            {
                Id_tipoAlerta_NivelRiesgo = value;
            }
        }

        public int TipoAlerta1
        {
            get
            {
                return TipoAlerta;
            }

            set
            {
                TipoAlerta = value;
            }
        }

        public decimal Min_Puntaje1
        {
            get
            {
                return Min_Puntaje;
            }

            set
            {
                Min_Puntaje = value;
            }
        }

        public decimal Max_Puntaje1
        {
            get
            {
                return Max_Puntaje;
            }

            set
            {
                Max_Puntaje = value;
            }
        }

        public string Referencia1
        {
            get
            {
                return Referencia;
            }

            set
            {
                Referencia = value;
            }
        }

        public int NivelRiesgoAlerta1
        {
            get
            {
                return NivelRiesgoAlerta;
            }

            set
            {
                NivelRiesgoAlerta = value;
            }
        }
    }
}
