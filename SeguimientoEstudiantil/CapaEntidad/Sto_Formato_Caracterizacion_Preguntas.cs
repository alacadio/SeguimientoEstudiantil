﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class Sto_Formato_Caracterizacion_Preguntas
    {
        private string CodigoInterno;
        private string DetallePregunta;
        private string TipoPreguntaRespuesta;
        private Boolean MultiValuado;
        public Sto_Formato_Caracterizacion_Preguntas()
        {
            CodigoInterno = string.Empty;
            DetallePregunta = string.Empty;
            TipoPreguntaRespuesta = string.Empty;
            MultiValuado = false;
        }

        public Sto_Formato_Caracterizacion_Preguntas(string codigoInterno, string detallePregunta, string tipoPreguntaRespuesta, bool multiValuado)
        {
            CodigoInterno = codigoInterno;
            DetallePregunta = detallePregunta;
            TipoPreguntaRespuesta = tipoPreguntaRespuesta;
            MultiValuado = multiValuado;
        }

        public string CodigoInterno1
        {
            get
            {
                return CodigoInterno;
            }

            set
            {
                CodigoInterno = value;
            }
        }

        public string DetallePregunta1
        {
            get
            {
                return DetallePregunta;
            }

            set
            {
                DetallePregunta = value;
            }
        }

        public string TipoPreguntaRespuesta1
        {
            get
            {
                return TipoPreguntaRespuesta;
            }

            set
            {
                TipoPreguntaRespuesta = value;
            }
        }

        public bool MultiValuado1
        {
            get
            {
                return MultiValuado;
            }

            set
            {
                MultiValuado = value;
            }
        }
    }
}
