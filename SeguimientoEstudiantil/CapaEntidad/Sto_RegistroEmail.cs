﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class Sto_RegistroEmail
    {
        private int id_RegistroEmail;
        private DateTime Fec_Envio;
        private int sto_TipoAsesor;
        private string estudiante;
        private int sto_TipoAcciones;

        public Sto_RegistroEmail()
        {

        }
        public Sto_RegistroEmail(int id_RegistroEmail)
        {
            this.id_RegistroEmail = id_RegistroEmail;
        }
        public Sto_RegistroEmail(int id_RegistroEmail, DateTime fec_Envio, int sto_TipoAsesor, string estudiante, int sto_TipoAcciones)
        {
            this.id_RegistroEmail = id_RegistroEmail;
            Fec_Envio = fec_Envio;
            this.sto_TipoAsesor = sto_TipoAsesor;
            this.estudiante = estudiante;
            this.sto_TipoAcciones = sto_TipoAcciones;
        }

        public int Id_RegistroEmail
        {
            get
            {
                return id_RegistroEmail;
            }

            set
            {
                id_RegistroEmail = value;
            }
        }

        public DateTime Fec_Envio1
        {
            get
            {
                return Fec_Envio;
            }

            set
            {
                Fec_Envio = value;
            }
        }

        public int Sto_TipoAsesor
        {
            get
            {
                return sto_TipoAsesor;
            }

            set
            {
                sto_TipoAsesor = value;
            }
        }

        public string Estudiante
        {
            get
            {
                return estudiante;
            }

            set
            {
                estudiante = value;
            }
        }

        public int Sto_TipoAcciones
        {
            get
            {
                return sto_TipoAcciones;
            }

            set
            {
                sto_TipoAcciones = value;
            }
        }
    }
}
