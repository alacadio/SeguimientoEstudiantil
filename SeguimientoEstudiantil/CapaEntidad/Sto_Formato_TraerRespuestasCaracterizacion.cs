﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class Sto_Formato_TraerRespuestasCaracterizacion
    {
        private string CodigoInterno;
        private string DetallePregunta;
        private char TipoPreguntaRespuesta;
        private string formatoDato;
        private string Respuesta;
        public Sto_Formato_TraerRespuestasCaracterizacion()
        {

        }

        public Sto_Formato_TraerRespuestasCaracterizacion(string codigoInterno)
        {
            CodigoInterno = codigoInterno;
        }
        public Sto_Formato_TraerRespuestasCaracterizacion(string codigoInterno, string detallePregunta, char tipoPreguntaRespuesta, string formatoDato, string respuesta)
        {
            CodigoInterno = codigoInterno;
            DetallePregunta = detallePregunta;
            TipoPreguntaRespuesta = tipoPreguntaRespuesta;
            this.formatoDato = formatoDato;
            Respuesta = respuesta;
        }

        public string CodigoInterno1
        {
            get
            {
                return CodigoInterno;
            }

            set
            {
                CodigoInterno = value;
            }
        }

        public string DetallePregunta1
        {
            get
            {
                return DetallePregunta;
            }

            set
            {
                DetallePregunta = value;
            }
        }

        public char TipoPreguntaRespuesta1
        {
            get
            {
                return TipoPreguntaRespuesta;
            }

            set
            {
                TipoPreguntaRespuesta = value;
            }
        }

        public string FormatoDato
        {
            get
            {
                return formatoDato;
            }

            set
            {
                formatoDato = value;
            }
        }

        public string Respuesta1
        {
            get
            {
                return Respuesta;
            }

            set
            {
                Respuesta = value;
            }
        }
    }
}
