﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class Sto_TipoAsesor_Usuario
    {
        private long id_TipoAsesor_Usuario;
        private string usuario;
        private int tipoAsesor;

        public Sto_TipoAsesor_Usuario()
        {

        }

        public Sto_TipoAsesor_Usuario(long id_TipoAsesor_Usuario)
        {
            this.id_TipoAsesor_Usuario = id_TipoAsesor_Usuario;
        }
        public Sto_TipoAsesor_Usuario(long id_TipoAsesor_Usuario, string usuario, int tipoAsesor)
        {
            this.id_TipoAsesor_Usuario = id_TipoAsesor_Usuario;
            this.usuario = usuario;
            this.tipoAsesor = tipoAsesor;
        }

        public long Id_TipoAsesor_Usuario
        {
            get
            {
                return id_TipoAsesor_Usuario;
            }

            set
            {
                id_TipoAsesor_Usuario = value;
            }
        }

        public string Usuario
        {
            get
            {
                return usuario;
            }

            set
            {
                usuario = value;
            }
        }

        public int TipoAsesor
        {
            get
            {
                return tipoAsesor;
            }

            set
            {
                tipoAsesor = value;
            }
        }
    }
}
