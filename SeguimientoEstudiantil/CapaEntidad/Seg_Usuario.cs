﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class Seg_Usuario
    {

        private string usuario;
        private string nombre;
        private Boolean activo;
        public Seg_Usuario()
        {

        }
        public Seg_Usuario(string usuario)
        {
            this.usuario = usuario;
        }
        public Seg_Usuario(string usuario, string nombre, bool activo)
        {
            this.usuario = usuario;
            this.nombre = nombre;
            this.activo = activo;
        }

        public string Usuario
        {
            get
            {
                return usuario;
            }

            set
            {
                usuario = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public bool Activo
        {
            get
            {
                return activo;
            }

            set
            {
                activo = value;
            }
        }
    }
}
