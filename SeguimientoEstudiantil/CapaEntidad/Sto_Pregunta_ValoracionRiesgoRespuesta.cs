﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class Sto_Pregunta_ValoracionRiesgoRespuesta 
    {
        private long Id_Pregunta_Valoracion;
        private long Id_TipoAlerta_Pregunta;
        private string Respuesta;
        private int ValorRiesgo;
        private string DescripcionRespuesta;
        public Sto_Pregunta_ValoracionRiesgoRespuesta()
        {

        }
        public Sto_Pregunta_ValoracionRiesgoRespuesta(long id_Pregunta_Valoracion)
        {
            Id_Pregunta_Valoracion = id_Pregunta_Valoracion;
        }
        public Sto_Pregunta_ValoracionRiesgoRespuesta(long id_Pregunta_Valoracion, long id_TipoAlerta_Pregunta, string respuesta, int valorRiesgo, string descripcionRespuesta)
        {
            Id_Pregunta_Valoracion = id_Pregunta_Valoracion;
            Id_TipoAlerta_Pregunta = id_TipoAlerta_Pregunta;
            Respuesta = respuesta;
            ValorRiesgo = valorRiesgo;
            DescripcionRespuesta = descripcionRespuesta;
        }

        public long Id_Pregunta_Valoracion1
        {
            get
            {
                return Id_Pregunta_Valoracion;
            }

            set
            {
                Id_Pregunta_Valoracion = value;
            }
        }

        public long Id_TipoAlerta_Pregunta1
        {
            get
            {
                return Id_TipoAlerta_Pregunta;
            }

            set
            {
                Id_TipoAlerta_Pregunta = value;
            }
        }

        public string Respuesta1
        {
            get
            {
                return Respuesta;
            }

            set
            {
                Respuesta = value;
            }
        }

        public int ValorRiesgo1
        {
            get
            {
                return ValorRiesgo;
            }

            set
            {
                ValorRiesgo = value;
            }
        }

        public string DescripcionRespuesta1
        {
            get
            {
                return DescripcionRespuesta;
            }

            set
            {
                DescripcionRespuesta = value;
            }
        }
    }
}
