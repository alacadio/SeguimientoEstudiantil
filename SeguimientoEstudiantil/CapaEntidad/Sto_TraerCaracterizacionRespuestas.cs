﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class Sto_TraerCaracterizacionRespuestas
    {
        
        private string CodigoInterno;
        private string respuesta;
        private string detalles;
        public Sto_TraerCaracterizacionRespuestas()
        {

        }
        public Sto_TraerCaracterizacionRespuestas(string codigoInterno)
        {
            CodigoInterno = codigoInterno;
        }
        public Sto_TraerCaracterizacionRespuestas(string codigoInterno, string respuesta, string detalles)
        {
            CodigoInterno = codigoInterno;
            this.respuesta = respuesta;
            this.detalles = detalles;
        }

        public string CodigoInterno1
        {
            get
            {
                return CodigoInterno;
            }

            set
            {
                CodigoInterno = value;
            }
        }

        public string Respuesta
        {
            get
            {
                return respuesta;
            }

            set
            {
                respuesta = value;
            }
        }

        public string Detalles
        {
            get
            {
                return detalles;
            }

            set
            {
                detalles = value;
            }
        }
    }
}
