﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class Sto_Accion_TipoAlerta_NivelRiesgo
    {
        private int ID_Accion_TipoAlerta_NivelRiesgo;
        private int ID_Accion;
        private long ID_TipoAlerta_NivelRiesgo;
        private string Referencias;
        public Sto_Accion_TipoAlerta_NivelRiesgo()
        {

        }
        public Sto_Accion_TipoAlerta_NivelRiesgo(int iD_Accion_TipoAlerta_NivelRiesgo)
        {
            ID_Accion_TipoAlerta_NivelRiesgo = iD_Accion_TipoAlerta_NivelRiesgo;
        }
        public Sto_Accion_TipoAlerta_NivelRiesgo(int iD_Accion_TipoAlerta_NivelRiesgo, int iD_Accion, long iD_TipoAlerta_NivelRiesgo, string referencias)
        {
            ID_Accion_TipoAlerta_NivelRiesgo = iD_Accion_TipoAlerta_NivelRiesgo;
            ID_Accion = iD_Accion;
            ID_TipoAlerta_NivelRiesgo = iD_TipoAlerta_NivelRiesgo;
            Referencias = referencias;
        }

        public int ID_Accion_TipoAlerta_NivelRiesgo1
        {
            get
            {
                return ID_Accion_TipoAlerta_NivelRiesgo;
            }

            set
            {
                ID_Accion_TipoAlerta_NivelRiesgo = value;
            }
        }

        public int ID_Accion1
        {
            get
            {
                return ID_Accion;
            }

            set
            {
                ID_Accion = value;
            }
        }

        public long ID_TipoAlerta_NivelRiesgo1
        {
            get
            {
                return ID_TipoAlerta_NivelRiesgo;
            }

            set
            {
                ID_TipoAlerta_NivelRiesgo = value;
            }
        }

        public string Referencias1
        {
            get
            {
                return Referencias;
            }

            set
            {
                Referencias = value;
            }
        }
    }
}
