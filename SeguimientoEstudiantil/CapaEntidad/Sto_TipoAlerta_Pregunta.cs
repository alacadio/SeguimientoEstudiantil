﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class Sto_TipoAlerta_Pregunta
    {
        private long Id_TipoAlerta_Preguntas;
        private int TipoAlerta;
        private string CodigoPRegunta;
        private string DescripcionPRegunta;

        public Sto_TipoAlerta_Pregunta()
        {

        }
        public Sto_TipoAlerta_Pregunta(long id_TipoAlerta_Preguntas)
        {
            Id_TipoAlerta_Preguntas = id_TipoAlerta_Preguntas;
        }

        public Sto_TipoAlerta_Pregunta(long id_TipoAlerta_Preguntas, int tipoAlerta, string codigoPRegunta, string descripcionPRegunta)
        {
            Id_TipoAlerta_Preguntas = id_TipoAlerta_Preguntas;
            TipoAlerta = tipoAlerta;
            CodigoPRegunta = codigoPRegunta;
            DescripcionPRegunta = descripcionPRegunta;
        }

        public Sto_TipoAlerta_Pregunta(int tipoAlerta, string codigoPRegunta, string descripcionPRegunta)
        {
            TipoAlerta = tipoAlerta;
            CodigoPRegunta = codigoPRegunta;
            DescripcionPRegunta = descripcionPRegunta;
        }

        public long Id_TipoAlerta_Preguntas1
        {
            get
            {
                return Id_TipoAlerta_Preguntas;
            }

            set
            {
                Id_TipoAlerta_Preguntas = value;
            }
        }

        public int TipoAlerta1
        {
            get
            {
                return TipoAlerta;
            }

            set
            {
                TipoAlerta = value;
            }
        }

        public string CodigoPRegunta1
        {
            get
            {
                return CodigoPRegunta;
            }

            set
            {
                CodigoPRegunta = value;
            }
        }

        public string DescripcionPRegunta1
        {
            get
            {
                return DescripcionPRegunta;
            }

            set
            {
                DescripcionPRegunta = value;
            }
        }
    }
}
