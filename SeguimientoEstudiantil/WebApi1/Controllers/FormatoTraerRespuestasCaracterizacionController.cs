﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CapaEntidad;
using CapaNegocio;

namespace WebApi1.Controllers
{
    public class FormatoTraerRespuestasCaracterizacionController : ApiController
    {
        public Sto_Formato_TraerRespuestasCaracterizacion_NE SFTRCNE = null;

        public FormatoTraerRespuestasCaracterizacionController()
        {
            SFTRCNE = new Sto_Formato_TraerRespuestasCaracterizacion_NE();
        }

        public List<Sto_Formato_TraerRespuestasCaracterizacion> getAllTraerRespuestas()
        {
            return SFTRCNE.lista();
        }
    }
}
