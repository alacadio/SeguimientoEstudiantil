﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CapaEntidad;
using CapaNegocio;
using Newtonsoft.Json;

namespace WebApi1.Controllers
{
    public class TraerCaracterizacionRespuestasController : ApiController
    {
        Sto_TraerCaracterizacionRespuestas_NE STCRNE = null;
        private List<Sto_TraerCaracterizacionRespuestas> lista;

        public TraerCaracterizacionRespuestasController()
        {
            STCRNE = new Sto_TraerCaracterizacionRespuestas_NE();

        }
        /*
        public json getAllCaracterizacion(string cod, string mod)
        {
            lista = STCRNE.Lista_TraerCaracterizacionRespuestas(cod, mod);
            return JsonConvert.SerializeObject(lista);
        }*/
        
        public List<Sto_TraerCaracterizacionRespuestas> getAllCaracterizacion(string cod,string mod)
        {
            return STCRNE.Lista_TraerCaracterizacionRespuestas(cod,mod);
        }
    }
}
