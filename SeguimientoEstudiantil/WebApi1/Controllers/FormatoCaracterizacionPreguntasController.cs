﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CapaEntidad;
using CapaNegocio;

namespace WebApi1.Controllers
{
    public class FormatoCaracterizacionPreguntasController : ApiController
    {

        private Sto_Formato_Caracterizacion_Preguntas_NE SFCPNE = new Sto_Formato_Caracterizacion_Preguntas_NE();

        public FormatoCaracterizacionPreguntasController()
        {
            SFCPNE = new Sto_Formato_Caracterizacion_Preguntas_NE();
        }


        public List<Sto_Formato_Caracterizacion_Preguntas> getAllPreguntasCaracterizacion()
        {
            return SFCPNE.listar();
        }

        public string getPregunta(string id)
        {
            return SFCPNE.getPregunta(id);
        }

    }
}
