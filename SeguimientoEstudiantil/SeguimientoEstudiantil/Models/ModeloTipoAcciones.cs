﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CapaEntidad;
using CapaNegocio;

namespace SeguimientoEstudiantil.Models
{
    public class ModeloTipoAcciones
    {

        public List<Sto_TipoAcciones> lista { get; set; }

        public Sto_TipoAcciones tipoAcciones { get; set; }

        public SelectList ListarTipoAccionesComboBox { get; set; }

        public ModeloTipoAcciones()
        {

            tipoAcciones = new Sto_TipoAcciones();

            lista = new List<Sto_TipoAcciones>();
        }

        public string MiTipoAccion(int id)
        {
            string rpta = "";

            Sto_TipoAccionesNE STANE = new Sto_TipoAccionesNE();

            tipoAcciones = STANE.getOne(id);

            rpta = tipoAcciones.Accion1;

            return rpta;
        }
    }
}