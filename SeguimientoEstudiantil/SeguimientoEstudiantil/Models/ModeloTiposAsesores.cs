﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CapaEntidad;
using CapaNegocio;

namespace SeguimientoEstudiantil.Models
{
    public class ModeloTiposAsesores
    {

        public List<Sto_TiposAsesores> listaTipoAsesores { get; set; }

        public SelectList listaComboBoxTipoAsesores { get; set; }

        public Sto_TiposAsesores TipoAsesores { set; get; }

        public ModeloTiposAsesores()
        {
            listaTipoAsesores = new List<Sto_TiposAsesores>();

            TipoAsesores = new Sto_TiposAsesores();
        }

        public string TipoAsesor(int id)
        {

            Sto_TiposAsesores_NE st = new Sto_TiposAsesores_NE();

            string rpta = "";

            TipoAsesores = st.TiposAsesores(id);

            rpta = TipoAsesores.Nombre;

            return rpta;
        }

        public string TipoAsesorEmail(int id)
        {

            Sto_TiposAsesores_NE st = new Sto_TiposAsesores_NE();

            string rpta = "";

            TipoAsesores = st.TiposAsesores(id);

            rpta = TipoAsesores.Email;

            return rpta;
        }

        public int TipoAsesorId(int id)
        {

            Sto_TiposAsesores_NE st = new Sto_TiposAsesores_NE();

            int rpta = 0;

            TipoAsesores = st.TiposAsesores(id);

            rpta = TipoAsesores.Sto_TipoAsesor;

            return rpta;
        }
    }
}