﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CapaEntidad;

namespace SeguimientoEstudiantil.Models
{
    public class ModeloUsuario
    {
        public Seg_Usuario Usuario { get; set; }

        public List<Seg_Usuario> ListaUsuario { get; set; }

        public SelectList ListaComboBoxUsuario { get; set; }

        public ModeloUsuario()
        {
            Usuario = new Seg_Usuario();

            ListaUsuario = new List<Seg_Usuario>();
        }
    }
}