﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SeguimientoEstudiantil.Models
{
    /// <summary>
    /// Modelo maestro que inicializa todos los modelos del sistema
    /// </summary>
    public class ModeloMaestro
    {
        public ModeloTipoAcciones Sto_TipoAcciones { get; set; }
        public ModeloTiposAlertas Sto_TipoAlertas { get; set; }
        public ModeloTipoAlerta_NivelRiesgo Sto_TipoAlerta_NivelRiesgo { get; set; }
        public ModeloNivelRiesgoAlerta Sto_NivelRiesgoAlerta { get; set; }
        public ModeloAccionTipoAlertaNivelRiesgo Sto_Accion_TipoAlerta_NivelRiesgo { get; set; }
        public ModeloAlertasBases Sto_AlertasBases { get; set; }
        public ModeloTipoAlertaPreguntas Sto_TipoAlertas_Preguntas { get; set; }
        public ModeloPreguntaValoracionRiesgo Sto_Respuestas { get; set; }
        public ModelPreguntasApi PreguntasApi { get; set; }
        public ModeloTiposAsesores Sto_TipoAsesores { get; set; }
        public ModeloTipoAsesorUsuario Sto_TipoAsesores_Usuario { get; set; }
        public ModeloUsuario Seg_Usuario { get; set; }

        public ModeloMaestro()
        {
            Sto_TipoAcciones = new ModeloTipoAcciones();

            Sto_TipoAlertas = new ModeloTiposAlertas();

            Sto_TipoAlerta_NivelRiesgo = new ModeloTipoAlerta_NivelRiesgo();

            Sto_NivelRiesgoAlerta = new ModeloNivelRiesgoAlerta();

            Sto_Accion_TipoAlerta_NivelRiesgo = new ModeloAccionTipoAlertaNivelRiesgo();

            Sto_AlertasBases = new ModeloAlertasBases();

            Sto_TipoAlertas_Preguntas = new ModeloTipoAlertaPreguntas();

            Sto_Respuestas = new ModeloPreguntaValoracionRiesgo();

            PreguntasApi = new ModelPreguntasApi();

            Sto_TipoAsesores = new ModeloTiposAsesores();

            Sto_TipoAsesores_Usuario = new ModeloTipoAsesorUsuario();

            Seg_Usuario = new ModeloUsuario();
        }
    }
}