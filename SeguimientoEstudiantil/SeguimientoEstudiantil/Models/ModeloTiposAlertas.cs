﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CapaEntidad;
using CapaNegocio;

namespace SeguimientoEstudiantil.Models
{
    public class ModeloTiposAlertas
    {
        public List<Sto_TiposAlertas> listaTipoAlertas { get; set; }

        public Sto_TiposAlertas TiposAlertas { get; set; }

        public SelectList ListarTiposAlertasComboBox { get; set; }

        public ModeloTiposAlertas()
        {
            listaTipoAlertas = new List<Sto_TiposAlertas>();

            TiposAlertas = new Sto_TiposAlertas();
        }

        public string NombreTipoAlerta(int id)
        {
            string nombre = "";

            Sto_TiposAlertas_NE STANE = new Sto_TiposAlertas_NE();

            TiposAlertas = STANE.getOne(id);

            nombre = TiposAlertas.Nombre1;

            return nombre;
        }
    }
}