﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CapaEntidad;
using CapaNegocio;
namespace SeguimientoEstudiantil.Models
{
    public class ModeloPreguntaValoracionRiesgo
    {
        public List<Sto_Pregunta_ValoracionRiesgoRespuesta> listaRespuestas { get; set; }
        public Sto_Pregunta_ValoracionRiesgoRespuesta Respuesta { get; set; }


        public ModeloPreguntaValoracionRiesgo()
        {
            listaRespuestas = new List<Sto_Pregunta_ValoracionRiesgoRespuesta>();

            Respuesta = new Sto_Pregunta_ValoracionRiesgoRespuesta();
        }
    }
}