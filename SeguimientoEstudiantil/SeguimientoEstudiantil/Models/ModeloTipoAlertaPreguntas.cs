﻿using System.Collections.Generic;
using System.Web.Mvc;
using CapaEntidad;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using CapaNegocio;

namespace SeguimientoEstudiantil.Models
{
    public class ModeloTipoAlertaPreguntas
    {
        public SelectList ListarComboBoxBaseDatos { get; set; }
        public SelectList ListarComboBoxAPI { get; set; }
        public List<Sto_TipoAlerta_Pregunta> listarTipoAlerta_Pregunta { get; set; }
        public Sto_TipoAlerta_Pregunta TipoAlerta_Pregunta { get; set; }

        public ModeloTipoAlertaPreguntas()
        {
            listarTipoAlerta_Pregunta = new List<Sto_TipoAlerta_Pregunta>();

            TipoAlerta_Pregunta = new Sto_TipoAlerta_Pregunta();
        }
        
        public Sto_TipoAlerta_Pregunta lol(string id)
        {
            Sto_TipoAlerta_Pregunta_NE STAPNE = new Sto_TipoAlerta_Pregunta_NE();

            TipoAlerta_Pregunta = new Sto_TipoAlerta_Pregunta();

            TipoAlerta_Pregunta = STAPNE.Listar_Pregunta_COD_Preg(id);

            return TipoAlerta_Pregunta;
        }



        public int preguntaRegistrada(int tipo, string preg)
        {
            int rpta = 0;

            Sto_TipoAlerta_Pregunta_NE STAPNE = new Sto_TipoAlerta_Pregunta_NE();

            rpta = STAPNE.getObtener(tipo, preg);

            return rpta;
        }

    }
}