﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CapaEntidad;
using CapaNegocio;

namespace SeguimientoEstudiantil.Models
{
    public class ModeloAccionTipoAlertaNivelRiesgo
    {
        public List<Sto_Accion_TipoAlerta_NivelRiesgo> ListarAccion_TipoAlerta_NivelRiesgo { get; set; }

        public Sto_Accion_TipoAlerta_NivelRiesgo Accion_TipoAlerta { get; set; }

        public ModeloAccionTipoAlertaNivelRiesgo()
        {
            ListarAccion_TipoAlerta_NivelRiesgo = new List<Sto_Accion_TipoAlerta_NivelRiesgo>();

            Accion_TipoAlerta = new Sto_Accion_TipoAlerta_NivelRiesgo();
        }

        public int IDTipoAcciones(int id)
        {
            int rpta = 0;

            Sto_Accion_TipoAlerta_NivelRiesgo_NE SATANRNE = new Sto_Accion_TipoAlerta_NivelRiesgo_NE();

            Accion_TipoAlerta = SATANRNE.getOne(id);

            rpta = Accion_TipoAlerta.ID_Accion1;

            return rpta;
        }
    }
}