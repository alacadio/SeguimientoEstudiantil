﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CapaEntidad;
using CapaNegocio;

namespace SeguimientoEstudiantil.Models
{
    public class ModeloAlertasBases
    {
        public List<Sto_AlertaBase> ListaAlertasBAses { get; set; }

        public Sto_AlertaBase AlertaBase { get; set; }

        public SelectList ListaAlertaBaseComboBox { get; set; }

        public ModeloAlertasBases()
        {
            ListaAlertasBAses = new List<Sto_AlertaBase>();

            AlertaBase = new Sto_AlertaBase();
        }


        public string NombreAlertaBase(int id)
        {
            string rpta = "";

            Sto_AlertaBase_NE STABNE = new Sto_AlertaBase_NE();

            AlertaBase = STABNE.getOneAlertasBAse(id);

            rpta = AlertaBase.Nombre;

            return rpta;
        }

    }
}