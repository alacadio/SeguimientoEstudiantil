﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CapaEntidad;
using CapaNegocio;

namespace SeguimientoEstudiantil.Models
{
    public class ModeloNivelRiesgoAlerta
    {
        public List<Sto_NivelRiesgoAlerta> listaNivelRiesgoAlerta { get; set; }

        public Sto_NivelRiesgoAlerta NivelRiesgoAlerta { get; set; }

        public SelectList ListaNivelRiesgoAlertaComboBox { get; set; }

        public ModeloNivelRiesgoAlerta()
        {
            listaNivelRiesgoAlerta = new List<Sto_NivelRiesgoAlerta>();

            NivelRiesgoAlerta = new Sto_NivelRiesgoAlerta();
        }

        public string NombreNivelRiesgo(int id)
        {
            string nombre = "";

            Sto_NivelRiesgoAlerta_NE SNRANE = new Sto_NivelRiesgoAlerta_NE();

            NivelRiesgoAlerta = SNRANE.getOneNivelRiesgoAlerta(id);

            nombre = NivelRiesgoAlerta.Nombre1;

            return nombre;
        }
    }
}