﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CapaEntidad;

namespace SeguimientoEstudiantil.Models
{
    public class ModeloTipoAsesorUsuario
    {

        public List<Sto_TipoAsesor_Usuario> listaTipoAsesorUsuario { get; set; }

        public Sto_TipoAsesor_Usuario TipoAsesorUsuario { get; set; }

        public SelectList ListaComboBoxTipoAsesorUSuario { get; set; }

        public ModeloTipoAsesorUsuario()
        {
            listaTipoAsesorUsuario = new List<Sto_TipoAsesor_Usuario>();

            TipoAsesorUsuario = new Sto_TipoAsesor_Usuario();
        }
    }
}