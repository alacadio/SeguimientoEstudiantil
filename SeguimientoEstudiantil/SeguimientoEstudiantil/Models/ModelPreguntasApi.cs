﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CapaEntidad;
namespace SeguimientoEstudiantil.Models
{
    public class ModelPreguntasApi
    {
        public Sto_Formato_Caracterizacion_Preguntas pregunta { set; get; }

        public List<Sto_Formato_Caracterizacion_Preguntas> listaPregutas { get; set; }

        public ModelPreguntasApi()
        {
            pregunta = new Sto_Formato_Caracterizacion_Preguntas();

            listaPregutas = new List<Sto_Formato_Caracterizacion_Preguntas>();
        }
    }
}