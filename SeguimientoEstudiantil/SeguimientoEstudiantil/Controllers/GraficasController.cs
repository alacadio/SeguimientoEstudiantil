﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SeguimientoEstudiantil.Controllers
{
    public class GraficasController : Controller
    {
        // GET: Graficas
        public ActionResult BarrasEstudiantesGenero()
        {
            //Esta
            return PartialView();
        }

        public ActionResult PieChartEstudiantesProgama()
        {
            //Esta
            return PartialView();
        }

        public ActionResult LineasRegistroHistorico()
        {
            return PartialView();
        }

        public ActionResult PolarNumeroAlertaProgramas()
        {
            //Esta
            return PartialView();
        }
    }
}