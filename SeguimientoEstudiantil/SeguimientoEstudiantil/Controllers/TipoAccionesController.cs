﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CapaEntidad;
using CapaNegocio;
using SeguimientoEstudiantil.Models;

namespace SeguimientoEstudiantil.Controllers
{
    public class TipoAccionesController : Controller
    {
        // GET: TipoAcciones
        public ActionResult Index()
        {
            Session["Estado"] = 4;
            ModeloTipoAcciones mod = new ModeloTipoAcciones();
            Sto_TipoAccionesNE STA = new Sto_TipoAccionesNE();
            mod.lista = STA.ListarTipoAcciones();
            return View(mod);
        }
        ///////////////////////////////
        #region CREATE TIPO ACCION
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(ModeloMaestro mod)
        {
            Sto_TipoAccionesNE STANE = new Sto_TipoAccionesNE();
            int rpta = STANE.CrearTipoAccion(mod.Sto_TipoAcciones.tipoAcciones);
            if(rpta > 0)
            {
                return RedirectToAction("Index");
            }
            return View();
        }
        #endregion
        ///////////////////////////////
        #region DELETE
        public ActionResult Delete(int id)
        {
            Sto_TipoAccionesNE STANE = new Sto_TipoAccionesNE();
            int rpta = STANE.EliminarTipoAccion(id);
            if(rpta > 0)
            {
                return RedirectToAction("Index");
            }
            return View();
        }
        #endregion
        //////////////////////////////
        #region ACTUALIZAR
        public ActionResult Update(int id)
        {
            Sto_TipoAccionesNE STANE = new Sto_TipoAccionesNE();
            ModeloMaestro mod = new ModeloMaestro();
            mod.Sto_TipoAcciones.tipoAcciones = STANE.getOne(id);
            return View(mod);
        }
        [HttpPost]
        public ActionResult Update(ModeloMaestro mod)
        {
            Sto_TipoAccionesNE STANE = new Sto_TipoAccionesNE();
            int rpta = STANE.ActualizarTipoAccion(mod.Sto_TipoAcciones.tipoAcciones);
            if(rpta > 0)
            {
                return RedirectToAction("Index");
            }
            return View();
        }
        #endregion
    }
}