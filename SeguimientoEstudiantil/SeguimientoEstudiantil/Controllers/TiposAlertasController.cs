﻿using SeguimientoEstudiantil.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CapaEntidad;
using CapaNegocio;

namespace SeguimientoEstudiantil.Controllers
{
    public class TiposAlertasController : Controller
    {

        #region TiposAlertas
        // GET: TiposAlertas
        public ActionResult Index()
        {
            Session["Estado"] = 7;
            ModeloMaestro mod = new ModeloMaestro();
            Sto_TiposAlertas_NE STANE = new Sto_TiposAlertas_NE();
            mod.Sto_TipoAlertas.listaTipoAlertas = STANE.ListarTiposAlertas();
            TempData["Error"] = 0;
            return View(mod);
        }
        #endregion

        #region TiposAlertas/Create
        public ActionResult Create()
        {
            ViewBag.Error = 0;
            ModeloMaestro mod = new ModeloMaestro();
            Sto_AlertaBase_NE STABNE = new Sto_AlertaBase_NE();
            mod.Sto_AlertasBases.ListaAlertaBaseComboBox = new SelectList(STABNE.ListarAlertasBase(), "AlertaBase", "Nombre");
            return PartialView(mod);
        }
        [HttpPost]
        public ActionResult Create(ModeloMaestro mod)
        {

            try
            {
                Sto_TiposAlertas_NE STANE = new Sto_TiposAlertas_NE();
                mod.Sto_TipoAlertas.TiposAlertas.AlertaBase = mod.Sto_AlertasBases.AlertaBase.AlertaBase;
                int rpta = STANE.CrearTiposAlertas(mod.Sto_TipoAlertas.TiposAlertas);
                if (rpta > 0) return RedirectToAction("Index");
                ViewBag.Error = 0;
                return View();
            }
            catch(Exception ex)
            {
                ViewBag.Error = 1;
                return View();
            }
        }
        #endregion

        #region TiposAlertas/Edit
        public ActionResult Edit(int id)
        {
            ModeloMaestro mod = new ModeloMaestro();
            Sto_TiposAlertas_NE STANE = new Sto_TiposAlertas_NE();
            mod.Sto_TipoAlertas.TiposAlertas = STANE.getOne(id);
            ViewBag.Error = 0;
            return View(mod);
        }
        [HttpPost]
        public ActionResult Edit(ModeloMaestro mod)
        {
            try
            {
                Sto_TiposAlertas_NE STANE = new Sto_TiposAlertas_NE();
                int rpta = STANE.ActualizarTiposAlertas(mod.Sto_TipoAlertas.TiposAlertas);
                if (rpta > 0)
                {
                    ViewBag.Error = 2;
                    return View();
                }
                ViewBag.Error = 0;
                return View();
            }
            catch
            {
                ViewBag.Error = 1;
                return View();
            }
        }
        #endregion

        #region TiposAlertas/Delete
        public ActionResult Delete(int id)
        {
            ModeloMaestro mod = new ModeloMaestro();
            Sto_TiposAlertas_NE STANE = new Sto_TiposAlertas_NE();
            Sto_AlertaBase_NE STABNE = new Sto_AlertaBase_NE();
            mod.Sto_TipoAlertas.TiposAlertas = STANE.getOne(id);
            mod.Sto_AlertasBases.ListaAlertaBaseComboBox = new SelectList(STABNE.ListarAlertasBase(), "AlertaBase", "Nombre");
            ViewBag.Error = 0;
            return View(mod);
        }

        [HttpPost]
        public ActionResult Delete(ModeloMaestro mod)
        {
            try
            {

                Sto_TiposAlertas_NE STANE = new Sto_TiposAlertas_NE();
                int rpta = STANE.EliminarTiposAlertas(mod.Sto_TipoAlertas.TiposAlertas.TipoAlerta1);
                if(rpta > 0 )
                {
                    ViewBag.Error = 2;
                    return RedirectToAction("Index");
                }
                ViewBag.Error = 0;
                return View(mod);
            }
            catch
            {
                ViewBag.Error = 1;
                return View();
            }
        }
        #endregion
    }
}