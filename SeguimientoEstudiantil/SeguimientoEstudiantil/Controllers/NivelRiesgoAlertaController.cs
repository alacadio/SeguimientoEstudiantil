﻿using SeguimientoEstudiantil.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CapaNegocio;

namespace SeguimientoEstudiantil.Controllers
{
    public class NivelRiesgoAlertaController : Controller
    {
        #region NivelRiesgoAlerta
        // GET: NivelRiesgoAlerta
        public ActionResult Index()
        {
            Session["Estado"] = 3;
            ModeloMaestro mod = new ModeloMaestro();
            Sto_NivelRiesgoAlerta_NE SNRANE = new Sto_NivelRiesgoAlerta_NE();
            mod.Sto_NivelRiesgoAlerta.listaNivelRiesgoAlerta = SNRANE.ListarNivelRiesgoAlerta();
            return View(mod);
        }
        #endregion 

        #region NivelRiesgoAlerta/Details
        // GET: NivelRiesgoAlerta/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
        #endregion

        #region NivelRiesgoAlerta/Create
        // GET: NivelRiesgoAlerta/Create
        public ActionResult Create()
        {
            ViewBag.Error = "si";
            return PartialView();
        }

        // POST: NivelRiesgoAlerta/Create
        [HttpPost]
        public ActionResult Create(ModeloMaestro mod)
        {
            try
            {
                Sto_NivelRiesgoAlerta_NE SNRANE = new Sto_NivelRiesgoAlerta_NE();
                int rpta = SNRANE.CrearNivelRiesgoAlerta(mod.Sto_NivelRiesgoAlerta.NivelRiesgoAlerta);
                if( rpta > 0) return RedirectToAction("Index");
                ViewBag.Error = "no";
                return View();
            }
            catch
            {
                return View();
            }
        }
        #endregion

        #region NivelRiesgoAlerta/Edit/
        // GET: NivelRiesgoAlerta/Edit/5
        public ActionResult Edit(int id)
        {
            ModeloMaestro mod = new ModeloMaestro();
            Sto_NivelRiesgoAlerta_NE SNRANE = new Sto_NivelRiesgoAlerta_NE();
            mod.Sto_NivelRiesgoAlerta.NivelRiesgoAlerta = SNRANE.getOneNivelRiesgoAlerta(id);
            ViewBag.Error = 0;
            return View(mod);
        }

        // POST: NivelRiesgoAlerta/Edit/5
        [HttpPost]
        public ActionResult Edit(ModeloMaestro mod)
        {
            try
            {
                // TODO: Add update logic here
                Sto_NivelRiesgoAlerta_NE SNERANE = new Sto_NivelRiesgoAlerta_NE();
                int rpta = SNERANE.ActualizarRiesgoNivelAlerta(mod.Sto_NivelRiesgoAlerta.NivelRiesgoAlerta);
                if (rpta > 0) return RedirectToAction("Index");
                ViewBag.Error = 1;
                return View();
            }
            catch
            {
                return View();
            }
        }
        #endregion

        #region NivelRiesgoAlerta/Delete/
        // GET: NivelRiesgoAlerta/Delete/5
        public ActionResult Delete(int id)
        {
            ModeloMaestro mod = new ModeloMaestro();
            Sto_NivelRiesgoAlerta_NE SNRANE = new Sto_NivelRiesgoAlerta_NE();
            mod.Sto_NivelRiesgoAlerta.NivelRiesgoAlerta = SNRANE.getOneNivelRiesgoAlerta(id);
            ViewBag.Error = 0;
            return View(mod);
        }

        // POST: NivelRiesgoAlerta/Delete/5
        [HttpPost]
        public ActionResult Delete(ModeloMaestro mod)
        {
            try
            {
                // TODO: Add delete logic here
                Sto_NivelRiesgoAlerta_NE SNRANE = new Sto_NivelRiesgoAlerta_NE();
                int rpta = SNRANE.EliminarRiesgoNivelAlerta(mod.Sto_NivelRiesgoAlerta.NivelRiesgoAlerta.NivelRiesgoAlerta1);
                if( rpta > 0) return RedirectToAction("Index");
                ViewBag.Error = 1;
                return View();
            }
            catch
            {
                ViewBag.Error = 1;
                return View();
            }
        }
        #endregion
    }
}
