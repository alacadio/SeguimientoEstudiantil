﻿using SeguimientoEstudiantil.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CapaNegocio;

namespace SeguimientoEstudiantil.Controllers
{
    public class TipoAlerta_NivelRiesgoController : Controller
    {
        #region TipoAlerta_NivelRiesgo
        // GET: TipoAlerta_NivelRiesgo
        public ActionResult Index()
        {
            Session["Estado"] = 5;
            ModeloMaestro mod = new ModeloMaestro();
            Sto_TipoAlerta_NivelRiesgo_NE STANRNE = new Sto_TipoAlerta_NivelRiesgo_NE();
            mod.Sto_TipoAlerta_NivelRiesgo.ListaTipoAlerta_NivelRiesgo = STANRNE.ListaTipoAlerta_NivelRiesgo();
            return View(mod);
        }
        #endregion

        #region TipoAlerta_NivelRiesgo/Create
        public ActionResult Create()
        {
            Sto_TipoAccionesNE TipoAcciones = new Sto_TipoAccionesNE();
            Sto_TiposAlertas_NE STANE = new Sto_TiposAlertas_NE();
            Sto_NivelRiesgoAlerta_NE SNRANE = new Sto_NivelRiesgoAlerta_NE();
            ModeloMaestro mod = new ModeloMaestro();
            mod.Sto_NivelRiesgoAlerta.ListaNivelRiesgoAlertaComboBox = new SelectList(SNRANE.ListarNivelRiesgoAlerta(), "NivelRiesgoAlerta1", "Nombre1");
            mod.Sto_TipoAlertas.ListarTiposAlertasComboBox = new SelectList(STANE.ListarTiposAlertas(),  "TipoAlerta1","Nombre1");
            mod.Sto_TipoAcciones.ListarTipoAccionesComboBox = new SelectList(TipoAcciones.ListarTipoAcciones(),  "ID_Sto_TipoAcciones1", "Accion1");
            return PartialView(mod);
        }
        [HttpPost]
        public ActionResult Create(ModeloMaestro mod)
        {
            Sto_TipoAlerta_NivelRiesgo_NE STANRNE = new Sto_TipoAlerta_NivelRiesgo_NE();

            Sto_Accion_TipoAlerta_NivelRiesgo_NE SATANRNE = new Sto_Accion_TipoAlerta_NivelRiesgo_NE();

            mod.Sto_TipoAlerta_NivelRiesgo.
                TipoAlerta_NivelRiesgo.TipoAlerta1 = mod.Sto_TipoAlertas.TiposAlertas.TipoAlerta1;

            mod.Sto_TipoAlerta_NivelRiesgo.
                TipoAlerta_NivelRiesgo.NivelRiesgoAlerta1 = mod.Sto_NivelRiesgoAlerta.NivelRiesgoAlerta.NivelRiesgoAlerta1;

            int rpta = STANRNE.CrearTipoAlerta_NivelRiesgo(mod.Sto_TipoAlerta_NivelRiesgo.TipoAlerta_NivelRiesgo);

            if (rpta > 0)
            {
                int ultimo = rpta;

                mod.Sto_Accion_TipoAlerta_NivelRiesgo.Accion_TipoAlerta.ID_Accion1 =
                    mod.Sto_TipoAcciones.tipoAcciones.ID_Sto_TipoAcciones1;

                mod.Sto_Accion_TipoAlerta_NivelRiesgo.
                    Accion_TipoAlerta.ID_TipoAlerta_NivelRiesgo1 = ultimo;

                int id = mod.Sto_Accion_TipoAlerta_NivelRiesgo.Accion_TipoAlerta.ID_Accion1;

                if (id > 0)
                {
                    rpta = SATANRNE.CrearAccion_TipoAlerta_NivelRiesgo(mod.Sto_Accion_TipoAlerta_NivelRiesgo.Accion_TipoAlerta);
                    if (rpta > 0)
                    {
                        return RedirectToAction("Index");
                    }
                }else
                {
                    return RedirectToAction("Index");
                }

            }
            return View();
        }
        #endregion

        #region TipoAlerta_NivelRiesgo/Edit
        public ActionResult Edit(int id)
        {
            Sto_TipoAlerta_NivelRiesgo_NE STANRNE = new Sto_TipoAlerta_NivelRiesgo_NE();
            Sto_TiposAlertas_NE STANE = new Sto_TiposAlertas_NE();
            Sto_Accion_TipoAlerta_NivelRiesgo_NE SATANRNE = new Sto_Accion_TipoAlerta_NivelRiesgo_NE();
            Sto_NivelRiesgoAlerta_NE SNRANE = new Sto_NivelRiesgoAlerta_NE();
            Sto_TipoAccionesNE STA_NE = new Sto_TipoAccionesNE();
            ModeloMaestro mod = new ModeloMaestro();

            mod.Sto_NivelRiesgoAlerta.
                ListaNivelRiesgoAlertaComboBox = new SelectList(SNRANE.ListarNivelRiesgoAlerta(), "NivelRiesgoAlerta1", "Nombre1");

            mod.Sto_TipoAlertas.
                ListarTiposAlertasComboBox = new SelectList(STANE.ListarTiposAlertas(), "TipoAlerta1", "Nombre1");

            mod.Sto_TipoAlerta_NivelRiesgo.TipoAlerta_NivelRiesgo = STANRNE.getOne(id);

            mod.Sto_TipoAcciones.ListarTipoAccionesComboBox = new SelectList(STA_NE.ListarTipoAcciones(), "ID_Sto_TipoAcciones1", "Accion1");

            mod.Sto_Accion_TipoAlerta_NivelRiesgo.Accion_TipoAlerta = SATANRNE.getOne(mod.Sto_TipoAlerta_NivelRiesgo.TipoAlerta_NivelRiesgo.Id_tipoAlerta_NivelRiesgo1);

            mod.Sto_TipoAcciones.tipoAcciones = STA_NE.getOne(mod.Sto_Accion_TipoAlerta_NivelRiesgo.Accion_TipoAlerta.ID_Accion1);

            return View(mod);
        }
        [HttpPost]
        public ActionResult Edit(ModeloMaestro mod)
        {
            Sto_TipoAlerta_NivelRiesgo_NE STANRNE = new Sto_TipoAlerta_NivelRiesgo_NE();
            Sto_Accion_TipoAlerta_NivelRiesgo_NE SATANRNE = new Sto_Accion_TipoAlerta_NivelRiesgo_NE();
            /*mod.Sto_TipoAlerta_NivelRiesgo.
               TipoAlerta_NivelRiesgo.TipoAlerta1 = mod.Sto_TipoAlertas.TiposAlertas.TipoAlerta1;

            mod.Sto_TipoAlerta_NivelRiesgo.TipoAlerta_NivelRiesgo.
                NivelRiesgoAlerta1 = mod.Sto_NivelRiesgoAlerta.NivelRiesgoAlerta.NivelRiesgoAlerta1;*/

            int rpta = STANRNE.ActualizarTipoAlerta_NivelRiesgo(mod.Sto_TipoAlerta_NivelRiesgo.TipoAlerta_NivelRiesgo);

            if (rpta > 0)
            {

                int i = mod.Sto_Accion_TipoAlerta_NivelRiesgo.Accion_TipoAlerta.ID_Accion_TipoAlerta_NivelRiesgo1;
                rpta = 0;
                if (i <= 0)
                {
                    mod.Sto_Accion_TipoAlerta_NivelRiesgo.Accion_TipoAlerta.ID_Accion1 = mod.Sto_TipoAcciones.tipoAcciones.ID_Sto_TipoAcciones1;
                    mod.Sto_Accion_TipoAlerta_NivelRiesgo.Accion_TipoAlerta.ID_TipoAlerta_NivelRiesgo1 = mod.Sto_TipoAlerta_NivelRiesgo.TipoAlerta_NivelRiesgo.Id_tipoAlerta_NivelRiesgo1;
                    rpta = SATANRNE.CrearAccion_TipoAlerta_NivelRiesgo(mod.Sto_Accion_TipoAlerta_NivelRiesgo.Accion_TipoAlerta);
                    if (rpta > 0)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View();
                    }
                }

                rpta = SATANRNE.Actualizar(mod.Sto_Accion_TipoAlerta_NivelRiesgo.Accion_TipoAlerta);

                if (rpta > 0)
                {
                    return RedirectToAction("Index");
                }else
                {
                    return View();
                }
            }
            return View();
        }
        #endregion

        #region TipoAlerta_NivelRiesgo/Delete
        public ActionResult Delete(int id)
        {
            ModeloMaestro mod = new ModeloMaestro();
            Sto_TipoAlerta_NivelRiesgo_NE STANRNE = new Sto_TipoAlerta_NivelRiesgo_NE();
            mod.Sto_TipoAlerta_NivelRiesgo.TipoAlerta_NivelRiesgo = STANRNE.getOne(id);
            return View(mod);
        }
        [HttpPost]
        public ActionResult Delete(ModeloMaestro mod)
        {
            Sto_TipoAlerta_NivelRiesgo_NE STANRNE = new Sto_TipoAlerta_NivelRiesgo_NE();
            int rpta = STANRNE.EliminarTipoAlerta_NivelRiesgo(mod.Sto_TipoAlerta_NivelRiesgo.TipoAlerta_NivelRiesgo.Id_tipoAlerta_NivelRiesgo1);
            if(rpta > 0)
            {
                return RedirectToAction("Index");
            }
            return View();
        }
        #endregion

        public ActionResult Detail(int id)
        {
            Sto_TipoAlerta_NivelRiesgo_NE STANRNE = new Sto_TipoAlerta_NivelRiesgo_NE();
            Sto_TiposAlertas_NE STANE = new Sto_TiposAlertas_NE();
            Sto_Accion_TipoAlerta_NivelRiesgo_NE SATANRNE = new Sto_Accion_TipoAlerta_NivelRiesgo_NE();
            Sto_NivelRiesgoAlerta_NE SNRANE = new Sto_NivelRiesgoAlerta_NE();
            Sto_TipoAccionesNE STA_NE = new Sto_TipoAccionesNE();
            ModeloMaestro mod = new ModeloMaestro();
            mod.Sto_TipoAlerta_NivelRiesgo.TipoAlerta_NivelRiesgo = STANRNE.getOne(id);
            mod.Sto_TipoAlertas.TiposAlertas = STANE.getOne(mod.Sto_TipoAlerta_NivelRiesgo.TipoAlerta_NivelRiesgo.TipoAlerta1);
            mod.Sto_NivelRiesgoAlerta.NivelRiesgoAlerta = SNRANE.getOneNivelRiesgoAlerta(mod.Sto_TipoAlerta_NivelRiesgo.TipoAlerta_NivelRiesgo.NivelRiesgoAlerta1);
            mod.Sto_Accion_TipoAlerta_NivelRiesgo.Accion_TipoAlerta = SATANRNE.getOne(mod.Sto_TipoAlerta_NivelRiesgo.TipoAlerta_NivelRiesgo.Id_tipoAlerta_NivelRiesgo1);
            mod.Sto_TipoAcciones.tipoAcciones = STA_NE.getOne(mod.Sto_Accion_TipoAlerta_NivelRiesgo.Accion_TipoAlerta.ID_Accion1);
            return View(mod);
        }
    }
}