﻿using CapaEntidad;
using CapaNegocio;
using Newtonsoft.Json;
using SeguimientoEstudiantil.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SeguimientoEstudiantil.Controllers
{
    public class GestionPreguntasController : Controller
    {
        // GET: GestionPreguntas
        #region CARGA DE LAS PREGUNTAS DEL API AL SISTEMA
        public async Task<ActionResult> Index()
        {
            Session["Estado"] = 6;
            ModeloMaestro mod = new ModeloMaestro();
            Sto_TiposAlertas_NE STANE = new Sto_TiposAlertas_NE();
            var httpClient = new HttpClient();
            var json = await httpClient.GetStringAsync("http://localhost:53931/api/FormatoCaracterizacionPreguntas");
            var lista = JsonConvert.DeserializeObject<List<Sto_Formato_Caracterizacion_Preguntas>>(json);
            Session["MyLista"] = lista;
            Session["MyResp"] = 0;
            mod.Sto_TipoAlertas.ListarTiposAlertasComboBox = new SelectList(STANE.ListarTiposAlertas(), "TipoAlerta1", "Nombre1");
            return View(mod);
        }
        #endregion

        #region REGISTRA LAS PREGUNTAS EN LA BASE DE DATOS
        public int RegPregDB(string cod,string tip,string det)
        {
            int tipo = Convert.ToInt32(tip.ToString());
            int rpta = 0;
            Sto_TipoAlerta_Pregunta_NE STAPNE = new Sto_TipoAlerta_Pregunta_NE();
            Sto_TipoAlerta_Pregunta STAP = new Sto_TipoAlerta_Pregunta();
            STAP.CodigoPRegunta1 = cod;
            STAP.TipoAlerta1 = tipo;
            STAP.DescripcionPRegunta1 = det;
            rpta = STAPNE.Crear_TipoAlerta_Pregunta(STAP);
            return rpta;
        }
        #endregion

        #region ELIMINA UNA PREGUNTA DE LA BASE DE DATOS
        public int DelPregDB(string cod)
        {
            int rpta = 0;
            Sto_TipoAlerta_Pregunta_NE STAPNE = new Sto_TipoAlerta_Pregunta_NE();
            Sto_TipoAlerta_Pregunta STAP = new Sto_TipoAlerta_Pregunta();
            STAP = STAPNE.Listar_Pregunta_COD_Preg(cod);
            rpta = STAPNE.EliminarPregunta(STAP);
            return rpta;
        }
        #endregion

        #region PEGUNTAS POR TIPO ALERTAS
        public ActionResult PreguntasTipoAlerta(string tipo)
        {
            int tip = Convert.ToInt32(tipo.ToString());
            ModeloMaestro mod = new ModeloMaestro();
            Sto_TipoAlerta_Pregunta_NE STAPNE = new Sto_TipoAlerta_Pregunta_NE();
            mod.Sto_TipoAlertas_Preguntas.listarTipoAlerta_Pregunta = STAPNE.Listar_Pregunta_COD_Tipo(tip);
            return PartialView(mod);
        }

        public JsonResult CargPregTipo(string tipo)
        {
            int tip = Convert.ToInt32(tipo.ToString());
            Sto_TipoAlerta_Pregunta_NE STAPNE = new Sto_TipoAlerta_Pregunta_NE();
            List<Sto_TipoAlerta_Pregunta> lista = STAPNE.Listar_Pregunta_COD_Tipo(tip);
            Session["preg"] = lista;
            Session["id"] = tip;
            var json = Json(lista, JsonRequestBehavior.AllowGet);
            return json;
        }
        #endregion

        #region RESPUESTAS POR CODIGO PREGUNTA

        public async Task<ActionResult>RespuestaPorPregunta(string cod)
        {
            
            Sto_TipoAlerta_Pregunta_NE STAPNE = new Sto_TipoAlerta_Pregunta_NE();
            Sto_TipoAlerta_Pregunta STAP = new Sto_TipoAlerta_Pregunta();
            STAP = STAPNE.Listar_Pregunta_COD_Preg(cod);
            var httpClient = new HttpClient();
            var url = "http://localhost:53931/api/TraerCaracterizacionRespuestas";
            var codigo = "?cod=" + cod;
            var json = await httpClient.GetStringAsync(url+codigo+"&mod=nose");
            var respuestas = JsonConvert.DeserializeObject<List<Sto_TraerCaracterizacionRespuestas>>(json);
            ViewBag.ListaRespuesta = respuestas;
            ViewBag.ID = STAP.Id_TipoAlerta_Preguntas1;
            return PartialView();
        }

        #endregion

        #region REGISTRO DE LAS RESPUESTAS
        public async Task<int> RegstResInd(string codP,string codR,string des,string val,string op)
        {
            Sto_TipoAlerta_Pregunta_NE STAPNE = new Sto_TipoAlerta_Pregunta_NE();
            Sto_Pregunta_ValoracionRiesgoRespuesta_NE SPVRNE = new Sto_Pregunta_ValoracionRiesgoRespuesta_NE();
            Sto_TipoAlerta_Pregunta STAP = new Sto_TipoAlerta_Pregunta();
            Sto_Pregunta_ValoracionRiesgoRespuesta SPVRR = new Sto_Pregunta_ValoracionRiesgoRespuesta();
            STAP = STAPNE.Listar_Pregunta_COD_Preg(codP);
            int rpta = 0;
            int aux = 0;
            int aux1 = 0;
            int operacion = Convert.ToInt32(op.ToString());
            int valor = Convert.ToInt32(val.ToString());
            if(operacion == 1)
            {
                var httpClient = new HttpClient();
                var url = "http://localhost:53931/api/TraerCaracterizacionRespuestas";
                var codigo = "?cod=" + codP;
                var json = await httpClient.GetStringAsync(url + codigo + "&mod=nose");
                List<Sto_TraerCaracterizacionRespuestas> respuestas = JsonConvert.DeserializeObject<List<Sto_TraerCaracterizacionRespuestas>>(json);
                aux = respuestas.Count();

                foreach(var m in respuestas)
                {
                    SPVRR = new Sto_Pregunta_ValoracionRiesgoRespuesta();
                    SPVRR.Id_TipoAlerta_Pregunta1 = STAP.Id_TipoAlerta_Preguntas1;
                    SPVRR.DescripcionRespuesta1 = m.Detalles;
                    SPVRR.Respuesta1 = m.Respuesta;
                    SPVRR.ValorRiesgo1 = 0;
                    rpta = SPVRNE.RegistrarRespuestaPorPregunta(SPVRR);
                    aux1++;
                    if(aux == aux1)
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            else if(operacion == 2)
            {
                SPVRR = new Sto_Pregunta_ValoracionRiesgoRespuesta();
                SPVRR.Id_TipoAlerta_Pregunta1 = STAP.Id_TipoAlerta_Preguntas1;
                SPVRR.DescripcionRespuesta1 = des;
                SPVRR.Respuesta1 = codR;
                SPVRR.ValorRiesgo1 = valor;
                rpta = SPVRNE.RegistrarRespuestaPorPregunta(SPVRR);
            }

            return rpta;
        }
        #endregion

        #region PREGUNTAS COMPARACION API BASE DATOS
        public ActionResult Preguntas(string tip)
        {
            int tipo = Convert.ToInt32(tip);
            Sto_TipoAlerta_Pregunta_NE STAPNE = new Sto_TipoAlerta_Pregunta_NE();
            Sto_TiposAlertas_NE STANE = new Sto_TiposAlertas_NE();
            ModeloMaestro mod = new ModeloMaestro();
            List<Sto_TipoAlerta_Pregunta> list = STAPNE.Listar_preguntas();
            Session["ListaB"] = list;
            mod.Sto_TipoAlertas_Preguntas.listarTipoAlerta_Pregunta = STAPNE.Listar_Pregunta_COD_Tipo(tipo);
            mod.Sto_TipoAlertas.TiposAlertas = STANE.getOne(tipo);
            ViewBag.ID = tipo;
            return PartialView(mod);
        }
        #endregion

        #region RESPUESTA POR PREGUNTA
        public async Task<ActionResult> ListaRespuestasSinRegistrar(string cod,string t)
        {
            int tipo = 0;
            if (t.Length > 0)
            {
                tipo = Convert.ToInt32(t.ToString());
            }
            ModeloMaestro mod = new ModeloMaestro();
            Sto_TipoAlerta_Pregunta_NE STAPNE = new Sto_TipoAlerta_Pregunta_NE();
            Sto_TipoAlerta_Pregunta STAP = new Sto_TipoAlerta_Pregunta();
            STAP = STAPNE.Listar_Pregunta_COD_Preg(cod);
            Sto_Pregunta_ValoracionRiesgoRespuesta_NE STPVRRNE = new Sto_Pregunta_ValoracionRiesgoRespuesta_NE();
            List<Sto_Pregunta_ValoracionRiesgoRespuesta> listaRes = STPVRRNE.obtenerPorIdPreguntaValorRespuesta(STAP.Id_TipoAlerta_Preguntas1,tipo);
            var httpClient = new HttpClient();
            var url = "http://localhost:53931/api/TraerCaracterizacionRespuestas";
            var codigo = "?cod=" + cod;
            var json = await httpClient.GetStringAsync(url + codigo + "&mod=nose");
            var respuestas = JsonConvert.DeserializeObject<List<Sto_TraerCaracterizacionRespuestas>>(json);
            ViewBag.ListaRespuestaNoRegistradas = respuestas;
            ViewBag.RespuestaBaseDatos = listaRes;
            return PartialView();
        }
        #endregion
    }
}
