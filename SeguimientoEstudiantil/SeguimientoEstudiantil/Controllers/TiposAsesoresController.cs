﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CapaNegocio;
using CapaEntidad;
using SeguimientoEstudiantil.Models;

namespace SeguimientoEstudiantil.Controllers
{
    public class TiposAsesoresController : Controller
    {
        // GET: TiposAsesores
        #region INDEX
        public ActionResult Index()
        {
            Session["Estado"] = 8;
            TempData["Error"] = 0;
            Sto_TiposAsesor_Usuario_NE STAUNE = new Sto_TiposAsesor_Usuario_NE();
            Seg_Usuario_NE SUNE = new Seg_Usuario_NE();
            Sto_TiposAsesores_NE STANE = new Sto_TiposAsesores_NE();
            ModeloMaestro mod = new ModeloMaestro();
            mod.Sto_TipoAsesores.listaTipoAsesores = STANE.ListarTiposAsesores();
            mod.Sto_TipoAsesores_Usuario.listaTipoAsesorUsuario = STAUNE.ListarTipoAsesorUsuario();
            mod.Seg_Usuario.ListaUsuario = SUNE.ListarUsuarios();
            return View(mod);
        }
        #endregion
        // GET: TiposAsesores/Details/5
        #region DETALLES
        public ActionResult Details(int id)
        {
            Sto_TiposAsesor_Usuario_NE STAUNE = new Sto_TiposAsesor_Usuario_NE();
            ModeloMaestro mod = new ModeloMaestro();
            mod.Sto_TipoAsesores_Usuario.TipoAsesorUsuario = STAUNE.TipoAsesorUsuario(id);
            return View(mod);
        }
        #endregion
        // GET: TiposAsesores/Create/
        #region REGISTRO
        public ActionResult CreateTipoAsesor()
        {
            ModeloMaestro mod = new ModeloMaestro();
            return PartialView(mod);
        }

        [HttpPost]
        public ActionResult CreateTipoAsesor(ModeloMaestro mod)
        {
            try
            {
                int rpta = 0;
                Sto_TiposAsesores_NE STANE = new Sto_TiposAsesores_NE();
                rpta = STANE.RegsitrarTiposAsesores(mod.Sto_TipoAsesores.TipoAsesores);
                if (rpta > 0)
                {
                    TempData["Err"] = 2;
                    return RedirectToAction("Index");
                }else
                {
                    TempData["Err"] = 1;
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                TempData["Err"] = 1;
                return RedirectToAction("Index");
            }
        }

        /////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////
        public ActionResult CreateUsuario()
        {
            ModeloMaestro mod = new ModeloMaestro();
            Sto_TiposAsesores_NE STANE = new Sto_TiposAsesores_NE();
            mod.Sto_TipoAsesores.listaComboBoxTipoAsesores = new SelectList(STANE.ListarTiposAsesores(), "Sto_TipoAsesor", "Nombre");
            return PartialView(mod);
        }
        [HttpPost]
        public ActionResult CreateUsuario(ModeloMaestro mod)
        {
            try
            {
                Seg_Usuario_NE SUNE = new Seg_Usuario_NE();
                Sto_TiposAsesor_Usuario_NE STAUNE = new Sto_TiposAsesor_Usuario_NE();
                Sto_TipoAsesor_Usuario STAU = new Sto_TipoAsesor_Usuario();
                int rpta = SUNE.RegistrarUsuario(mod.Seg_Usuario.Usuario);
                if(rpta > 0)
                {
                    rpta = 0;
                    STAU.TipoAsesor = mod.Sto_TipoAsesores_Usuario.TipoAsesorUsuario.TipoAsesor;
                    STAU.Usuario = mod.Seg_Usuario.Usuario.Usuario;
                    rpta = STAUNE.RegistrarTipoAsesorUsuario(STAU);
                    if (rpta > 0)
                    {
                        TempData["Err"] = 2;
                        return RedirectToAction("Index");
                    }
                }
                TempData["Err"] = 1;
                return PartialView();
            }
            catch
            {
                TempData["Err"] = 1;
                return RedirectToAction("Index");
            }
        }
        #endregion
        // GET: TiposAsesores/Edit/5
        #region CONFIGURAR
        public ActionResult Edit(string cod)
        {
            return PartialView();
        }

        // POST: TiposAsesores/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        #endregion
        // GET: TiposAsesores/Delete/5
        #region ELIMINAR
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: TiposAsesores/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        #endregion
        //  GET: TiposAsesores/ListaUsuarios/
        #region Lista
        public ActionResult ListaGrupos()
        {
            ModeloMaestro mod = new ModeloMaestro();
            Sto_TiposAsesores_NE STANE = new Sto_TiposAsesores_NE();
            mod.Sto_TipoAsesores.listaTipoAsesores = STANE.ListarTiposAsesores();
            return PartialView(mod);
        }
        public ActionResult ListaUsuarios()
        {
            ModeloMaestro mod = new ModeloMaestro();
            Seg_Usuario_NE SUNE = new Seg_Usuario_NE();
            mod.Seg_Usuario.ListaUsuario = SUNE.ListarUsuarios();
            return PartialView(mod);
        }
        #endregion
    }
}
