﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CapaEntidad;
using CapaNegocio;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SeguimientoEstudiantil.Models;
using System.Web.Routing;
using System;

namespace SeguimientoEstudiantil.Controllers
{
    public class TipoAlertaPreguntasController : Controller
    {
        /// <summary>
        /// Variables globales para el manejo del id de la pregunta para el registro de la respuesta
        /// </summary>
        #region VARIABLES GLOBALES
        private int ID { get; set; }
        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////

        #region CARGA DE FORMA AUTOMATICA TODAS LAS PREGUNTAS REGISTRADAS A LA CARACTERIZACION
        // GET: TipoAlertaPreguntas
        public async Task<ActionResult> Index()
        {
            Session["Estado"] = 6;
            ModeloMaestro mod = new ModeloMaestro();
            Sto_TiposAlertas_NE STANE = new Sto_TiposAlertas_NE();
            var httpClient = new HttpClient();
            var json = await httpClient.GetStringAsync("http://localhost:53931/api/FormatoCaracterizacionPreguntas");
            var lista = JsonConvert.DeserializeObject<List<Sto_Formato_Caracterizacion_Preguntas>>(json);
            Session["MyLista"] = lista;
            mod.Sto_TipoAlertas_Preguntas.ListarComboBoxAPI = new SelectList(lista, "CodigoInterno1", "DetallePregunta1");
            mod.Sto_TipoAlertas.ListarTiposAlertasComboBox = new SelectList(STANE.ListarTiposAlertas(), "TipoAlerta1", "Nombre1");
            return View(mod);
        }
        ///////////////////////
        public ActionResult PreguntasApis()
        {
            ModeloMaestro mod = new ModeloMaestro();
            Sto_TiposAlertas_NE STANE = new Sto_TiposAlertas_NE();
            mod.Sto_TipoAlertas.ListarTiposAlertasComboBox = new SelectList(STANE.ListarTiposAlertas(), "TipoAlerta1", "Nombre1");
            return PartialView(mod);
        }
        #endregion

        #region BUSCA LAS PREGUNTAS QUE SE TIENEN EN EL API
        public async Task<ActionResult> Buscar(string contexto, int id, int val)
        {
            var httpClient = new HttpClient();
            var json = await httpClient.GetStringAsync("http://localhost:53931/api/TraerCaracterizacionRespuestas?cod=" + contexto + "&mod=nose");
            var lista = JsonConvert.DeserializeObject<List<Sto_TraerCaracterizacionRespuestas>>(json);
            CreateR(lista, id, val);
            ViewBag.Envio = lista;
            ViewBag.id = id;
            return PartialView();
        }
        //////////////////////////////////////////////////////////////////////////////////////////
        /*public async Task<ActionResult> Buscar(string contexto, int id,int val)
        {
            var httpClient = new HttpClient();
            var json = await httpClient.GetStringAsync("http://localhost:53931/api/TraerCaracterizacionRespuestas?cod=" + contexto + "&mod=nose");
            var lista = JsonConvert.DeserializeObject<List<Sto_TraerCaracterizacionRespuestas>>(json);
            ViewBag.Envio = lista;
            ViewBag.id = id;
            return PartialView();
        }*/
        //////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////
        public async Task<JsonResult> Buscar3(string contexto)
        {
            var httpClient = new HttpClient();
            var json = await httpClient.GetStringAsync("http://localhost:53931/api/FormatoCaracterizacionPreguntas?id=" + contexto);
            var js = Json(json, JsonRequestBehavior.AllowGet);
            return js;
        }
        #endregion

        #region DETALLES GENERALES DE LAS PREGUNTAS REGISTRADAS Y LAS REPUESTAS REGISTRADAS
        // GET: TipoAlertaPreguntas/Details/5
        public ActionResult DetailsP(int id)
        {
            return View();
        }
        //////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////
        public ActionResult DetailsR(int id)
        {
            return View();
        }
        #endregion

        #region REGISTRA UNA NUEVA PREGUNTA Y LOS VALORES DE LAS RESPUESTAS
        // GET: TipoAlertaPreguntas/Create
        public ActionResult CreateP(string cod, string tipo, string des, int val)
        {
            /*if(cod != null)
            {
                RouteValueDictionary ru = new RouteValueDictionary();
                ru.Add("contexto", cod);
                ru.Add("id", 1);
                if (val == 0)
                {
                    ru.Add("val", 0);
                }
                else
                {
                    ru.Add("val", val);
                }
                return RedirectToAction("Buscar", ru);
            }*/
            int id = Convert.ToInt32(tipo.ToString());
            Sto_TipoAlerta_Pregunta STAP;
            Sto_TipoAlerta_Pregunta_NE STAPNE = new Sto_TipoAlerta_Pregunta_NE();
            STAP = STAPNE.Listar_Pregunta_COD_Preg(cod);
            if (STAP.CodigoPRegunta1 == null)
            {
                STAP = new Sto_TipoAlerta_Pregunta(id, cod, des);
                int rpta = STAPNE.Crear_TipoAlerta_Pregunta(STAP);
                if (rpta > 0)
                {
                    RouteValueDictionary ru = new RouteValueDictionary();
                    ru.Add("contexto", cod);
                    ru.Add("id", rpta);
                    if (val == 0)
                    {
                        ru.Add("val", 0);
                    }
                    else
                    {
                        ru.Add("val", val);
                    }
                    return RedirectToAction("Buscar", ru);
                }
            }
            ViewBag.Error = 0;
            return PartialView();
        }
        //////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////
        public int CreateR(List<Sto_TraerCaracterizacionRespuestas> lista, int id, int val)
        {
            Sto_Pregunta_ValoracionRiesgoRespuesta_NE SPVRNE = new Sto_Pregunta_ValoracionRiesgoRespuesta_NE();
            Sto_Pregunta_ValoracionRiesgoRespuesta SPVR = new Sto_Pregunta_ValoracionRiesgoRespuesta();
            int rpta = 0;
            int l = lista.Count();
            foreach (var m in lista)
            {
                SPVR.Id_TipoAlerta_Pregunta1 = id;
                SPVR.ValorRiesgo1 = val;
                SPVR.Respuesta1 = m.Respuesta;
                SPVR.DescripcionRespuesta1 = m.Detalles;
                rpta = SPVRNE.RegistrarRespuestaPorPregunta(SPVR);
            }
            if (rpta > 0 && rpta == l)
            {
                rpta = 1;
            }
            else
            {
                rpta = 0;
            }
            return rpta;
        }
        //////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////
        public int CreatePreguntaLista(string cod, string aler)
        {
            int rpta = 0;
            int a = Convert.ToInt32(aler.ToString());
            Sto_TipoAlerta_Pregunta_NE STAPNE = new Sto_TipoAlerta_Pregunta_NE();
            if ((cod != null) && (a > 0))
            {
                rpta = 1;
            }
            return rpta;
        } 
        public int DeletePreguntaLista(string cod)
        {
            int rpta = 0;
            Sto_TipoAlerta_Pregunta_NE STAPNE = new Sto_TipoAlerta_Pregunta_NE();
            if (cod != null)
            {
                rpta = 1;
            }
            return rpta;
        }
        #endregion

        #region EDITA LA INFORMACION DE LAS PREGUNTAS REGISTRADAS Y LAS RESPUESTAS
        // GET: TipoAlertaPreguntas/Edit/5
        public ActionResult EditP(int id)
        {
            ModeloMaestro mod = new ModeloMaestro();
            Sto_TiposAlertas_NE STANE = new Sto_TiposAlertas_NE();
            Sto_TipoAlerta_Pregunta_NE STAPNE = new Sto_TipoAlerta_Pregunta_NE();
            mod.Sto_TipoAlertas.ListarTiposAlertasComboBox = new SelectList(STANE.ListarTiposAlertas(), "TipoAlerta1", "Nombre1");
            mod.Sto_TipoAlertas.ListarTiposAlertasComboBox = new SelectList(STANE.ListarTiposAlertas(), "TipoAlerta1", "Nombre1");
            mod.Sto_TipoAlertas_Preguntas.TipoAlerta_Pregunta = STAPNE.Listar_Pregunta_ByID(id);
            return PartialView(mod);
        }

        // POST: TipoAlertaPreguntas/Edit/5
        [HttpPost]
        public ActionResult EditP(ModeloMaestro mod)
        {
            try
            {
                Sto_TipoAlerta_Pregunta_NE STAPNE = new Sto_TipoAlerta_Pregunta_NE();

                int rpta = STAPNE.EditarPregunta(mod.Sto_TipoAlertas_Preguntas.TipoAlerta_Pregunta);

                if (rpta > 0)
                {
                    TempData["Erro"] = 1;
                    return RedirectToAction("Details");
                }
                else
                {
                    ViewBag.Erro = 0;
                    return View();
                }
            }
            catch
            {
                return View();
            }
        }

        ///////////////////////////////////////////////////////////////////////
        public ActionResult EditR(int id)
        {
            return PartialView();
        }
        [HttpPost]
        public ActionResult EditR(ModeloMaestro mod)
        {
            ViewBag.Error = 3;
            return View();
        }
        #endregion

        #region ELIMINA UNA PREGUNTA Y LAS RESPUESTA DEL SISTEMA
        // GET: TipoAlertaPreguntas/Delete/5
        public ActionResult Delete(int id)
        {
            Sto_TipoAlerta_Pregunta_NE STAPNE = new Sto_TipoAlerta_Pregunta_NE();
            ModeloMaestro mod = new ModeloMaestro();
            mod.Sto_TipoAlertas_Preguntas.TipoAlerta_Pregunta = STAPNE.Listar_Pregunta_ByID(id);
            return View(mod);
        }

        // POST: TipoAlertaPreguntas/Delete/5
        [HttpPost]
        public ActionResult Delete(ModeloMaestro mod)
        {
            try
            {
                // TODO: Add delete logic here
                Sto_TipoAlerta_Pregunta_NE STAPNE = new Sto_TipoAlerta_Pregunta_NE();
                int rpta = STAPNE.EliminarPregunta(mod.Sto_TipoAlertas_Preguntas.TipoAlerta_Pregunta);
                if (rpta > 0)
                {
                    TempData["Error"] = 1;
                    return RedirectToAction("Details");
                }
                ViewBag.Error = 2;
                return View();
            }
            catch
            {
                ViewBag.Error = 0;
                return View();
            }
        }
        #endregion

        #region DETALLES DE LAS PREGUNTAS
        public ActionResult Details()
        {
            ModeloMaestro mod = new ModeloMaestro();
            Sto_TipoAlerta_Pregunta_NE STAPNE = new Sto_TipoAlerta_Pregunta_NE();
            mod.Sto_TipoAlertas_Preguntas.listarTipoAlerta_Pregunta = STAPNE.Listar_preguntas();
            return View(mod);
        }

        #endregion
        /*
        #region LISTADO DE RESPUESTAS REGISTRADAS POR PREGUNTA
        public ActionResult ListRespuesta(string codPreg)
        {
            ModeloMaestro mod = new ModeloMaestro();
            Sto_Pregunta_ValoracionRiesgoRespuesta_NE SPVRNE = new Sto_Pregunta_ValoracionRiesgoRespuesta_NE();
            Sto_TipoAlerta_Pregunta_NE STAPNE = new Sto_TipoAlerta_Pregunta_NE();
            Sto_TipoAlerta_Pregunta STAP = new Sto_TipoAlerta_Pregunta();
            Sto_Pregunta_ValoracionRiesgoRespuesta SPVR = new Sto_Pregunta_ValoracionRiesgoRespuesta();
            try
            {
                STAP = STAPNE.Listar_Pregunta_COD_Preg(codPreg);
                if (STAP.Id_TipoAlerta_Preguntas1 > 0)
                {
                    SPVR.Id_TipoAlerta_Pregunta1 = STAP.Id_TipoAlerta_Preguntas1;
                    mod.Sto_Respuestas.listaRespuestas = SPVRNE.obtenerPorIdPreguntaValorRespuesta(SPVR.Id_TipoAlerta_Pregunta1);
                    ViewBag.Error = 1;
                    return PartialView(mod);
                }
                else
                {
                    ViewBag.Error = 0;
                    return PartialView();
                }
            }
            catch (Exception e)
            {
                ViewBag.Error = -1;
                return PartialView();
            }
        }
        #endregion
    */
    }
}
