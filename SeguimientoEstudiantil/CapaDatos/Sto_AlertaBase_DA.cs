﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CapaEntidad;

namespace CapaDatos
{
    public class Sto_AlertaBase_DA
    {
        
        #region VARIABLES GLOBALES DE LA CLASE
        /// <summary>
        /// Variable de inicializacion de los nombre de los procedimiento.
        /// </summary>
        private string PROCEDIMIENTO = "";
        /// <summary>
        /// Variable de las diferentes operaciones que realiza el procedimiento.
        /// </summary>
        private int LISTAR = ListaProcedimientos.OPCION_LISTAR;
        private int LISTAR_ByID = ListaProcedimientos.OPCION_LISTAR_BYID;
        private int CREAR = ListaProcedimientos.OPCION_CREAR;
        private int ACTUALIZAR = ListaProcedimientos.OPCION_ACTUALIZAR;
        private int ELIMINAR = ListaProcedimientos.OPCION_ELIMINAR;
        #endregion

        #region LISTAR TODAS LAS ALERTAS BASE
        public List<Sto_AlertaBase> listaAlertaBase()
        {
            List<Sto_AlertaBase> lista = new List<Sto_AlertaBase>();
            PROCEDIMIENTO = ListaProcedimientos.MAESTRO + ListaProcedimientos.Alerta_Base;
            Sto_AlertaBase STAB = new Sto_AlertaBase();
            using(SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using(SqlCommand comando = new SqlCommand(PROCEDIMIENTO, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@AlertaBase", 0);
                    comando.Parameters.AddWithValue("@Nombre ", string.Empty);
                    comando.Parameters.AddWithValue("@Descripcion", string.Empty);
                    comando.Parameters.AddWithValue("@FuenteDatos", 0);
                    comando.Parameters.AddWithValue("@UsaurioConfigura", 0);
                    comando.Parameters.AddWithValue("@op", LISTAR);
                    comando.Parameters.AddWithValue("@id", 0);
                    using(SqlDataReader read = comando.ExecuteReader())
                    {
                        if (read != null)
                        {
                            while (read.Read())
                            {
                                STAB = new Sto_AlertaBase();
                                STAB.AlertaBase = Convert.ToInt32(read["Alerta_base"].ToString());
                                STAB.Nombre= read["Nombre"].ToString();
                                STAB.Descripcion = read["Descripcion"].ToString();
                                STAB.FuenteDato = Convert.ToInt32(read["FuenteDato"].ToString());
                                STAB.UsuarioConfigura = Convert.ToBoolean(read["UsuarioConfigura"].ToString());
                                lista.Add(STAB);

                            }
                        }
                    }
                }
            }
            return lista;
        }
        #endregion

        #region LISTAR LAS ALERTAS BASE POR ID
        public Sto_AlertaBase getOneAlertaBase(int id)
        {
            PROCEDIMIENTO = ListaProcedimientos.MAESTRO + ListaProcedimientos.Alerta_Base;
            Sto_AlertaBase STAB = new Sto_AlertaBase();
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTO, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@AlertaBase", id);
                    comando.Parameters.AddWithValue("@Nombre ", string.Empty);
                    comando.Parameters.AddWithValue("@Descripcion", string.Empty);
                    comando.Parameters.AddWithValue("@FuenteDatos", 0);
                    comando.Parameters.AddWithValue("@UsaurioConfigura", 0);
                    comando.Parameters.AddWithValue("@op", LISTAR_ByID);
                    comando.Parameters.AddWithValue("@id", 0);
                    using (SqlDataReader read = comando.ExecuteReader())
                    {
                        if (read != null)
                        {
                            if(read.Read())
                            {
                                STAB = new Sto_AlertaBase();
                                STAB.AlertaBase = Convert.ToInt32(read["Alerta_base"].ToString());
                                STAB.Nombre = read["Nombre"].ToString();
                                STAB.Descripcion = read["Descripcion"].ToString();
                                STAB.FuenteDato = Convert.ToInt32(read["FuenteDato"].ToString());
                                STAB.UsuarioConfigura = Convert.ToBoolean(read["UsuarioConfigura"].ToString());
                            }
                        }
                    }
                }
            }
            return STAB;
        }
        #endregion
    }
}
