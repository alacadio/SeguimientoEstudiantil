﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CapaEntidad;

namespace CapaDatos
{
    public class Sto_TipoAlerta_Pregunta_DA
    {

        #region VARIABLES GLOBALES DE LA CLASE
        /// <summary>
        /// Variable de inicializacion de los nombre de los procedimiento.
        /// </summary>
        private string PROCEDIMIENTOS = "";
        /// <summary>
        /// Variable de las diferentes operaciones que realiza el procedimiento.
        /// </summary>
        private int LISTAR = ListaProcedimientos.OPCION_LISTAR;
        private int LISTAR_ByID = ListaProcedimientos.OPCION_LISTAR_BYID;
        private int CREAR = ListaProcedimientos.OPCION_CREAR;
        private int ACTUALIZAR = ListaProcedimientos.OPCION_ACTUALIZAR;
        private int ELIMINAR = ListaProcedimientos.OPCION_ELIMINAR;
        /// <summary>
        /// Declaraion de la instancia a la clase
        /// </summary>
        private Sto_TipoAlerta_Pregunta STAP;
        #endregion

        #region REGISTRAR NUEVO TIPO ALERTAS PREGUNTAS
        public int CrearTiposAlertas(Sto_TipoAlerta_Pregunta STA)
        {
            int rpta = 0;
            PROCEDIMIENTOS = ListaProcedimientos.MAESTRO + ListaProcedimientos.TipoAlertas_Preguntas;
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    SqlParameter id = comando.Parameters.AddWithValue("@id", 0);
                    id.Direction = ParameterDirection.InputOutput;
                    comando.Parameters.AddWithValue("@id_tipoAlerta_pregunta", STA.Id_TipoAlerta_Preguntas1);
                    comando.Parameters.AddWithValue("@tipoAlerta", STA.TipoAlerta1);
                    comando.Parameters.AddWithValue("@codPreg", STA.CodigoPRegunta1);
                    comando.Parameters.AddWithValue("@descPreg", STA.DescripcionPRegunta1);
                    comando.Parameters.AddWithValue("@operacion", CREAR);
                    
                    comando.ExecuteNonQuery();
                    rpta = Convert.ToInt32(comando.Parameters["@id"].Value);
                }
            }
            return rpta;
        }
        #endregion

        #region ActualizarTipoAlertasPreguntas
        public int ActualizarTipoAlertasPreguntas(Sto_TipoAlerta_Pregunta STAP)
        {
            int rpta = 0;
            PROCEDIMIENTOS = ListaProcedimientos.MAESTRO + ListaProcedimientos.TipoAlertas_Preguntas;
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@id", 0);
                    comando.Parameters.AddWithValue("@id_tipoAlerta_pregunta", STAP.Id_TipoAlerta_Preguntas1);
                    comando.Parameters.AddWithValue("@tipoAlerta", STAP.TipoAlerta1);
                    comando.Parameters.AddWithValue("@codPreg", STAP.CodigoPRegunta1);
                    comando.Parameters.AddWithValue("@descPreg", STAP.DescripcionPRegunta1);
                    comando.Parameters.AddWithValue("@operacion", ACTUALIZAR);

                    rpta = comando.ExecuteNonQuery();
                }
            }
            return rpta;
        }
        #endregion

        #region ELIMINAR PREGUNTA DE LA BASE DE DATOS
        public int EliminarPregunta(Sto_TipoAlerta_Pregunta st)
        {
            int rpta = 0;
            try
            {
                PROCEDIMIENTOS = ListaProcedimientos.MAESTRO + ListaProcedimientos.TipoAlertas_Preguntas;
                using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
                {
                    con.Open();
                    using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                    {
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@id", 0);
                        comando.Parameters.AddWithValue("@id_tipoAlerta_pregunta", st.Id_TipoAlerta_Preguntas1);
                        comando.Parameters.AddWithValue("@tipoAlerta", 0);
                        comando.Parameters.AddWithValue("@codPreg", string.Empty);
                        comando.Parameters.AddWithValue("@descPreg", string.Empty);
                        comando.Parameters.AddWithValue("@operacion", ELIMINAR);

                        rpta = comando.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                rpta = 0;
            }
            
            return rpta;
        }
        #endregion

        #region LISTAR PREGUNTA POR ID
        public Sto_TipoAlerta_Pregunta getPregunta(int id)
        {
            Sto_TipoAlerta_Pregunta STAP = new Sto_TipoAlerta_Pregunta();
            PROCEDIMIENTOS = ListaProcedimientos.MAESTRO + ListaProcedimientos.TipoAlertas_Preguntas;
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@id", 0);
                    comando.Parameters.AddWithValue("@id_tipoAlerta_pregunta", id);
                    comando.Parameters.AddWithValue("@tipoAlerta", 0);
                    comando.Parameters.AddWithValue("@codPreg", "s");
                    comando.Parameters.AddWithValue("@descPreg", "s");
                    comando.Parameters.AddWithValue("@operacion", LISTAR_ByID);

                    using (SqlDataReader read = comando.ExecuteReader())
                    {
                        if (read != null)
                        {
                            while (read.Read())
                            {
                                STAP = new Sto_TipoAlerta_Pregunta();
                                STAP.Id_TipoAlerta_Preguntas1 = Convert.ToInt32(read["id_tipoAlerta_pregunta"].ToString());
                                STAP.TipoAlerta1 = Convert.ToInt32(read["TipoAlerta"].ToString());
                                STAP.CodigoPRegunta1 = read["CodigoPregunta"].ToString();
                                STAP.DescripcionPRegunta1 = read["DescripcionPregunta"].ToString();
                            }
                        }
                    }
                }
            }
            return STAP;
        }
        #endregion

        #region lISTAR TODAS LAS PREGUNTAS REGISTRADAS
        public List<Sto_TipoAlerta_Pregunta> ListarPreguntasRegistradas()
        {
            List<Sto_TipoAlerta_Pregunta> lista = new List<Sto_TipoAlerta_Pregunta>();
            PROCEDIMIENTOS = ListaProcedimientos.MAESTRO + ListaProcedimientos.TipoAlertas_Preguntas;
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@id", 0);
                    comando.Parameters.AddWithValue("@id_tipoAlerta_pregunta", 0);
                    comando.Parameters.AddWithValue("@tipoAlerta", 0);
                    comando.Parameters.AddWithValue("@codPreg", "s");
                    comando.Parameters.AddWithValue("@descPreg", "s");
                    comando.Parameters.AddWithValue("@operacion", LISTAR);

                    using(SqlDataReader read = comando.ExecuteReader())
                    {
                        if(read != null)
                        {
                            while (read.Read())
                            {
                                STAP = new Sto_TipoAlerta_Pregunta();
                                STAP.Id_TipoAlerta_Preguntas1 = Convert.ToInt32(read["id_tipoAlerta_pregunta"].ToString());
                                STAP.TipoAlerta1 = Convert.ToInt32(read["TipoAlerta"].ToString());
                                STAP.CodigoPRegunta1 = read["CodigoPregunta"].ToString();
                                STAP.DescripcionPRegunta1 = read["DescripcionPregunta"].ToString();

                                lista.Add(STAP);
                            }
                        }
                    }
                }
            }
            return lista;
        }
        #endregion

        #region LISTAR PREGUNTAS POR CODIGO PREGUNTA
        public Sto_TipoAlerta_Pregunta ListarPreguntasRegistradas(string codPregs)
        {
            PROCEDIMIENTOS = ListaProcedimientos.MAESTRO + ListaProcedimientos.TipoAlertas_Preguntas;
            STAP = new Sto_TipoAlerta_Pregunta();
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@id", 0);
                    comando.Parameters.AddWithValue("@id_tipoAlerta_pregunta", 0);
                    comando.Parameters.AddWithValue("@tipoAlerta", 0);
                    comando.Parameters.AddWithValue("@codPreg", codPregs);
                    comando.Parameters.AddWithValue("@descPreg", "s");
                    comando.Parameters.AddWithValue("@operacion", ListaProcedimientos.OPCION_ESPECIAL_5);

                    using (SqlDataReader read = comando.ExecuteReader())
                    {
                        if (read != null)
                        {
                            if (read.Read())
                            {
                                STAP = new Sto_TipoAlerta_Pregunta();
                                STAP.Id_TipoAlerta_Preguntas1 = Convert.ToInt32(read["id_tipoAlerta_pregunta"].ToString());
                                STAP.TipoAlerta1 = Convert.ToInt32(read["TipoAlerta"].ToString());
                                STAP.CodigoPRegunta1 = read["CodigoPregunta"].ToString();
                                STAP.DescripcionPRegunta1 = read["DescripcionPregunta"].ToString();
                            }
                        }
                    }
                }
            }
            return STAP;
        }
        #endregion

        #region LISTAR PREGUNTAS POR CODIGO TIPO ALERTA
        public List<Sto_TipoAlerta_Pregunta> ListarPreguntasRegistradas(int tipo)
        {
            List<Sto_TipoAlerta_Pregunta> lista = new List<Sto_TipoAlerta_Pregunta>();
            PROCEDIMIENTOS = ListaProcedimientos.MAESTRO + ListaProcedimientos.TipoAlertas_Preguntas;
            STAP = new Sto_TipoAlerta_Pregunta();
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@id", 0);
                    comando.Parameters.AddWithValue("@id_tipoAlerta_pregunta", 0);
                    comando.Parameters.AddWithValue("@tipoAlerta", 0);
                    comando.Parameters.AddWithValue("@codPreg", string.Empty);
                    comando.Parameters.AddWithValue("@descPreg", string.Empty);
                    comando.Parameters.AddWithValue("@tipo", tipo); 
                    comando.Parameters.AddWithValue("@operacion", ListaProcedimientos.OPCION_ESPECIAL_6);

                    using (SqlDataReader read = comando.ExecuteReader())
                    {
                        if (read != null)
                        {
                            while (read.Read())
                            {
                                STAP = new Sto_TipoAlerta_Pregunta();
                                STAP.Id_TipoAlerta_Preguntas1 = Convert.ToInt32(read["id_tipoAlerta_pregunta"].ToString());
                                STAP.TipoAlerta1 = Convert.ToInt32(read["TipoAlerta"].ToString());
                                STAP.CodigoPRegunta1 = read["CodigoPregunta"].ToString();
                                STAP.DescripcionPRegunta1 = read["DescripcionPregunta"].ToString();
                                lista.Add(STAP);
                            }
                        }
                    }
                }
            }
            return lista;
        }
        #endregion

        #region OBTENER PREGUNTAS POR CODIGO PREGUNTA AND TIPO ALERTA
        public int getObtener(int tipo,string preg)
        {
            int rpta = 0;
            PROCEDIMIENTOS = ListaProcedimientos.MAESTRO + ListaProcedimientos.TipoAlertas_Preguntas;
            STAP = new Sto_TipoAlerta_Pregunta();
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@id", 0);
                    comando.Parameters.AddWithValue("@id_tipoAlerta_pregunta", 0);
                    comando.Parameters.AddWithValue("@tipoAlerta", 0);
                    comando.Parameters.AddWithValue("@codPreg", preg);
                    comando.Parameters.AddWithValue("@descPreg", string.Empty);
                    comando.Parameters.AddWithValue("@tipo", tipo);
                    comando.Parameters.AddWithValue("@operacion", ListaProcedimientos.OPCION_ESPECIAL_7);

                    using (SqlDataReader read = comando.ExecuteReader())
                    {
                        if (read != null)
                        {
                            if (read.Read())
                            {
                                rpta = Convert.ToInt32(read["CON"].ToString());
                            }
                        }
                    }
                }
            }
            return rpta;
        }
        #endregion
    }
}
