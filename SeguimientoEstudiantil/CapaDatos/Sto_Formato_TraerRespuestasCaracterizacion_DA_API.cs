﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CapaEntidad;



namespace CapaDatos
{
    public class Sto_Formato_TraerRespuestasCaracterizacion_DA_API
    {


        private string PROCEDIMIENTOS = "";


        public List<Sto_Formato_TraerRespuestasCaracterizacion> lista()
        {
            List<Sto_Formato_TraerRespuestasCaracterizacion> lis = new List<Sto_Formato_TraerRespuestasCaracterizacion>();
            Sto_Formato_TraerRespuestasCaracterizacion ADTRC = null;

            PROCEDIMIENTOS = "zzFormato_TraerRespuestasCaracterizacion";
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@Ambito", string.Empty);
                    comando.Parameters.AddWithValue("@DocIdentidad", string.Empty);
                    using (SqlDataReader read = comando.ExecuteReader())
                    {
                        if (read != null)
                        {
                            while (read.Read())
                            {
                                ADTRC = new Sto_Formato_TraerRespuestasCaracterizacion();
                                ADTRC.FormatoDato = read["DocIDentidad"].ToString();
                                ADTRC.CodigoInterno1 = read["CodigoInterno"].ToString();
                                ADTRC.Respuesta1 = read["respuesta"].ToString();
                                lis.Add(ADTRC);
                            }
                        }
                    }
                }
            }

            return lis;
        }


    }
}
