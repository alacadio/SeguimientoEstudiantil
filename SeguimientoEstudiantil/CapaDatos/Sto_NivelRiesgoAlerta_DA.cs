﻿using CapaDatos.Interfaz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CapaEntidad;

namespace CapaDatos
{
    public class Sto_NivelRiesgoAlerta_DA : Sto_Maestro_DAO<Sto_NivelRiesgoAlerta, int>
    {
        private string PROCEDIMIENTO = "";

        public int Actualizar(Sto_NivelRiesgoAlerta k)
        {
            int rpta = 0;
            PROCEDIMIENTO = ListaProcedimientos.ACTUALIZAR + ListaProcedimientos.NivelRiesgoAlerta;
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTO, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@id", Convert.ToByte(k.NivelRiesgoAlerta1));
                    comando.Parameters.AddWithValue("@nombre", k.Nombre1);

                    rpta = comando.ExecuteNonQuery();

                }

            }
            return rpta;
        }

        public int Crear(Sto_NivelRiesgoAlerta k)
        {
            int rpta = 0;
            PROCEDIMIENTO = ListaProcedimientos.CREAR + ListaProcedimientos.NivelRiesgoAlerta;
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using(SqlCommand comando = new SqlCommand(PROCEDIMIENTO, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@nombre", k.Nombre1);

                    rpta = comando.ExecuteNonQuery();

                }

            }
            return rpta;
        }

        public int Eliminar(int k)
        {
            int rpta = 0;
            PROCEDIMIENTO = ListaProcedimientos.ELIMINAR + ListaProcedimientos.NivelRiesgoAlerta;
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTO, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@id",  Convert.ToByte(k));

                    rpta = comando.ExecuteNonQuery();

                }

            }
            return rpta;
        }

        public Sto_NivelRiesgoAlerta getOne(int k)
        {
            Sto_NivelRiesgoAlerta SNRA = null;
            PROCEDIMIENTO = ListaProcedimientos.LISTAR + ListaProcedimientos.NivelRiesgoAlerta + ListaProcedimientos.ByID;
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using(SqlCommand comando = new SqlCommand(PROCEDIMIENTO,con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@id", k);
                    using(SqlDataReader read = comando.ExecuteReader())
                    {
                        if(read != null)
                        {
                            if (read.Read())
                            {
                                SNRA = new Sto_NivelRiesgoAlerta();
                                SNRA.NivelRiesgoAlerta1 = Convert.ToInt32(read["NivelRiesgoAlerta"].ToString());
                                SNRA.Nombre1 = read["Nombre"].ToString();
                            }
                        }
                    }
                }
            }
            return SNRA;

        }

        public List<Sto_NivelRiesgoAlerta> Listar()
        {
            List<Sto_NivelRiesgoAlerta> lista = new List<Sto_NivelRiesgoAlerta>();
            Sto_NivelRiesgoAlerta SNRA = null;
            PROCEDIMIENTO = ListaProcedimientos.LISTAR + ListaProcedimientos.NivelRiesgoAlerta;
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTO, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    using (SqlDataReader read = comando.ExecuteReader())
                    {
                        if (read != null)
                        {
                            while (read.Read())
                            {
                                SNRA = new Sto_NivelRiesgoAlerta();
                                SNRA.NivelRiesgoAlerta1 = Convert.ToInt32(read["NivelRiesgoAlerta"].ToString());
                                SNRA.Nombre1 = read["Nombre"].ToString();
                                lista.Add(SNRA);
                            }
                        }
                    }
                }
            }
            return lista;
        }
    }
}

