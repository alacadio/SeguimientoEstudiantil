﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CapaEntidad;

namespace CapaDatos
{
    public class Sto_TiposAsesores_DA
    {

        #region VARIABLES GLOBALES DE LA CLASE
        /// <summary>
        /// Variable de inicializacion de los nombre de los procedimiento.
        /// </summary>
        private string PROCEDIMIENTOS = "";
        /// <summary>
        /// Variable de las diferentes operaciones que realiza el procedimiento.
        /// </summary>
        private int LISTAR = ListaProcedimientos.OPCION_LISTAR;
        private int LISTAR_ByID = ListaProcedimientos.OPCION_LISTAR_BYID;
        private int CREAR = ListaProcedimientos.OPCION_CREAR;
        private int ACTUALIZAR = ListaProcedimientos.OPCION_ACTUALIZAR;
        private int ELIMINAR = ListaProcedimientos.OPCION_ELIMINAR;
        /// <summary>
        /// Declaraion de la instancia a la clase
        /// </summary>
        private Sto_TiposAsesores STA;
        #endregion

        #region REGISTRAR NUEVO TIPOS ASESORES
        public int resgistrarTipoAsesor(Sto_TiposAsesores st)
        {
            int rpta = 0;
            PROCEDIMIENTOS = ListaProcedimientos.MAESTRO + ListaProcedimientos.TiposAsesores;
            try
            {
                using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
                {
                    con.Open();
                    using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                    {
                        comando.CommandType = CommandType.StoredProcedure;
                        SqlParameter id = comando.Parameters.AddWithValue("@id", 0);
                        id.Direction = ParameterDirection.InputOutput;
                        comando.Parameters.AddWithValue("@TipoAsesor", st.Sto_TipoAsesor);
                        comando.Parameters.AddWithValue("@nomb", st.Nombre);
                        comando.Parameters.AddWithValue("@email", st.Email);
                        comando.Parameters.AddWithValue("@op", CREAR);

                        comando.ExecuteNonQuery();
                        rpta = Convert.ToInt32(comando.Parameters["@id"].Value);
                    }
                }
                return rpta;
            }
            catch(Exception e)
            {
                return rpta;
            }
           
        }
        #endregion

        #region ACTUALIZAR TIPOS ASESORES
        public int actualizarTipoAsesor(Sto_TiposAsesores st)
        {
            int rpta = 0;
            PROCEDIMIENTOS = ListaProcedimientos.MAESTRO + ListaProcedimientos.TiposAsesores;
            try
            {
                using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
                {
                    con.Open();
                    using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                    {
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@id", 0);
                        comando.Parameters.AddWithValue("@TipoAsesor", st.Sto_TipoAsesor);
                        comando.Parameters.AddWithValue("@nomb", st.Nombre);
                        comando.Parameters.AddWithValue("@email", st.Email);
                        comando.Parameters.AddWithValue("@op", ACTUALIZAR);

                        rpta = comando.ExecuteNonQuery();
                    }
                }
                return rpta;
            }
            catch (Exception e)
            {
                return rpta;
            }
        }
        #endregion

        #region ELIMINAR TIPOS ASESORES
        public int eliminarTipoAsesor(Sto_TiposAsesores st)
        {
            int rpta = 0;
            PROCEDIMIENTOS = ListaProcedimientos.MAESTRO + ListaProcedimientos.TiposAsesores;
            try
            {
                using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
                {
                    con.Open();
                    using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                    {
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@id", 0);
                        comando.Parameters.AddWithValue("@TipoAsesor", st.Sto_TipoAsesor);
                        comando.Parameters.AddWithValue("@nomb", st.Nombre);
                        comando.Parameters.AddWithValue("@email", st.Email);
                        comando.Parameters.AddWithValue("@op", ELIMINAR);

                        rpta = comando.ExecuteNonQuery();
                    }
                }
                return rpta;
            }
            catch(Exception e)
            {
                return rpta;
            }
        }
        #endregion

        #region LISTAR TIPOS ASESORES
        public List<Sto_TiposAsesores> listaTipoAsesor()
        {
            List<Sto_TiposAsesores> lista = new List<Sto_TiposAsesores>();
            PROCEDIMIENTOS = ListaProcedimientos.MAESTRO + ListaProcedimientos.TiposAsesores;
            STA = new Sto_TiposAsesores();
            try
            {
                using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
                {
                    con.Open();
                    using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                    {
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@id", 0);
                        comando.Parameters.AddWithValue("@TipoAsesor", 0);
                        comando.Parameters.AddWithValue("@nomb", string.Empty);
                        comando.Parameters.AddWithValue("@email", string.Empty);
                        comando.Parameters.AddWithValue("@op", LISTAR);
                        using (SqlDataReader read = comando.ExecuteReader())
                        {
                            if (read != null)
                            {
                                while (read.Read())
                                {
                                    STA = new Sto_TiposAsesores();
                                    STA.Sto_TipoAsesor = Convert.ToInt32(read["Sto_TipoAsesor"].ToString());
                                    STA.Nombre = read["Nombre"].ToString();
                                    STA.Email = read["email"].ToString();
                                    lista.Add(STA);
                                }
                            }
                        }
                    }
                }
                return lista;
            }
            catch(Exception e)
            {
                lista.Add(STA);
                return lista;
            }
        }
        #endregion

        #region LISTAR TIPOS ASESORES POR ID
        public Sto_TiposAsesores listaTipoAsesor_ByID(int id)
        {
            PROCEDIMIENTOS = ListaProcedimientos.MAESTRO + ListaProcedimientos.TiposAsesores;
            STA = new Sto_TiposAsesores();
            try
            {
                using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
                {
                    con.Open();
                    using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                    {
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@id", 0);
                        comando.Parameters.AddWithValue("@TipoAsesor", id);
                        comando.Parameters.AddWithValue("@nomb", string.Empty);
                        comando.Parameters.AddWithValue("@email", string.Empty);
                        comando.Parameters.AddWithValue("@op", LISTAR_ByID);
                        using (SqlDataReader read = comando.ExecuteReader())
                        {
                            if (read != null)
                            {
                                if (read.Read())
                                {
                                    STA = new Sto_TiposAsesores();
                                    STA.Sto_TipoAsesor = Convert.ToInt32(read["Sto_TipoAsesor"].ToString());
                                    STA.Nombre = read["Nombre"].ToString();
                                    STA.Email = read["email"].ToString();
                                }
                            }
                        }
                    }
                }
                return STA;
            }
            catch(Exception e)
            {
                return STA;
            }
        }
        #endregion
    }
}
