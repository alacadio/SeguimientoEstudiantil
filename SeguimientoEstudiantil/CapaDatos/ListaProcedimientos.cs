﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos
{
    public class ListaProcedimientos
    {
        //////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Ecabezado y final de los procedimientos.
        /// </summary>
        public static string CREAR           =   "Sto_crear_";
        public static string ACTUALIZAR      =   "Sto_actualizar_";
        public static string ELIMINAR        =   "Sto_eliminar_";
        public static string LISTAR          =   "Sto_listar_";
        public static string ByID            =   "_byID";
        public static string MAESTRO         =   "Sto_maestro_";
        //////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Nombre de los procedimientos almacenados.
        /// </summary>
        public static string TipoAcciones    =   "TipoAccion";
        public static string TiposAlertas    =   "TipoAlerta";
        public static string Accion_TipoAlerta_NivelRiesgo = "Accion_TipoAlerta_NivelRiesgo";
        public static string TipoAlerta_NivelRiesgo = "TipoAlerta_NivelRiesgo";
        public static string NivelRiesgoAlerta = "NivelesRiesgoAlertas";
        public static string TipoAlertas_Preguntas = "TipoAlerta_Preguntas";
        public static string Alerta_Base = "AlertaBase";
        public static string Pregunta_ValoracionRiesgoRespuesta = "Pregunta_ValoracionRiesgoRespuesta";
        public static string TiposAsesores = "TiposAsesores";
        public static string TiposAsesoresUsuario = "TipoAsesor_Usuario";
        public static string RegistroEmail = "RegistroEmail";
        /// <summary>
        /// Procedimientos Temporales
        /// </summary>
        public static string zzFormato_TraerRespuestasCaracterizacion = "zzFormato_TraerRespuestasCaracterizacion";
        public static string Formato_Caracterizacion_Preguntas = "Formato_Caracterizacion_Preguntas";
        public static string Formato_TraerCaracterizacionRespuestas = "Formato_TraerCaracterizacionRespuestas";

        /////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Nombre de las opciones para los procedimientos maestros
        /// </summary>
        public static int OPCION_CREAR = 1;
        public static int OPCION_ACTUALIZAR = 2;
        public static int OPCION_LISTAR = 3;
        public static int OPCION_LISTAR_BYID = 4;
        public static int OPCION_ELIMINAR = 0;
        public static int OPCION_ESPECIAL_5 = 5;
        public static int OPCION_ESPECIAL_6 = 6;
        public static int OPCION_ESPECIAL_7 = 7;
        /////////////////////////////////////////////////////////////////////

    }
}
