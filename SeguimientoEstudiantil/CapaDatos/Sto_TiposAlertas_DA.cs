﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CapaEntidad;

namespace CapaDatos
{
    public class Sto_TiposAlertas_DA
    {
        #region VARIABLES GLOBALES DE LA CLASE
        /// <summary>
        /// Variable de inicializacion de los nombre de los procedimiento.
        /// </summary>
        private string PROCEDIMIENTO = "";
        /// <summary>
        /// Variable de las diferentes operaciones que realiza el procedimiento.
        /// </summary>
        private int LISTAR = ListaProcedimientos.OPCION_LISTAR;
        private int LISTAR_ByID = ListaProcedimientos.OPCION_LISTAR_BYID;
        private int CREAR = ListaProcedimientos.OPCION_CREAR;
        private int ACTUALIZAR = ListaProcedimientos.OPCION_ACTUALIZAR;
        private int ELIMINAR = ListaProcedimientos.OPCION_ELIMINAR;
        #endregion

        #region LISTAR TODOS LOS TIPOS DE ALERTAS
        public List<Sto_TiposAlertas> listaTiposAlertas()
        {
            List<Sto_TiposAlertas> lista = new List<Sto_TiposAlertas>();
            PROCEDIMIENTO = ListaProcedimientos.MAESTRO + ListaProcedimientos.TiposAlertas;
            Sto_TiposAlertas STA = null;
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using(SqlCommand comando = new SqlCommand(PROCEDIMIENTO, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@TipoAlerta", 0);
                    comando.Parameters.AddWithValue("@nombre", string.Empty);
                    comando.Parameters.AddWithValue("@AlertaBase", 0);
                    comando.Parameters.AddWithValue("@op", LISTAR);
                    comando.Parameters.AddWithValue("@id ", 0);
                    using (SqlDataReader read = comando.ExecuteReader())
                    {
                        if(read != null)
                        {
                            while (read.Read())
                            {
                                STA = new Sto_TiposAlertas();
                                STA.TipoAlerta1 = Convert.ToInt32(read["TipoAlerta"]);
                                STA.Nombre1 = read["Nombre"].ToString();
                                STA.AlertaBase = Convert.ToInt32(read["Alerta_base"].ToString());
                                lista.Add(STA);
                            }
                        }
                    }
                }
            }
            return lista;
        }
        #endregion

        #region CREAR UN NUEVO TIPO DE ALERTA
        public int CrearTiposAlertas(Sto_TiposAlertas STA)
        {
            PROCEDIMIENTO = ListaProcedimientos.MAESTRO + ListaProcedimientos.TiposAlertas;
            int rpta = 0;
            using(SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using(SqlCommand comando = new SqlCommand(PROCEDIMIENTO, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@TipoAlerta", 0);
                    comando.Parameters.AddWithValue("@nombre", STA.Nombre1);
                    comando.Parameters.AddWithValue("@AlertaBase", STA.AlertaBase);
                    comando.Parameters.AddWithValue("@op", CREAR);
                    comando.Parameters.AddWithValue("@id ", 0);

                    rpta = comando.ExecuteNonQuery();
                }
            }
            return rpta;
        }
        #endregion

        #region ACTUALIZA LOS TIPOS DE ALERTAS
        public int ActualizarTiposAlertas(Sto_TiposAlertas STA)
        {
            PROCEDIMIENTO = ListaProcedimientos.ACTUALIZAR + ListaProcedimientos.TiposAlertas;
            int rpta = 0;
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTO, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@TipoAlerta", STA.TipoAlerta1);
                    comando.Parameters.AddWithValue("@nombre", STA.Nombre1);
                    comando.Parameters.AddWithValue("@AlertaBase", STA.AlertaBase);
                    comando.Parameters.AddWithValue("@op", ACTUALIZAR);
                    comando.Parameters.AddWithValue("@id ", 0);

                    rpta = comando.ExecuteNonQuery();
                }
            }
            return rpta;
        }
        #endregion

        #region ELIMINA LOS TIPOS DE ALERTAS
        public int EliminarTiposAlertas(int id)
        {
            PROCEDIMIENTO = ListaProcedimientos.MAESTRO + ListaProcedimientos.TiposAlertas;
            int rpta = 0;
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTO, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@TipoAlerta", id);
                    comando.Parameters.AddWithValue("@nombre", string.Empty);
                    comando.Parameters.AddWithValue("@AlertaBase", 0);
                    comando.Parameters.AddWithValue("@op", ELIMINAR);
                    comando.Parameters.AddWithValue("@id ", 0);
                    rpta = comando.ExecuteNonQuery();
                }
            }
            return rpta;
        }
        #endregion

        #region SELECCIONA UN TIPO DE LERTA
        public Sto_TiposAlertas getOne(int id)
        {
            PROCEDIMIENTO = ListaProcedimientos.MAESTRO + ListaProcedimientos.TiposAlertas;
            Sto_TiposAlertas STA = null;
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTO, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@TipoAlerta", id);
                    comando.Parameters.AddWithValue("@nombre", string.Empty);
                    comando.Parameters.AddWithValue("@AlertaBase", 0);
                    comando.Parameters.AddWithValue("@op", LISTAR_ByID);
                    comando.Parameters.AddWithValue("@id ", 0);
                    using (SqlDataReader read = comando.ExecuteReader())
                    {
                        if (read != null)
                        {
                            while (read.Read())
                            {
                                STA = new Sto_TiposAlertas();
                                STA.TipoAlerta1 = Convert.ToInt32(read["TipoAlerta"]);
                                STA.Nombre1 = read["Nombre"].ToString();
                                STA.AlertaBase = Convert.ToInt32(read["Alerta_base"].ToString());
                            }
                        }
                    }
                }
            }
            return STA;
        }
        #endregion
    }
}
