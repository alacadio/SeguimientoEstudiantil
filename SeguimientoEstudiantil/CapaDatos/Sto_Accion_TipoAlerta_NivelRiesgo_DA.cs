﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CapaEntidad;

namespace CapaDatos
{
    public class Sto_Accion_TipoAlerta_NivelRiesgo_DA
    {
        private string PROCEDIMIENTOS = "";

        public List<Sto_Accion_TipoAlerta_NivelRiesgo> listarAccionTipoAlerta()
        {
            PROCEDIMIENTOS = ListaProcedimientos.LISTAR + ListaProcedimientos.Accion_TipoAlerta_NivelRiesgo;
            Sto_Accion_TipoAlerta_NivelRiesgo SATANR = null;
            List<Sto_Accion_TipoAlerta_NivelRiesgo> lista = new List<Sto_Accion_TipoAlerta_NivelRiesgo>();

            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    using (SqlDataReader read = comando.ExecuteReader())
                    {
                        if (read != null)
                        {
                            while (read.Read())
                            {
                                SATANR = new Sto_Accion_TipoAlerta_NivelRiesgo();
                                SATANR.ID_Accion_TipoAlerta_NivelRiesgo1 = Convert.ToInt32(read["id_Accion_TipoAlerta_NivelRiesgo"].ToString());
                                SATANR.ID_Accion1 = Convert.ToInt32(read["id_Accion"].ToString());
                                SATANR.ID_TipoAlerta_NivelRiesgo1 = Convert.ToInt64(read["id_Accion"].ToString());
                                SATANR.Referencias1 = read["Referencia"].ToString();
                                lista.Add(SATANR);
                            }
                        }
                    }
                }
            }

            return lista;
        }

        public int CrearAccionTipoAlerta(Sto_Accion_TipoAlerta_NivelRiesgo st)
        {
            PROCEDIMIENTOS = ListaProcedimientos.CREAR + ListaProcedimientos.Accion_TipoAlerta_NivelRiesgo;
            int rpta = 0;
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@id_accion", st.ID_Accion1);
                    comando.Parameters.AddWithValue("@id_TipoAlerta_NivelRiesgo", st.ID_TipoAlerta_NivelRiesgo1);
                    comando.Parameters.AddWithValue("@refe", st.Referencias1);
                    comando.Parameters.AddWithValue("@id", 0);
                    rpta = comando.ExecuteNonQuery();
                }
            }
            return rpta;
        }


        public int ActualizarAccionTipoAlerta(Sto_Accion_TipoAlerta_NivelRiesgo st)
        {
            PROCEDIMIENTOS = ListaProcedimientos.ACTUALIZAR + ListaProcedimientos.Accion_TipoAlerta_NivelRiesgo;
            int rpta = 0;
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@id_Accion_TipoAlerta_NivelRiesgo", st.ID_Accion_TipoAlerta_NivelRiesgo1);
                    comando.Parameters.AddWithValue("@id_accion", st.ID_Accion1);
                    comando.Parameters.AddWithValue("@id_TipoAlerta_NivelRiesgo", st.ID_TipoAlerta_NivelRiesgo1);
                    comando.Parameters.AddWithValue("@refe", st.Referencias1);
                    comando.Parameters.AddWithValue("@id", 0);
                    rpta = comando.ExecuteNonQuery();
                }
            }
            return rpta;
        }

        public Sto_Accion_TipoAlerta_NivelRiesgo getAccionTipoAlerta(long st)
        {
            PROCEDIMIENTOS = "Sto_listar_Accion_TipoAlerta_NivelRiesgo_byID_D";
            Sto_Accion_TipoAlerta_NivelRiesgo SATANR = new Sto_Accion_TipoAlerta_NivelRiesgo();

            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@id_d", st);
                    using (SqlDataReader read = comando.ExecuteReader())
                    {
                        if (read != null)
                        {
                            if (read.Read())
                            {
                                SATANR = new Sto_Accion_TipoAlerta_NivelRiesgo();
                                SATANR.ID_Accion_TipoAlerta_NivelRiesgo1 = Convert.ToInt32(read["id_Accion_TipoAlerta_NivelRiesgo"].ToString());
                                SATANR.ID_Accion1 = Convert.ToInt32(read["id_Accion"].ToString());
                                SATANR.ID_TipoAlerta_NivelRiesgo1 = Convert.ToInt64(read["id_Accion"].ToString());
                                SATANR.Referencias1 = read["Referencia"].ToString();
                            }
                        }
                    }
                }
            }

            return SATANR;
        }
    }
}
