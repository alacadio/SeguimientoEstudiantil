﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidad;
using System.Data;
using System.Data.SqlClient;

namespace CapaDatos
{
    public class Sto_Formato_Caracterizacion_Preguntas_DA_API
    {

        private string PROCEDIMIENTO = "";

        public List<Sto_Formato_Caracterizacion_Preguntas> listar()
        {
            List<Sto_Formato_Caracterizacion_Preguntas> lista = new List<Sto_Formato_Caracterizacion_Preguntas>();
            Sto_Formato_Caracterizacion_Preguntas SFCP = null;
            PROCEDIMIENTO = "Formato_Caracterizacion_Preguntas";
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using(SqlCommand comando = new SqlCommand(PROCEDIMIENTO, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    /*comando.Parameters.AddWithValue("@Ambito", ambito);
                    comando.Parameters.AddWithValue("@DocIdentidad", DocIdentidad);*/
                    using(SqlDataReader read = comando.ExecuteReader())
                    {
                        if(read != null)
                        {
                            while (read.Read())
                            {
                                SFCP = new Sto_Formato_Caracterizacion_Preguntas();
                                SFCP.CodigoInterno1 = read["CodigoInterno"].ToString();
                                SFCP.DetallePregunta1 = read["DetallePregunta"].ToString();
                                SFCP.TipoPreguntaRespuesta1 = read["TipoPreguntaRespuesta"].ToString();
                                SFCP.MultiValuado1 = Convert.ToBoolean(read["MultiValuado"].ToString());
                                lista.Add(SFCP);
                            }
                        }
                    }
                }
            }
            return lista;
        }
        
        public string getNombrePregunta(string id)
        {
            string rpta = "";
            PROCEDIMIENTO = "Formato_Caracterizacion_Preguntas";
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTO, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@Ambito", id);
                    comando.Parameters.AddWithValue("@op", 1);
                    using (SqlDataReader read = comando.ExecuteReader())
                    {
                        if (read != null)
                        {
                            if (read.Read())
                            {
                                rpta = read["DetallePregunta"].ToString();
                            }
                        }
                    }
                }
            }

            return rpta;
        }
    }
}
