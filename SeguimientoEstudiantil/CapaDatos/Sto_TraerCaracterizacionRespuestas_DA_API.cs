﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CapaEntidad;

namespace CapaDatos
{
    public class Sto_TraerCaracterizacionRespuestas_DA_API
    {
        private string PROCEDIMIENTO = "";

        public List<Sto_TraerCaracterizacionRespuestas> listar_TraerCaracterizacionRespuestas(string cod,string ambito)
        {

            List<Sto_TraerCaracterizacionRespuestas> lista = new List<Sto_TraerCaracterizacionRespuestas>();
            Sto_TraerCaracterizacionRespuestas STCR = null;
            PROCEDIMIENTO = ListaProcedimientos.Formato_TraerCaracterizacionRespuestas;
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTO, con)) 
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@Ambito",ambito);
                    if(cod != null)
                    {
                        comando.Parameters.AddWithValue("@CodigoInterno", cod);
                    }else
                    {
                        STCR = new Sto_TraerCaracterizacionRespuestas();
                        STCR.CodigoInterno1 ="Seleccione una opcion";
                        STCR.Detalles = "Seleccione una opcion";
                        STCR.Respuesta = "Seleccione una opcion";
                        lista.Add(STCR);
                        return lista;
                    }
                    
                    using (SqlDataReader read = comando.ExecuteReader())
                    {
                        if(read != null)
                        {
                            while (read.Read())
                            {
                                STCR = new Sto_TraerCaracterizacionRespuestas();
                                STCR.CodigoInterno1 = read["CodigoInterno"].ToString();
                                STCR.Detalles = read["Detalle"].ToString();
                                STCR.Respuesta = read["respuesta"].ToString();
                                lista.Add(STCR);
                            }
                        }
                    }

                }

            }
            return lista;
        }
    }
}
