﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos.Interfaz
{
    interface Sto_Maestro_DAO<T,K>
    {
        int Crear(T k);
        int Actualizar(T k);
        int Eliminar(K k);
        T getOne(K k);
        List<T> Listar();
    }
}
