﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CapaEntidad;

namespace CapaDatos
{
    public class Sto_TiposAsesor_Usuario_DA
    {

        #region VARIABLES GLOBALES DE LA CLASE
        /// <summary>
        /// Variable de inicializacion de los nombre de los procedimiento.
        /// </summary>
        private string PROCEDIMIENTOS = "";
        /// <summary>
        /// Variable de las diferentes operaciones que realiza el procedimiento.
        /// </summary>
        private int LISTAR = ListaProcedimientos.OPCION_LISTAR;
        private int LISTAR_ByID = ListaProcedimientos.OPCION_LISTAR_BYID;
        private int CREAR = ListaProcedimientos.OPCION_CREAR;
        private int ACTUALIZAR = ListaProcedimientos.OPCION_ACTUALIZAR;
        private int ELIMINAR = ListaProcedimientos.OPCION_ELIMINAR;
        /// <summary>
        /// Declaraion de la instancia a la clase
        /// </summary>
        private Sto_TipoAsesor_Usuario STAU;
        #endregion

        #region REGISTRAR NUEVO TIPO ASESOR USUARIO
        public int resgistrarTipoAsesorUsuario(Sto_TipoAsesor_Usuario st)
        {
            int rpta = 0;
            PROCEDIMIENTOS = ListaProcedimientos.MAESTRO + ListaProcedimientos.TiposAsesoresUsuario;
            try
            {
                using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
                {
                    con.Open();
                    using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                    {
                        comando.CommandType = CommandType.StoredProcedure;
                        SqlParameter id = comando.Parameters.AddWithValue("@id", 0);
                        id.Direction = ParameterDirection.InputOutput;
                        comando.Parameters.AddWithValue("@Id_tipo_asesor_usuario", st.Id_TipoAsesor_Usuario);
                        comando.Parameters.AddWithValue("@usuario", st.Usuario);
                        comando.Parameters.AddWithValue("@TipoAsesor", st.TipoAsesor);
                        comando.Parameters.AddWithValue("@op", CREAR);

                        comando.ExecuteNonQuery();
                        rpta = Convert.ToInt32(comando.Parameters["@id"].Value);
                    }
                }
                return rpta;
            }
            catch (Exception e)
            {
                return rpta;
            }

        }
        #endregion

        #region ACTUALIZAR TIPO ASESOR USUARIO
        public int actualizarTipoAsesorUsuario(Sto_TipoAsesor_Usuario st)
        {
            int rpta = 0;
            PROCEDIMIENTOS = ListaProcedimientos.MAESTRO + ListaProcedimientos.TiposAsesoresUsuario;
            try
            {
                using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
                {
                    con.Open();
                    using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                    {
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@id", 0);
                        comando.Parameters.AddWithValue("@Id_tipo_asesor_usuario", st.Id_TipoAsesor_Usuario);
                        comando.Parameters.AddWithValue("@usuario", st.Usuario);
                        comando.Parameters.AddWithValue("@TipoAsesor", st.TipoAsesor);
                        comando.Parameters.AddWithValue("@op", ACTUALIZAR);

                        rpta = comando.ExecuteNonQuery();
                    }
                }
                return rpta;
            }
            catch (Exception e)
            {
                return rpta;
            }
        }
        #endregion

        #region ELIMINAR TIPO ASESOR USUARIO
        public int eliminarTipoAsesorUsuario(Sto_TipoAsesor_Usuario st)
        {
            int rpta = 0;
            PROCEDIMIENTOS = ListaProcedimientos.MAESTRO + ListaProcedimientos.TiposAsesoresUsuario;
            try
            {
                using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
                {
                    con.Open();
                    using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                    {
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@id", 0);
                        comando.Parameters.AddWithValue("@Id_tipo_asesor_usuario", st.Id_TipoAsesor_Usuario);
                        comando.Parameters.AddWithValue("@usuario", st.Usuario);
                        comando.Parameters.AddWithValue("@TipoAsesor", st.TipoAsesor);
                        comando.Parameters.AddWithValue("@op", ELIMINAR);

                        rpta = comando.ExecuteNonQuery();
                    }
                }
                return rpta;
            }
            catch (Exception e)
            {
                return rpta;
            }
        }
        #endregion

        #region LISTAR TIPOS ASESORES
        public List<Sto_TipoAsesor_Usuario> listaTipoAsesorUsuario()
        {
            List<Sto_TipoAsesor_Usuario> lista = new List<Sto_TipoAsesor_Usuario>();
            PROCEDIMIENTOS = ListaProcedimientos.MAESTRO + ListaProcedimientos.TiposAsesoresUsuario;
            STAU = new Sto_TipoAsesor_Usuario();
            try
            {
                using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
                {
                    con.Open();
                    using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                    {
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@id", 0);
                        comando.Parameters.AddWithValue("@Id_tipo_asesor_usuario", 0);
                        comando.Parameters.AddWithValue("@usuario", string.Empty);
                        comando.Parameters.AddWithValue("@TipoAsesor", string.Empty);
                        comando.Parameters.AddWithValue("@op", LISTAR);
                        using (SqlDataReader read = comando.ExecuteReader())
                        {
                            if (read != null)
                            {
                                while (read.Read())
                                {
                                    STAU = new Sto_TipoAsesor_Usuario();
                                    STAU.Id_TipoAsesor_Usuario = Convert.ToInt64(read["Id_tipo_asesor_usuario"].ToString());
                                    STAU.Usuario = read["Usuario"].ToString();
                                    STAU.TipoAsesor = Convert.ToInt32(read["TipoAsesor"].ToString());
                                    lista.Add(STAU);
                                }
                            }
                        }
                    }
                }
                return lista;
            }
            catch(Exception e)
            {
                lista.Add(STAU);
                return lista;
            }
        }
        #endregion

        #region LISTAR TIPOS ASESORES POR ID
        public Sto_TipoAsesor_Usuario listaTipoAsesor_ByID(long id)
        {
            PROCEDIMIENTOS = ListaProcedimientos.MAESTRO + ListaProcedimientos.TiposAsesoresUsuario;
            STAU = new Sto_TipoAsesor_Usuario();
            try
            {
                using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
                {
                    con.Open();
                    using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                    {
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@id", 0);
                        comando.Parameters.AddWithValue("@Id_tipo_asesor_usuario", id);
                        comando.Parameters.AddWithValue("@usuario", string.Empty);
                        comando.Parameters.AddWithValue("@TipoAsesor", string.Empty);
                        comando.Parameters.AddWithValue("@op", LISTAR_ByID);
                        using (SqlDataReader read = comando.ExecuteReader())
                        {
                            if (read != null)
                            {
                                if (read.Read())
                                {
                                    STAU = new Sto_TipoAsesor_Usuario();
                                    STAU.Id_TipoAsesor_Usuario = Convert.ToInt64(read["Id_tipo_asesor_usuario"].ToString());
                                    STAU.Usuario = read["Usuario"].ToString();
                                    STAU.TipoAsesor = Convert.ToInt32(read["TipoAsesor"].ToString());
                                }
                            }
                        }
                    }
                }
                return STAU;
            }
            catch (Exception e)
            {
                return STAU;
            }
        }
        #endregion
    }
}
