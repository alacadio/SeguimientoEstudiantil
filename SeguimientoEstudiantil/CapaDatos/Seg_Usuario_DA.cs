﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CapaEntidad;

namespace CapaDatos
{
    public class Seg_Usuario_DA
    {

        #region VARIABLES GLOBALES DE LA CLASE
        /// <summary>
        /// Variable de inicializacion de los nombre de los procedimiento.
        /// </summary>
        private string PROCEDIMIENTOS = "";
        /// <summary>
        /// Variable de las diferentes operaciones que realiza el procedimiento.
        /// </summary>
        private int LISTAR = ListaProcedimientos.OPCION_LISTAR;
        private int LISTAR_ByID = ListaProcedimientos.OPCION_LISTAR_BYID;
        private int CREAR = ListaProcedimientos.OPCION_CREAR;
        private int ACTUALIZAR = ListaProcedimientos.OPCION_ACTUALIZAR;
        private int ELIMINAR = ListaProcedimientos.OPCION_ELIMINAR;
        /// <summary>
        /// Declaraion de la instancia a la clase
        /// </summary>
        private Seg_Usuario SEU;
        #endregion

        #region REGISTRAR UN NUEVO USUARIO
        public int registrarUsuario(Seg_Usuario seg)
        {
            int rpta = 0;
            PROCEDIMIENTOS = "Seg_maestro_Usuarios";
             using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
                {
                    con.Open();
                    using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                    {
                        comando.CommandType = CommandType.StoredProcedure;
                        SqlParameter id = comando.Parameters.AddWithValue("@id", 0);
                        id.Direction = ParameterDirection.InputOutput;
                        comando.Parameters.AddWithValue("@usu", seg.Usuario);
                        comando.Parameters.AddWithValue("@nom", seg.Nombre);
                        comando.Parameters.AddWithValue("@act", seg.Activo);
                        comando.Parameters.AddWithValue("@op", CREAR);

                        comando.ExecuteNonQuery();
                        rpta = Convert.ToInt32(comando.Parameters["@id"].Value);
                    }
                }
                return rpta;
        }
        #endregion

        #region ACTUALIZAR USUARIO
        public int actualizarUsuario(Seg_Usuario seg)
        {
            int rpta = 0;
            PROCEDIMIENTOS = "Seg_maestro_Usuarios";
            try
            {
                using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
                {
                    con.Open();
                    using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                    {
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@id", 0);
                        comando.Parameters.AddWithValue("@usu", seg.Usuario);
                        comando.Parameters.AddWithValue("@nom", seg.Nombre);
                        comando.Parameters.AddWithValue("@act", seg.Activo);
                        comando.Parameters.AddWithValue("@op", ACTUALIZAR);

                        rpta = comando.ExecuteNonQuery();
                    }
                }
                return rpta;
            }
            catch (Exception e)
            {
                return rpta;
            }
        }
        #endregion

        #region ELIMINAR USUARIO
        public int eliminarUsuario(string seg)
        {
            int rpta = 0;
            PROCEDIMIENTOS = "Seg_maestro_Usuarios";
            try
            {
                using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
                {
                    con.Open();
                    using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                    {
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@id", 0);
                        comando.Parameters.AddWithValue("@usu", seg);
                        comando.Parameters.AddWithValue("@nom", string.Empty);
                        comando.Parameters.AddWithValue("@act", 0);
                        comando.Parameters.AddWithValue("@op", ELIMINAR);

                        rpta = comando.ExecuteNonQuery();
                    }
                }
                return rpta;
            }
            catch (Exception e)
            {
                return rpta;
            }
        }
        #endregion

        #region OBTENER UN USUARIO POR DOCUMENTO
        public Seg_Usuario obtenerUsuario(string seg)
        {
            PROCEDIMIENTOS = "Seg_maestro_Usuarios";
            try
            {
                using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
                {
                    con.Open();
                    using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                    {
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@id", 0);
                        comando.Parameters.AddWithValue("@usu", seg);
                        comando.Parameters.AddWithValue("@nom", string.Empty);
                        comando.Parameters.AddWithValue("@act", 0);
                        comando.Parameters.AddWithValue("@op", LISTAR_ByID);

                        using (SqlDataReader read = comando.ExecuteReader())
                        {
                            if (read != null)
                            {
                                if (read.Read())
                                {
                                    SEU = new Seg_Usuario();
                                    SEU.Usuario = read["Usuario"].ToString();
                                    SEU.Nombre = read["Nombre"].ToString();
                                    SEU.Activo = Convert.ToBoolean(read["Activo"].ToString());
                                }
                            }
                        }
                    }
                }
                return SEU;
            }
            catch (Exception e)
            {
                return SEU;
            }
        }
        #endregion

        #region LISTAR TODOS LOS USUARIOS
        public List<Seg_Usuario> listarUsuario()
        {
            List<Seg_Usuario> lista = new List<Seg_Usuario>();
            PROCEDIMIENTOS = "Seg_maestro_Usuarios";
            try
            {
                using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
                {
                    con.Open();
                    using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                    {
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@id", 0);
                        comando.Parameters.AddWithValue("@usu", string.Empty);
                        comando.Parameters.AddWithValue("@nom", string.Empty);
                        comando.Parameters.AddWithValue("@act", 0);
                        comando.Parameters.AddWithValue("@op", LISTAR);

                        using (SqlDataReader read = comando.ExecuteReader())
                        {
                            if (read != null)
                            {
                                while (read.Read())
                                {
                                    SEU = new Seg_Usuario();
                                    SEU.Usuario = read["Usuario"].ToString();
                                    SEU.Nombre = read["Nombre"].ToString();
                                    SEU.Activo = Convert.ToBoolean(read["Activo"].ToString());
                                    lista.Add(SEU);
                                }
                            }
                        }
                    }
                }
                return lista;
            }
            catch (Exception e)
            {
                return lista;
            }
        }
        #endregion

    }
}
