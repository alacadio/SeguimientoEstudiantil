﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CapaEntidad;

namespace CapaDatos
{
    public class Sto_TipoAlerta_NivelRiesgo_DA
    {
        private string PROCEDIMIENTO = "";

        public int ultimoId()
        {
            PROCEDIMIENTO = "select Id_tipoAlerta_NivelRiesgo from Sto_TipoAlerta_NivelRiego order by Id_tipoAlerta_NivelRiesgo desc";
            int rpta = 0;
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTO, con))
                {
                    comando.CommandType = CommandType.Text;

                    rpta = comando.ExecuteNonQuery();
                }
            }
            return rpta;
        }

        public int CrearTipoAlertaNivelRiesgo(Sto_TipoAleta_NivelRiesgo STANR)
        {
            PROCEDIMIENTO = ListaProcedimientos.CREAR + ListaProcedimientos.TipoAlerta_NivelRiesgo;
            int rpta = 0;
            using(SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using(SqlCommand comando = new SqlCommand(PROCEDIMIENTO, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    SqlParameter id = comando.Parameters.AddWithValue("@id", 0);
                    id.Direction = ParameterDirection.InputOutput;
                    comando.Parameters.AddWithValue("@tipoAlerta", STANR.TipoAlerta1);
                    comando.Parameters.AddWithValue("@minPuntaje", STANR.Min_Puntaje1);
                    comando.Parameters.AddWithValue("@maxPuntaje", STANR.Max_Puntaje1);
                    comando.Parameters.AddWithValue("@nivlRiesgoAlerta", STANR.NivelRiesgoAlerta1);

                    comando.ExecuteNonQuery();
                    rpta = Convert.ToInt32(comando.Parameters["@id"].Value);
                }
            }
            return rpta;
        }

        public int ActualizarTipoAlertaNivelRiesgo(Sto_TipoAleta_NivelRiesgo STANR)
        {
            PROCEDIMIENTO = ListaProcedimientos.ACTUALIZAR + ListaProcedimientos.TipoAlerta_NivelRiesgo;
            int rpta = 0;
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTO, con))
                {
                    try
                    {
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@tipoAlerta", STANR.TipoAlerta1);
                        comando.Parameters.AddWithValue("@minPuntaje", STANR.Min_Puntaje1);
                        comando.Parameters.AddWithValue("@maxPuntaje", STANR.Max_Puntaje1);
                        //comando.Parameters.AddWithValue("@referencia", STANR.Referencia1);
                        comando.Parameters.AddWithValue("@nivlRiesgoAlerta", STANR.NivelRiesgoAlerta1);
                        comando.Parameters.AddWithValue("@id", STANR.Id_tipoAlerta_NivelRiesgo1);

                        rpta = comando.ExecuteNonQuery();
                    }
                    catch(Exception ex)
                    {
                        var msg = ex.Message;
                    }
                    
                }
            }
            return rpta;
        }

        public int EliminarTipoAlertaNivelRiesgo(long id)
        {
            PROCEDIMIENTO = ListaProcedimientos.ELIMINAR + ListaProcedimientos.TipoAlerta_NivelRiesgo;
            int rpta = 0;
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTO, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@id", id);

                    rpta = comando.ExecuteNonQuery();
                }
            }
            return rpta;
        }

        public List<Sto_TipoAleta_NivelRiesgo> listaTipoAlerta_NivelRiesgo()
        {
            PROCEDIMIENTO = ListaProcedimientos.LISTAR + ListaProcedimientos.TipoAlerta_NivelRiesgo;
            Sto_TipoAleta_NivelRiesgo STANR = null;
            List<Sto_TipoAleta_NivelRiesgo> rpta = new List<Sto_TipoAleta_NivelRiesgo>();
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTO, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    using(SqlDataReader read = comando.ExecuteReader())
                    {
                        if(read != null)
                        {
                            while (read.Read())
                            {
                                STANR = new Sto_TipoAleta_NivelRiesgo();
                                STANR.Id_tipoAlerta_NivelRiesgo1 = Convert.ToInt64(read["Id_tipoAlerta_NivelRiesgo"]);
                                STANR.TipoAlerta1 = Convert.ToInt32(read["TipoAlerta"].ToString());
                                STANR.Min_Puntaje1 = Convert.ToDecimal(read["Min_Puntaje"].ToString());
                                STANR.Max_Puntaje1 = Convert.ToDecimal(read["Max_Puntaje"].ToString());
                                //STANR.Referencia1 = read["Referencia"].ToString();
                                STANR.NivelRiesgoAlerta1 = Convert.ToInt32(read["NivelRiesgoAlerta"].ToString());
                                rpta.Add(STANR);

                            }
                        }
                    }
                }
            }
            return rpta;
        }

        public Sto_TipoAleta_NivelRiesgo getOne(int id)
        {
            PROCEDIMIENTO = ListaProcedimientos.LISTAR + ListaProcedimientos.TipoAlerta_NivelRiesgo + ListaProcedimientos.ByID;
            Sto_TipoAleta_NivelRiesgo STANR = null;
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTO, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@id", id);
                    using (SqlDataReader read = comando.ExecuteReader())
                    {
                        if (read != null)
                        {
                            if (read.Read())
                            {
                                STANR = new Sto_TipoAleta_NivelRiesgo();
                                STANR.Id_tipoAlerta_NivelRiesgo1 = Convert.ToInt64(read["Id_tipoAlerta_NivelRiesgo"]);
                                STANR.TipoAlerta1 = Convert.ToInt32(read["TipoAlerta"].ToString());
                                STANR.Min_Puntaje1 = Convert.ToDecimal(read["Min_Puntaje"].ToString());
                                STANR.Max_Puntaje1 = Convert.ToDecimal(read["Max_Puntaje"].ToString());
                                //STANR.Referencia1 = read["Referencia"].ToString();
                                STANR.NivelRiesgoAlerta1 = Convert.ToInt32(read["NivelRiesgoAlerta"].ToString());

                            }
                        }
                    }
                }
            }
            return STANR;
        }
    }
}
