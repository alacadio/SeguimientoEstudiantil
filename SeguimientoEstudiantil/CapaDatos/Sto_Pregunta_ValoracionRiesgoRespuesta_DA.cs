﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CapaEntidad;

namespace CapaDatos
{
    public class Sto_Pregunta_ValoracionRiesgoRespuesta_DA
    {


        #region VARIABLES GLOBALES DE LA CLASE
        /// <summary>
        /// Variable de inicializacion de los nombre de los procedimiento.
        /// </summary>
        private string PROCEDIMIENTOS = "";
        /// <summary>
        /// Variable de las diferentes operaciones que realiza el procedimiento.
        /// </summary>
        private int LISTAR = ListaProcedimientos.OPCION_LISTAR;
        private int LISTAR_ByID = ListaProcedimientos.OPCION_LISTAR_BYID;
        private int CREAR = ListaProcedimientos.OPCION_CREAR;
        private int ACTUALIZAR = ListaProcedimientos.OPCION_ACTUALIZAR;
        private int ELIMINAR = ListaProcedimientos.OPCION_ELIMINAR;
        #endregion

        #region CREAR PREGUNTA VALORACION RIESGO RESPUESTA
        public int crearPregunta_ValoracionRiesgoRespuesta(Sto_Pregunta_ValoracionRiesgoRespuesta st)
        {
            int rpta = 0;
            PROCEDIMIENTOS = ListaProcedimientos.MAESTRO + ListaProcedimientos.Pregunta_ValoracionRiesgoRespuesta;
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    SqlParameter id = comando.Parameters.AddWithValue("@id", 0);
                    id.Direction = ParameterDirection.InputOutput;
                    comando.Parameters.AddWithValue("@id_pregunta_valoracion", st.Id_Pregunta_Valoracion1);
                    comando.Parameters.AddWithValue("@id_tipoAlerta_pregunta", st.Id_TipoAlerta_Pregunta1);
                    comando.Parameters.AddWithValue("@res", st.Respuesta1);
                    comando.Parameters.AddWithValue("@valor", st.ValorRiesgo1);
                    comando.Parameters.AddWithValue("@tipoAlerta", 0);
                    comando.Parameters.AddWithValue("@des", st.DescripcionRespuesta1);
                    comando.Parameters.AddWithValue("@op", CREAR);

                    comando.ExecuteNonQuery();
                    rpta = Convert.ToInt32(comando.Parameters["@id"].Value);
                }
            }


            return rpta;
        }
        #endregion

        #region ACTUALIZAR PREGUNTA VALORACION RIESGO RESPUESTA
        public int ActualizarPregunta_ValoracionRiesgoRespuesta(Sto_Pregunta_ValoracionRiesgoRespuesta st)
        {
            int rpta = 0;
            PROCEDIMIENTOS = ListaProcedimientos.MAESTRO + ListaProcedimientos.Pregunta_ValoracionRiesgoRespuesta;
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    SqlParameter id = comando.Parameters.AddWithValue("@id", 0);
                    id.Direction = ParameterDirection.InputOutput;
                    comando.Parameters.AddWithValue("@id_pregunta_valoracion", st.Id_Pregunta_Valoracion1);
                    comando.Parameters.AddWithValue("@id_tipoAlerta_pregunta", st.Id_TipoAlerta_Pregunta1);
                    comando.Parameters.AddWithValue("@res", st.Respuesta1);
                    comando.Parameters.AddWithValue("@valor", st.ValorRiesgo1);
                    comando.Parameters.AddWithValue("@tipoAlerta", 0);
                    comando.Parameters.AddWithValue("@des", st.DescripcionRespuesta1);
                    comando.Parameters.AddWithValue("@op", ACTUALIZAR);

                    rpta = comando.ExecuteNonQuery();
                }
            }


            return rpta;
        }
        #endregion

        #region ELIMINAR PREGUNTA VALORACION RIESGO RESPUESTA
        public int EliminarPregunta_ValoracionRiesgoRespuesta(Sto_Pregunta_ValoracionRiesgoRespuesta st)
        {
            int rpta = 0;
            PROCEDIMIENTOS = ListaProcedimientos.MAESTRO + ListaProcedimientos.Pregunta_ValoracionRiesgoRespuesta;
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    SqlParameter id = comando.Parameters.AddWithValue("@id", 0);
                    id.Direction = ParameterDirection.InputOutput;
                    comando.Parameters.AddWithValue("@id_pregunta_valoracion", st.Id_Pregunta_Valoracion1);
                    comando.Parameters.AddWithValue("@id_tipoAlerta_pregunta", st.Id_TipoAlerta_Pregunta1);
                    comando.Parameters.AddWithValue("@res", st.Respuesta1);
                    comando.Parameters.AddWithValue("@valor", st.ValorRiesgo1);
                    comando.Parameters.AddWithValue("@tipoAlerta", 0);
                    comando.Parameters.AddWithValue("@des", st.DescripcionRespuesta1);
                    comando.Parameters.AddWithValue("@op", ELIMINAR);

                    rpta = comando.ExecuteNonQuery();
                }
            }


            return rpta;
        }
        #endregion

        #region OBTENER PREGUNTA VALORACION RIESGO RESPUESTA POR ID_PREGUNTA
        public List<Sto_Pregunta_ValoracionRiesgoRespuesta> ListarPregunta_ValoracionRiesgoRespuesta_ID_PREGUNTA(Sto_Pregunta_ValoracionRiesgoRespuesta st)
        {
            List<Sto_Pregunta_ValoracionRiesgoRespuesta> lista = new List<Sto_Pregunta_ValoracionRiesgoRespuesta>();
            Sto_Pregunta_ValoracionRiesgoRespuesta SPVR = new Sto_Pregunta_ValoracionRiesgoRespuesta();
            PROCEDIMIENTOS = ListaProcedimientos.MAESTRO + ListaProcedimientos.Pregunta_ValoracionRiesgoRespuesta;
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    SqlParameter id = comando.Parameters.AddWithValue("@id", 0);
                    id.Direction = ParameterDirection.InputOutput;
                    comando.Parameters.AddWithValue("@id_pregunta_valoracion", st.Id_Pregunta_Valoracion1);
                    comando.Parameters.AddWithValue("@id_tipoAlerta_pregunta", st.Id_TipoAlerta_Pregunta1);
                    comando.Parameters.AddWithValue("@res", st.Respuesta1);
                    comando.Parameters.AddWithValue("@valor", st.ValorRiesgo1);
                    comando.Parameters.AddWithValue("@tipoAlerta", 0);
                    comando.Parameters.AddWithValue("@des", st.DescripcionRespuesta1);
                    comando.Parameters.AddWithValue("@op", LISTAR_ByID);

                    using(SqlDataReader read = comando.ExecuteReader())
                    {
                        if(read != null)
                        {
                            while (read.Read())
                            {
                                SPVR = new Sto_Pregunta_ValoracionRiesgoRespuesta();
                                SPVR.Id_Pregunta_Valoracion1 = Convert.ToInt32(read["id_pregunta_valoracion"].ToString());
                                SPVR.Id_TipoAlerta_Pregunta1 = Convert.ToUInt32(read["id_tipoAlerta_pregunta"].ToString());
                                SPVR.Respuesta1 = read["Respuesta"].ToString();
                                SPVR.ValorRiesgo1 = Convert.ToInt32(read["ValorRiesgo"].ToString());
                                SPVR.DescripcionRespuesta1 = read["DescripcionRespuesta"].ToString();
                                lista.Add(SPVR);
                            }
                        }
                    }
                }
            }
            return lista;
        }
        #endregion

        #region OBTENER PREGUNTA VALORACION RIESGO RESPUESTA POR ID_PREGUNTA_VALOR
        public List<Sto_Pregunta_ValoracionRiesgoRespuesta> ListarPregunta_ValoracionRiesgoRespuesta_ID_PREGUNTA_VALOR(long st,int tipo)
        {
            List<Sto_Pregunta_ValoracionRiesgoRespuesta> lista = new List<Sto_Pregunta_ValoracionRiesgoRespuesta>();
            Sto_Pregunta_ValoracionRiesgoRespuesta SPVR = new Sto_Pregunta_ValoracionRiesgoRespuesta();
            PROCEDIMIENTOS = ListaProcedimientos.MAESTRO + ListaProcedimientos.Pregunta_ValoracionRiesgoRespuesta;
            try
            {
                using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
                {
                    con.Open();
                    using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                    {
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@id", 0);
                        comando.Parameters.AddWithValue("@id_pregunta_valoracion", 0);
                        comando.Parameters.AddWithValue("@id_tipoAlerta_pregunta", st);
                        comando.Parameters.AddWithValue("@res", string.Empty);
                        comando.Parameters.AddWithValue("@tipoAlerta", tipo);
                        comando.Parameters.AddWithValue("@valor", 0);
                        comando.Parameters.AddWithValue("@des", 0);
                        comando.Parameters.AddWithValue("@op", ListaProcedimientos.OPCION_ESPECIAL_6);

                        using (SqlDataReader read = comando.ExecuteReader())
                        {
                            if (read != null)
                            {
                                while (read.Read())
                                {
                                    SPVR = new Sto_Pregunta_ValoracionRiesgoRespuesta();
                                    SPVR.Id_Pregunta_Valoracion1 = Convert.ToInt32(read["id_pregunta_valoracion"].ToString());
                                    SPVR.Id_TipoAlerta_Pregunta1 = Convert.ToUInt32(read["id_tipoAlerta_pregunta"].ToString());
                                    SPVR.Respuesta1 = read["Respuesta"].ToString();
                                    SPVR.ValorRiesgo1 = Convert.ToInt32(read["ValorRiesgo"].ToString());
                                    SPVR.DescripcionRespuesta1 = read["DescripcionRespuesta"].ToString();
                                    lista.Add(SPVR);
                                }
                            }
                        }
                    }
                }
            }catch(Exception e)
            {
                return lista;
            }
            return lista;
        }
        #endregion
    }
}
