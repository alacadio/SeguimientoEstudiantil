﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CapaEntidad;

namespace CapaDatos
{
    public class Sto_RegistroEmail_DA
    {

        #region VARIABLES GLOBALES DE LA CLASE
        /// <summary>
        /// Variable de inicializacion de los nombre de los procedimiento.
        /// </summary>
        private string PROCEDIMIENTOS = "";
        /// <summary>
        /// Variable de las diferentes operaciones que realiza el procedimiento.
        /// </summary>
        private int LISTAR = ListaProcedimientos.OPCION_LISTAR;
        private int LISTAR_ByID = ListaProcedimientos.OPCION_LISTAR_BYID;
        private int CREAR = ListaProcedimientos.OPCION_CREAR;
        private int ACTUALIZAR = ListaProcedimientos.OPCION_ACTUALIZAR;
        private int ELIMINAR = ListaProcedimientos.OPCION_ELIMINAR;
        /// <summary>
        /// Declaraion de la instancia a la clase
        /// </summary>
        private Sto_RegistroEmail STAU;
        #endregion

        #region REGISTRAR REGISTROEMAIL
        public int resgistrarRegistroEmail(Sto_RegistroEmail st)
        {
            int rpta = 0;
            PROCEDIMIENTOS = ListaProcedimientos.MAESTRO + ListaProcedimientos.RegistroEmail;
            try
            {
                using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
                {
                    con.Open();
                    using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                    {
                        comando.CommandType = CommandType.StoredProcedure;
                        SqlParameter id = comando.Parameters.AddWithValue("@id", 0);
                        id.Direction = ParameterDirection.InputOutput;
                        comando.Parameters.AddWithValue("@id_registroEmail", st.Id_RegistroEmail);
                        comando.Parameters.AddWithValue("@fec_Env", st.Fec_Envio1);
                        comando.Parameters.AddWithValue("@TipoAsesor", st.Sto_TipoAsesor);
                        comando.Parameters.AddWithValue("@Estudiante", st.Estudiante);
                        comando.Parameters.AddWithValue("@TipoAcciones", st.Sto_TipoAcciones);
                        comando.Parameters.AddWithValue("@op", CREAR);

                        comando.ExecuteNonQuery();
                        rpta = Convert.ToInt32(comando.Parameters["@id"].Value);
                    }
                }
                return rpta;
            }
            catch (Exception e)
            {
                return rpta;
            }

        }
        #endregion

        #region ACTUALIZAR REGISTROEMAIL
        public int actualizarRegistroEmail(Sto_RegistroEmail st)
        {
            int rpta = 0;
            PROCEDIMIENTOS = ListaProcedimientos.MAESTRO + ListaProcedimientos.RegistroEmail;
            try
            {
                using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
                {
                    con.Open();
                    using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                    {
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@id", 0);
                        comando.Parameters.AddWithValue("@id_registroEmail", st.Id_RegistroEmail);
                        comando.Parameters.AddWithValue("@fec_Env", st.Fec_Envio1);
                        comando.Parameters.AddWithValue("@TipoAsesor", st.Sto_TipoAsesor);
                        comando.Parameters.AddWithValue("@Estudiante", st.Estudiante);
                        comando.Parameters.AddWithValue("@TipoAcciones", st.Sto_TipoAcciones);
                        comando.Parameters.AddWithValue("@op", ACTUALIZAR);
                        rpta = comando.ExecuteNonQuery();
                    }
                }
                return rpta;
            }
            catch (Exception e)
            {
                return rpta;
            }
        }
        #endregion

        #region ELIMINAR REGISTROEMAIL
        public int eliminarRegistroEmail(Sto_RegistroEmail st)
        {
            int rpta = 0;
            PROCEDIMIENTOS = ListaProcedimientos.MAESTRO + ListaProcedimientos.RegistroEmail;
            try
            {
                using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
                {
                    con.Open();
                    using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                    {
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@id", 0);
                        comando.Parameters.AddWithValue("@id_registroEmail", st.Id_RegistroEmail);
                        comando.Parameters.AddWithValue("@fec_Env", st.Fec_Envio1);
                        comando.Parameters.AddWithValue("@TipoAsesor", st.Sto_TipoAsesor);
                        comando.Parameters.AddWithValue("@Estudiante", st.Estudiante);
                        comando.Parameters.AddWithValue("@TipoAcciones", st.Sto_TipoAcciones);
                        comando.Parameters.AddWithValue("@op", ELIMINAR);

                        rpta = comando.ExecuteNonQuery();
                    }
                }
                return rpta;
            }
            catch (Exception e)
            {
                return rpta;
            }
        }
        #endregion

        #region LISTAR REGISTROEMAIL
        public List<Sto_RegistroEmail> listaRegistroEmail()
        {
            List<Sto_RegistroEmail> lista = new List<Sto_RegistroEmail>();
            PROCEDIMIENTOS = ListaProcedimientos.MAESTRO + ListaProcedimientos.TiposAsesores;
            STAU = new Sto_RegistroEmail();
            try
            {
                using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
                {
                    con.Open();
                    using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                    {
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@id", 0);
                        comando.Parameters.AddWithValue("@id_registroEmail", 0);
                        comando.Parameters.AddWithValue("@fec_Env", DateTime.Now);
                        comando.Parameters.AddWithValue("@TipoAsesor", 0);
                        comando.Parameters.AddWithValue("@Estudiante", string.Empty);
                        comando.Parameters.AddWithValue("@TipoAcciones", 0);
                        comando.Parameters.AddWithValue("@op", LISTAR);
                        using (SqlDataReader read = comando.ExecuteReader())
                        {
                            if (read != null)
                            {
                                while (read.Read())
                                {
                                    STAU = new Sto_RegistroEmail();
                                    STAU.Id_RegistroEmail = Convert.ToInt32(read["id_RegistroEmail"].ToString());
                                    STAU.Fec_Envio1 = DateTime.Parse(read["Fec_Envio"].ToString());
                                    STAU.Sto_TipoAsesor = Convert.ToInt32(read["Sto_TipoAsesor"].ToString());
                                    STAU.Estudiante = read["Estudiante"].ToString();
                                    STAU.Sto_TipoAcciones = Convert.ToInt32(read["Sto_TipoAcciones"].ToString());
                                    lista.Add(STAU);
                                }
                            }
                        }
                    }
                }
                return lista;
            }
            catch (Exception e)
            {
                lista.Add(STAU);
                return lista;
            }
        }
        #endregion

        #region LISTAR REGISTROEMAIL POR ID
        public Sto_RegistroEmail listaRegistroEmail_ByID(long id)
        {
            PROCEDIMIENTOS = ListaProcedimientos.MAESTRO + ListaProcedimientos.TiposAsesores;
            STAU = new Sto_RegistroEmail();
            try
            {
                using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
                {
                    con.Open();
                    using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                    {
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@id", 0);
                        comando.Parameters.AddWithValue("@id_registroEmail", 0);
                        comando.Parameters.AddWithValue("@fec_Env", DateTime.Now);
                        comando.Parameters.AddWithValue("@TipoAsesor", 0);
                        comando.Parameters.AddWithValue("@Estudiante", string.Empty);
                        comando.Parameters.AddWithValue("@TipoAcciones", 0);
                        comando.Parameters.AddWithValue("@op", LISTAR_ByID);
                        using (SqlDataReader read = comando.ExecuteReader())
                        {
                            if (read != null)
                            {
                                if (read.Read())
                                {
                                    STAU = new Sto_RegistroEmail();
                                    STAU.Id_RegistroEmail = Convert.ToInt32(read["id_RegistroEmail"].ToString());
                                    STAU.Fec_Envio1 = DateTime.Parse(read["Fec_Envio"].ToString());
                                    STAU.Sto_TipoAsesor = Convert.ToInt32(read["Sto_TipoAsesor"].ToString());
                                    STAU.Estudiante = read["Estudiante"].ToString();
                                    STAU.Sto_TipoAcciones = Convert.ToInt32(read["Sto_TipoAcciones"].ToString());
                                }
                            }
                        }
                    }
                }
                return STAU;
            }
            catch (Exception e)
            {
                return STAU;
            }
        }
        #endregion
    }
}
