﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidad;
using System.Data;
using System.Data.SqlClient;

namespace CapaDatos
{
    public class Sto_TipoAcciones_DA
    {
        private string PROCEDIMIENTOS = "";

        public List<Sto_TipoAcciones> listaTipoAcciones()
        {
            Sto_TipoAcciones acciones = null;
            List<Sto_TipoAcciones> lista = new List<Sto_TipoAcciones>();
            PROCEDIMIENTOS = ListaProcedimientos.LISTAR + ListaProcedimientos.TipoAcciones;

            using(SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using(SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    using (SqlDataReader read = comando.ExecuteReader())
                    {
                        if(read != null)
                        {
                            while (read.Read())
                            {
                                acciones = new Sto_TipoAcciones();
                                acciones.ID_Sto_TipoAcciones1 = Convert.ToInt32(read["Sto_TipoAcciones"].ToString());
                                acciones.Accion1 = read["Nombre"].ToString();
                                lista.Add(acciones);
                            }
                        }
                    }
                }
            }
            return lista;
        }

        public int CrearTipoAcciones (Sto_TipoAcciones STA)
        {
            PROCEDIMIENTOS = ListaProcedimientos.CREAR + ListaProcedimientos.TipoAcciones;
            int rpta = 0;
            using(SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using(SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@accion",STA.Accion1);
                    comando.Parameters.AddWithValue("@id", 0);

                    rpta = comando.ExecuteNonQuery();
                }
            }
            return rpta;
        }

        public int EliminarTipoAcciones(int id)
        {
            PROCEDIMIENTOS = ListaProcedimientos.ELIMINAR + ListaProcedimientos.TipoAcciones;
            int rpta = 0;
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@id", id);

                    rpta = comando.ExecuteNonQuery();
                }
            }
            return rpta;
        }

        public int ActualizarTipoAcciones(Sto_TipoAcciones st)
        {
            PROCEDIMIENTOS = ListaProcedimientos.ACTUALIZAR + ListaProcedimientos.TipoAcciones;
            int rpta = 0;
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@accion", st.Accion1);
                    comando.Parameters.AddWithValue("@id", st.ID_Sto_TipoAcciones1);

                    rpta = comando.ExecuteNonQuery();
                }
            }
            return rpta;
        }

        public Sto_TipoAcciones selectOne(int id)
        {
            PROCEDIMIENTOS = ListaProcedimientos.LISTAR + ListaProcedimientos.TipoAcciones + ListaProcedimientos.ByID;
            Sto_TipoAcciones acciones = new Sto_TipoAcciones();
            using (SqlConnection con = new SqlConnection(Conexion.getConexion()))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(PROCEDIMIENTOS, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@id", id);
                    using (SqlDataReader read = comando.ExecuteReader())
                    {
                        if (read != null)
                        {
                            while (read.Read())
                            {
                                acciones = new Sto_TipoAcciones();
                                acciones.ID_Sto_TipoAcciones1 = Convert.ToInt32(read["Sto_TipoAcciones"].ToString());
                                acciones.Accion1 = read["Nombre"].ToString();
                            }
                        }
                    }
                }
            }
            return acciones;
        }

    }
}
