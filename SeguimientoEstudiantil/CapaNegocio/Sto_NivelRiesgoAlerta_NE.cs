﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidad;

namespace CapaNegocio
{
    public class Sto_NivelRiesgoAlerta_NE
    {
        private Sto_NivelRiesgoAlerta_DA SNRA = null;

        public Sto_NivelRiesgoAlerta_NE()
        {
            SNRA = new Sto_NivelRiesgoAlerta_DA();
        }

        public int CrearNivelRiesgoAlerta(Sto_NivelRiesgoAlerta st)
        {
            return SNRA.Crear(st);
        }

        public int ActualizarRiesgoNivelAlerta(Sto_NivelRiesgoAlerta st)
        {
            return SNRA.Actualizar(st);
        }

        public int EliminarRiesgoNivelAlerta(int st)
        {
            return SNRA.Eliminar(st);
        }

        public Sto_NivelRiesgoAlerta getOneNivelRiesgoAlerta(int st)
        {
            return SNRA.getOne(st);
        }

        public List<Sto_NivelRiesgoAlerta> ListarNivelRiesgoAlerta()
        {
            return SNRA.Listar();
        }
    }
}
