﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidad;

namespace CapaNegocio
{
    public class Sto_TipoAlerta_NivelRiesgo_NE
    {
        private Sto_TipoAlerta_NivelRiesgo_DA STANR = null;
        public Sto_TipoAlerta_NivelRiesgo_NE()
        {
            STANR = new Sto_TipoAlerta_NivelRiesgo_DA();
        }

        public int CrearTipoAlerta_NivelRiesgo(Sto_TipoAleta_NivelRiesgo st)
        {
            return STANR.CrearTipoAlertaNivelRiesgo(st);
        }

        public List<Sto_TipoAleta_NivelRiesgo> ListaTipoAlerta_NivelRiesgo()
        {
            return STANR.listaTipoAlerta_NivelRiesgo();
        }

        public int EliminarTipoAlerta_NivelRiesgo(long st)
        {
            return STANR.EliminarTipoAlertaNivelRiesgo(st);
        }

        public int ActualizarTipoAlerta_NivelRiesgo(Sto_TipoAleta_NivelRiesgo st)
        {
            return STANR.ActualizarTipoAlertaNivelRiesgo(st);
        }

        public Sto_TipoAleta_NivelRiesgo getOne(int st)
        {
            return STANR.getOne(st);
        }

        public int ultimoID()
        {
            return STANR.ultimoId();
        }

    }
}
