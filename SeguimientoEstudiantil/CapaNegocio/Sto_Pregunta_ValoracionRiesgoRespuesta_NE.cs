﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidad;
using CapaDatos;    
namespace CapaNegocio
{
    public class Sto_Pregunta_ValoracionRiesgoRespuesta_NE
    {
        private Sto_Pregunta_ValoracionRiesgoRespuesta_DA SPVRDA = null;

        public Sto_Pregunta_ValoracionRiesgoRespuesta_NE()
        {
            SPVRDA = new Sto_Pregunta_ValoracionRiesgoRespuesta_DA();
        }
        public int RegistrarRespuestaPorPregunta(Sto_Pregunta_ValoracionRiesgoRespuesta st)
        {
            return SPVRDA.crearPregunta_ValoracionRiesgoRespuesta(st);
        }
        public int ActualizarRespuestaPorPregunta(Sto_Pregunta_ValoracionRiesgoRespuesta st)
        {
            return SPVRDA.ActualizarPregunta_ValoracionRiesgoRespuesta(st);
        }
        public int EliminarRespuestaPorPregunta(Sto_Pregunta_ValoracionRiesgoRespuesta st)
        {
            return SPVRDA.EliminarPregunta_ValoracionRiesgoRespuesta(st);
        }
        public List<Sto_Pregunta_ValoracionRiesgoRespuesta> obtenerPorIdPregunta(Sto_Pregunta_ValoracionRiesgoRespuesta st)
        {
            return SPVRDA.ListarPregunta_ValoracionRiesgoRespuesta_ID_PREGUNTA(st);
        }
        public List<Sto_Pregunta_ValoracionRiesgoRespuesta> obtenerPorIdPreguntaValorRespuesta(long st,int tipo)
        {
            return SPVRDA.ListarPregunta_ValoracionRiesgoRespuesta_ID_PREGUNTA_VALOR(st, tipo);
        }

    }
}
