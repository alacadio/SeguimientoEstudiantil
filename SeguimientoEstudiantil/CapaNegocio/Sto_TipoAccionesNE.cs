﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidad;
namespace CapaNegocio
{
    public class Sto_TipoAccionesNE
    {
        private Sto_TipoAcciones_DA STA = null;
        public Sto_TipoAccionesNE()
        {
            STA = new Sto_TipoAcciones_DA();
        }

        public List<Sto_TipoAcciones> ListarTipoAcciones()
        {
            return STA.listaTipoAcciones();
        }
        public int CrearTipoAccion (Sto_TipoAcciones st)
        {
            return STA.CrearTipoAcciones(st);
        }
        public int EliminarTipoAccion (int id)
        {
            return STA.EliminarTipoAcciones(id);
        }
        public int ActualizarTipoAccion(Sto_TipoAcciones st)
        {
            return STA.ActualizarTipoAcciones(st);
        }
        public Sto_TipoAcciones getOne(int id)
        {
            return STA.selectOne(id);
        }

    }
}
