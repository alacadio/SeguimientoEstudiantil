﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidad;

namespace CapaNegocio
{
    public class Sto_TipoAlerta_Pregunta_NE
    {
        private Sto_TipoAlerta_Pregunta_DA STAPDA = null;

        public Sto_TipoAlerta_Pregunta_NE()
        {
            STAPDA = new Sto_TipoAlerta_Pregunta_DA();
        }

        public int Crear_TipoAlerta_Pregunta(Sto_TipoAlerta_Pregunta st)
        {
            return STAPDA.CrearTiposAlertas(st);
        }
        public int EditarPregunta(Sto_TipoAlerta_Pregunta st)
        {
            return STAPDA.ActualizarTipoAlertasPreguntas(st);
        }
        public int EliminarPregunta(Sto_TipoAlerta_Pregunta st)
        {
            return STAPDA.EliminarPregunta(st);
        }
        public List<Sto_TipoAlerta_Pregunta> Listar_preguntas()
        {
            return STAPDA.ListarPreguntasRegistradas();
        }
        public Sto_TipoAlerta_Pregunta Listar_Pregunta_COD_Preg(string cod)
        {
            return STAPDA.ListarPreguntasRegistradas(cod);
        }
        public List<Sto_TipoAlerta_Pregunta> Listar_Pregunta_COD_Tipo(int cod)
        {
            return STAPDA.ListarPreguntasRegistradas(cod);
        }
        public Sto_TipoAlerta_Pregunta Listar_Pregunta_ByID(int cod)
        {
            return STAPDA.getPregunta(cod);
        }
        public int getObtener(int tipo, string preg)
        {
            return STAPDA.getObtener(tipo, preg);
        }
    }
}
