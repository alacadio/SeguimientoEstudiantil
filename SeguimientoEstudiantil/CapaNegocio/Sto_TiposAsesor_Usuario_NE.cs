﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidad;

namespace CapaNegocio
{
    public class Sto_TiposAsesor_Usuario_NE
    {

        private Sto_TiposAsesor_Usuario_DA STAUDA = null;

        public Sto_TiposAsesor_Usuario_NE()
        {
            STAUDA = new Sto_TiposAsesor_Usuario_DA();
        }

        public int RegistrarTipoAsesorUsuario(Sto_TipoAsesor_Usuario st)
        {
            return STAUDA.resgistrarTipoAsesorUsuario(st);
        }

        public int AcualizarTipoAsesorUsuario(Sto_TipoAsesor_Usuario st)
        {
            return STAUDA.actualizarTipoAsesorUsuario(st);
        }

        public int EliminarTipoAsesorUsuario(Sto_TipoAsesor_Usuario st)
        {
            return STAUDA.eliminarTipoAsesorUsuario(st);
        }

        public Sto_TipoAsesor_Usuario TipoAsesorUsuario(int id)
        {
            return STAUDA.listaTipoAsesor_ByID(id);
        }

        public List<Sto_TipoAsesor_Usuario> ListarTipoAsesorUsuario()
        {
            return STAUDA.listaTipoAsesorUsuario();
        }
    }
}
