﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidad;

namespace CapaNegocio
{
    public class Sto_TiposAlertas_NE
    {
        private Sto_TiposAlertas_DA STA = null;

        public Sto_TiposAlertas_NE()
        {
            STA = new Sto_TiposAlertas_DA();
        }

        public int CrearTiposAlertas(Sto_TiposAlertas st)
        {
            return STA.CrearTiposAlertas(st);
        }

        public int ActualizarTiposAlertas(Sto_TiposAlertas st)
        {
            return STA.ActualizarTiposAlertas(st);
        }

        public int EliminarTiposAlertas(int id)
        {
            return STA.EliminarTiposAlertas(id);
        }

        public List<Sto_TiposAlertas> ListarTiposAlertas()
        {
            return STA.listaTiposAlertas();
        }

        public Sto_TiposAlertas getOne(int id)
        {
            return STA.getOne(id);
        }
    }
}
