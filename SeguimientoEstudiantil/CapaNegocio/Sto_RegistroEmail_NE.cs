﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidad;

namespace CapaNegocio
{
    public class Sto_RegistroEmail_NE
    {

        private Sto_RegistroEmail_DA SRDA = null;

        public Sto_RegistroEmail_NE()
        {
            SRDA = new Sto_RegistroEmail_DA();
        }

        public int RegistrarNuevoRegistroEmail(Sto_RegistroEmail st)
        {
            return SRDA.resgistrarRegistroEmail(st);
        }

        public int ActualizarRegistroEmail(Sto_RegistroEmail st)
        {
            return SRDA.actualizarRegistroEmail(st);
        }

        public int EliminarRegistroEmail(Sto_RegistroEmail st)
        {
            return SRDA.eliminarRegistroEmail(st);
        }

        public List<Sto_RegistroEmail> ListarRegistroEmail()
        {
            return SRDA.listaRegistroEmail();
        }

        public Sto_RegistroEmail RegistroEmail(int st)
        {
            return SRDA.listaRegistroEmail_ByID(st);
        }
    }
}
