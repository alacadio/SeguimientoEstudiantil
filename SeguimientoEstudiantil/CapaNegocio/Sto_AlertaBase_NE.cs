﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidad;

namespace CapaNegocio
{
    public class Sto_AlertaBase_NE
    {
        private Sto_AlertaBase_DA STABDA = null;

        public Sto_AlertaBase_NE()
        {
            STABDA = new Sto_AlertaBase_DA();
        }

        public List<Sto_AlertaBase> ListarAlertasBase()
        {
            return STABDA.listaAlertaBase();
        }

        public Sto_AlertaBase getOneAlertasBAse(int id)
        {
            return STABDA.getOneAlertaBase(id);
        }
    }
}
