﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidad;

namespace CapaNegocio
{
    public class Sto_Formato_Caracterizacion_Preguntas_NE
    {
        private Sto_Formato_Caracterizacion_Preguntas_DA_API STCPDA = null;

        public Sto_Formato_Caracterizacion_Preguntas_NE()
        {
            STCPDA = new Sto_Formato_Caracterizacion_Preguntas_DA_API();
        }


        public List<Sto_Formato_Caracterizacion_Preguntas> listar()
        {
            return STCPDA.listar();
        }

        public string getPregunta(string id)
        {
            return STCPDA.getNombrePregunta(id);
        }
    }
}
