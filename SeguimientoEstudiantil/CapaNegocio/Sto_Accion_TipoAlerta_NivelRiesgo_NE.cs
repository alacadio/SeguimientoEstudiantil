﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidad;

namespace CapaNegocio
{
    public class Sto_Accion_TipoAlerta_NivelRiesgo_NE
    {
        private Sto_Accion_TipoAlerta_NivelRiesgo_DA SATNRDA = null;

        public Sto_Accion_TipoAlerta_NivelRiesgo_NE()
        {
            SATNRDA = new Sto_Accion_TipoAlerta_NivelRiesgo_DA();
        }

        public List<Sto_Accion_TipoAlerta_NivelRiesgo> ListarAccion_TipoAlerta_NivelRiesgo()
        {
            return SATNRDA.listarAccionTipoAlerta();
        }

        public Sto_Accion_TipoAlerta_NivelRiesgo getOne(long id)
        {
            return SATNRDA.getAccionTipoAlerta(id);
        }

        public int CrearAccion_TipoAlerta_NivelRiesgo(Sto_Accion_TipoAlerta_NivelRiesgo st)
        {
            return SATNRDA.CrearAccionTipoAlerta(st);
        }

        public int Actualizar(Sto_Accion_TipoAlerta_NivelRiesgo st)
        {
            return SATNRDA.ActualizarAccionTipoAlerta(st);
        }

    }
}
