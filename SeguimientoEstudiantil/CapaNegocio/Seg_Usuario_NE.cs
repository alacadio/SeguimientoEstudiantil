﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidad;
using CapaDatos;

namespace CapaNegocio
{
    public class Seg_Usuario_NE
    {
        Seg_Usuario_DA SEUDA = new Seg_Usuario_DA();

        public Seg_Usuario_NE()
        {
            SEUDA = new Seg_Usuario_DA();
        }

        public int RegistrarUsuario(Seg_Usuario seg)
        {
            return SEUDA.registrarUsuario(seg);
        }

        public int ActualizarUsuario(Seg_Usuario seg)
        {
            return SEUDA.actualizarUsuario(seg);
        }

        public int EliminarUsuario(string seg)
        {
            return SEUDA.eliminarUsuario(seg);
        }

        public Seg_Usuario GetOneUsuario(string seg)
        {
            return SEUDA.obtenerUsuario(seg);
        }

        public List<Seg_Usuario> ListarUsuarios()
        {
            return SEUDA.listarUsuario();
        }
    }
}
