﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidad;

namespace CapaNegocio
{
    public class Sto_TraerCaracterizacionRespuestas_NE
    {
        private Sto_TraerCaracterizacionRespuestas_DA_API STCRDA = null;

        public Sto_TraerCaracterizacionRespuestas_NE()
        {
            STCRDA = new Sto_TraerCaracterizacionRespuestas_DA_API();
        }

        public List<Sto_TraerCaracterizacionRespuestas> Lista_TraerCaracterizacionRespuestas(string cod,string ambito)
        {
            return STCRDA.listar_TraerCaracterizacionRespuestas(cod, ambito);
        }
    }
}
