﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidad;

namespace CapaNegocio
{
    public class Sto_TiposAsesores_NE
    {

        public Sto_TiposAsesores_DA STADA = null;

        public Sto_TiposAsesores_NE()
        {
            STADA = new Sto_TiposAsesores_DA();
        }

        public int RegsitrarTiposAsesores(Sto_TiposAsesores st)
        {
            return STADA.resgistrarTipoAsesor(st);
        }

        public int ActualizarTiposAsesores(Sto_TiposAsesores st)
        {
            return STADA.actualizarTipoAsesor(st);
        }

        public int EliminarTiposAsesores(Sto_TiposAsesores st)
        {
            return STADA.eliminarTipoAsesor(st);
        }

        public Sto_TiposAsesores TiposAsesores(int id)
        {
            return STADA.listaTipoAsesor_ByID(id);
        }

        public List<Sto_TiposAsesores> ListarTiposAsesores()
        {
            return STADA.listaTipoAsesor();
        }
    }
}
