﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidad;

namespace CapaNegocio
{
    public class Sto_Formato_TraerRespuestasCaracterizacion_NE
    {
        public Sto_Formato_TraerRespuestasCaracterizacion_DA_API SFTRCDA = null;
        public Sto_Formato_TraerRespuestasCaracterizacion_NE()
        {
            SFTRCDA = new Sto_Formato_TraerRespuestasCaracterizacion_DA_API();
        }

        public List<Sto_Formato_TraerRespuestasCaracterizacion> lista()
        {
            return SFTRCDA.lista();
        }
    }
}
