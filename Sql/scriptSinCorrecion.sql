USE [Seguimiento]
GO
/****** Object:  Table [dbo].[Periodos]    Script Date: 13/02/2018 6:00:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Periodos](
	[Periodo] [varchar](10) NOT NULL,
 CONSTRAINT [PK_Periodos] PRIMARY KEY CLUSTERED 
(
	[Periodo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Res_Usuario_Nivel_Riesgo]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Res_Usuario_Nivel_Riesgo](
	[Estudiante] [varchar](25) NULL,
	[TipoAlerta] [int] NULL,
	[NivelRiesgoAlerta] [tinyint] NULL,
	[Periodo] [varchar](10) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Res_Usuario_Pregunta_Valor]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Res_Usuario_Pregunta_Valor](
	[Id_Usuario_Pregunta_Valor] [int] IDENTITY(1,1) NOT NULL,
	[Estudiante] [varchar](25) NOT NULL,
	[codigoInterno] [varchar](50) NOT NULL,
	[valorMarcado] [int] NOT NULL,
	[Respuesta] [varchar](200) NOT NULL,
	[Periodo] [varchar](10) NOT NULL,
 CONSTRAINT [PK_Res_Usuario_Pregunta_Valor] PRIMARY KEY CLUSTERED 
(
	[Id_Usuario_Pregunta_Valor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Seg_Usuarios]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Seg_Usuarios](
	[Usuario] [varchar](50) NOT NULL,
	[Nombre] [varchar](512) NOT NULL,
	[Activo] [bit] NOT NULL,
 CONSTRAINT [PK_seg_Usuarios] PRIMARY KEY CLUSTERED 
(
	[Usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sto_Accion_TipoAlerta_NivelRiesgo]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sto_Accion_TipoAlerta_NivelRiesgo](
	[id_Accion_TipoAlerta_NivelRiesgo] [int] IDENTITY(1,1) NOT NULL,
	[id_Accion] [int] NOT NULL,
	[id_TipoAlerta_NivelRiesgo] [bigint] NOT NULL,
	[Referencia] [varchar](2000) NULL,
 CONSTRAINT [PK_Sto_Accion_TipoAlerta_NivelRiesgo] PRIMARY KEY CLUSTERED 
(
	[id_Accion_TipoAlerta_NivelRiesgo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sto_AlertasBase]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sto_AlertasBase](
	[Alerta_base] [int] NOT NULL,
	[Nombre] [varchar](256) NOT NULL,
	[Descripcion] [varchar](2000) NOT NULL,
	[FuenteDato] [int] NOT NULL CONSTRAINT [DF_Sto_AlertasBase_DependeDeCaracterizacion]  DEFAULT ((1)),
	[UsuarioConfigura] [bit] NOT NULL CONSTRAINT [DF_Sto_AlertasBase_Unica]  DEFAULT ((1)),
 CONSTRAINT [PK_Sto_AlertasBase] PRIMARY KEY CLUSTERED 
(
	[Alerta_base] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sto_ConfiguracionesGenerales]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sto_ConfiguracionesGenerales](
	[IDConfig] [int] IDENTITY(1,1) NOT NULL,
	[EnvioCorreoAutoMatico] [bit] NOT NULL,
	[MostrarTodasGraficas] [bit] NOT NULL,
 CONSTRAINT [PK_Sto_ConfiguracionesGenerales] PRIMARY KEY CLUSTERED 
(
	[IDConfig] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sto_CuerpoEmail]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sto_CuerpoEmail](
	[id_CuerpoCorreo] [int] IDENTITY(1,1) NOT NULL,
	[cuerpo_correo] [text] NOT NULL,
	[estructura_correo] [text] NOT NULL,
 CONSTRAINT [PK_Sto_CuerpoEmail] PRIMARY KEY CLUSTERED 
(
	[id_CuerpoCorreo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sto_FuentesDatos]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sto_FuentesDatos](
	[FuenteDato] [int] NOT NULL,
	[nombre] [varchar](256) NULL,
	[Descripcion] [varchar](2000) NULL,
 CONSTRAINT [PK_Sto_FuentesDatos] PRIMARY KEY CLUSTERED 
(
	[FuenteDato] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sto_NivelesRiesgoAlerta]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sto_NivelesRiesgoAlerta](
	[NivelRiesgoAlerta] [tinyint] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](125) NOT NULL,
 CONSTRAINT [PK_Sto_NivelesRiegoAlerta] PRIMARY KEY CLUSTERED 
(
	[NivelRiesgoAlerta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sto_Pregunta_ValoracionRiesgoRespuesta]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sto_Pregunta_ValoracionRiesgoRespuesta](
	[id_pregunta_valoracion] [bigint] IDENTITY(1,1) NOT NULL,
	[id_tipoAlerta_pregunta] [bigint] NOT NULL,
	[Respuesta] [varchar](200) NOT NULL,
	[ValorRiesgo] [int] NOT NULL,
	[DescripcionRespuesta] [varchar](max) NULL,
 CONSTRAINT [PK_Sto_Pregunta_ValoracionRiesgoRespuesta] PRIMARY KEY CLUSTERED 
(
	[id_pregunta_valoracion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sto_RegistroEmail]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sto_RegistroEmail](
	[id_RegistroEmail] [int] IDENTITY(1,1) NOT NULL,
	[Fec_Envio] [date] NOT NULL,
	[Sto_TipoAsesor] [int] NOT NULL,
	[Estudiante] [varchar](25) NOT NULL,
	[Sto_TipoAcciones] [int] NOT NULL,
 CONSTRAINT [PK_Sto_RegistroEmail] PRIMARY KEY CLUSTERED 
(
	[id_RegistroEmail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sto_TipoAcciones]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sto_TipoAcciones](
	[Sto_TipoAcciones] [int] IDENTITY(5,1) NOT NULL,
	[Nombre] [varchar](256) NOT NULL,
 CONSTRAINT [PK_Sto_TipoAcciones] PRIMARY KEY CLUSTERED 
(
	[Sto_TipoAcciones] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sto_TipoAlerta_NivelRiego]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sto_TipoAlerta_NivelRiego](
	[Id_tipoAlerta_NivelRiesgo] [bigint] IDENTITY(1,1) NOT NULL,
	[TipoAlerta] [int] NOT NULL,
	[Min_Puntaje] [numeric](10, 1) NOT NULL,
	[Max_puntaje] [numeric](10, 1) NOT NULL,
	[NivelRiesgoAlerta] [tinyint] NOT NULL,
 CONSTRAINT [PK_Sto_TipoAlerta_NivelRiego] PRIMARY KEY CLUSTERED 
(
	[Id_tipoAlerta_NivelRiesgo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sto_TipoAlerta_Preguntas]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sto_TipoAlerta_Preguntas](
	[id_tipoAlerta_pregunta] [bigint] IDENTITY(1,1) NOT NULL,
	[TipoAlerta] [int] NOT NULL,
	[CodigoPregunta] [varchar](50) NOT NULL,
	[DescripcionPregunta] [varchar](max) NULL,
 CONSTRAINT [PK_Sto_TipoAlerta_Configuraciones] PRIMARY KEY CLUSTERED 
(
	[id_tipoAlerta_pregunta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sto_TipoAsesor_Usuario]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sto_TipoAsesor_Usuario](
	[Id_tipo_asesor_usuario] [bigint] IDENTITY(1,1) NOT NULL,
	[Usuario] [varchar](50) NOT NULL,
	[TipoAsesor] [int] NOT NULL,
 CONSTRAINT [PK_Sto_TipoAsesor_Usuario] PRIMARY KEY CLUSTERED 
(
	[Id_tipo_asesor_usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sto_TiposAlertas]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sto_TiposAlertas](
	[TipoAlerta] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](516) NOT NULL,
	[Alerta_base] [int] NULL,
 CONSTRAINT [PK_Sto_TiposAlertas] PRIMARY KEY CLUSTERED 
(
	[TipoAlerta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sto_TiposAsesores]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sto_TiposAsesores](
	[Sto_TipoAsesor] [int] IDENTITY(2,1) NOT NULL,
	[Nombre] [varchar](50) NULL,
	[email] [varchar](256) NULL,
 CONSTRAINT [PK_Sto_TiposAsesores] PRIMARY KEY CLUSTERED 
(
	[Sto_TipoAsesor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Res_Usuario_Nivel_Riesgo]  WITH CHECK ADD  CONSTRAINT [FK_Res_Usuario_Nivel_Riesgo_Periodos] FOREIGN KEY([Periodo])
REFERENCES [dbo].[Periodos] ([Periodo])
GO
ALTER TABLE [dbo].[Res_Usuario_Nivel_Riesgo] CHECK CONSTRAINT [FK_Res_Usuario_Nivel_Riesgo_Periodos]
GO
ALTER TABLE [dbo].[Res_Usuario_Nivel_Riesgo]  WITH CHECK ADD  CONSTRAINT [FK_Res_Usuario_Nivel_Riesgo_Sto_NivelesRiesgoAlerta] FOREIGN KEY([NivelRiesgoAlerta])
REFERENCES [dbo].[Sto_NivelesRiesgoAlerta] ([NivelRiesgoAlerta])
GO
ALTER TABLE [dbo].[Res_Usuario_Nivel_Riesgo] CHECK CONSTRAINT [FK_Res_Usuario_Nivel_Riesgo_Sto_NivelesRiesgoAlerta]
GO
ALTER TABLE [dbo].[Res_Usuario_Nivel_Riesgo]  WITH CHECK ADD  CONSTRAINT [FK_Res_Usuario_Nivel_Riesgo_Sto_TiposAlertas] FOREIGN KEY([TipoAlerta])
REFERENCES [dbo].[Sto_TiposAlertas] ([TipoAlerta])
GO
ALTER TABLE [dbo].[Res_Usuario_Nivel_Riesgo] CHECK CONSTRAINT [FK_Res_Usuario_Nivel_Riesgo_Sto_TiposAlertas]
GO
ALTER TABLE [dbo].[Res_Usuario_Pregunta_Valor]  WITH CHECK ADD  CONSTRAINT [FK_Res_Usuario_Pregunta_Valor_Periodos] FOREIGN KEY([Periodo])
REFERENCES [dbo].[Periodos] ([Periodo])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Res_Usuario_Pregunta_Valor] CHECK CONSTRAINT [FK_Res_Usuario_Pregunta_Valor_Periodos]
GO
ALTER TABLE [dbo].[Sto_Accion_TipoAlerta_NivelRiesgo]  WITH CHECK ADD  CONSTRAINT [FK_Sto_Accion_TipoAlerta_NivelRiesgo_Sto_TipoAcciones] FOREIGN KEY([id_Accion])
REFERENCES [dbo].[Sto_TipoAcciones] ([Sto_TipoAcciones])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Sto_Accion_TipoAlerta_NivelRiesgo] CHECK CONSTRAINT [FK_Sto_Accion_TipoAlerta_NivelRiesgo_Sto_TipoAcciones]
GO
ALTER TABLE [dbo].[Sto_Accion_TipoAlerta_NivelRiesgo]  WITH CHECK ADD  CONSTRAINT [FK_Sto_Accion_TipoAlerta_NivelRiesgo_Sto_TipoAlerta_NivelRiego] FOREIGN KEY([id_TipoAlerta_NivelRiesgo])
REFERENCES [dbo].[Sto_TipoAlerta_NivelRiego] ([Id_tipoAlerta_NivelRiesgo])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Sto_Accion_TipoAlerta_NivelRiesgo] CHECK CONSTRAINT [FK_Sto_Accion_TipoAlerta_NivelRiesgo_Sto_TipoAlerta_NivelRiego]
GO
ALTER TABLE [dbo].[Sto_AlertasBase]  WITH CHECK ADD  CONSTRAINT [FK_Sto_AlertasBase_Sto_FuentesDatos] FOREIGN KEY([FuenteDato])
REFERENCES [dbo].[Sto_FuentesDatos] ([FuenteDato])
GO
ALTER TABLE [dbo].[Sto_AlertasBase] CHECK CONSTRAINT [FK_Sto_AlertasBase_Sto_FuentesDatos]
GO
ALTER TABLE [dbo].[Sto_Pregunta_ValoracionRiesgoRespuesta]  WITH CHECK ADD  CONSTRAINT [FK_Sto_Pregunta_ValoracionRiesgoRespuesta_Sto_TipoAlerta_Preguntas] FOREIGN KEY([id_tipoAlerta_pregunta])
REFERENCES [dbo].[Sto_TipoAlerta_Preguntas] ([id_tipoAlerta_pregunta])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Sto_Pregunta_ValoracionRiesgoRespuesta] CHECK CONSTRAINT [FK_Sto_Pregunta_ValoracionRiesgoRespuesta_Sto_TipoAlerta_Preguntas]
GO
ALTER TABLE [dbo].[Sto_RegistroEmail]  WITH CHECK ADD  CONSTRAINT [FK_Sto_RegistroEmail_Sto_TipoAcciones] FOREIGN KEY([Sto_TipoAcciones])
REFERENCES [dbo].[Sto_TipoAcciones] ([Sto_TipoAcciones])
GO
ALTER TABLE [dbo].[Sto_RegistroEmail] CHECK CONSTRAINT [FK_Sto_RegistroEmail_Sto_TipoAcciones]
GO
ALTER TABLE [dbo].[Sto_RegistroEmail]  WITH CHECK ADD  CONSTRAINT [FK_Sto_RegistroEmail_Sto_TiposAsesores] FOREIGN KEY([Sto_TipoAsesor])
REFERENCES [dbo].[Sto_TiposAsesores] ([Sto_TipoAsesor])
GO
ALTER TABLE [dbo].[Sto_RegistroEmail] CHECK CONSTRAINT [FK_Sto_RegistroEmail_Sto_TiposAsesores]
GO
ALTER TABLE [dbo].[Sto_TipoAlerta_NivelRiego]  WITH CHECK ADD  CONSTRAINT [FK_Sto_TipoAlerta_NivelRiego_Sto_NivelesRiesgoAlerta] FOREIGN KEY([NivelRiesgoAlerta])
REFERENCES [dbo].[Sto_NivelesRiesgoAlerta] ([NivelRiesgoAlerta])
GO
ALTER TABLE [dbo].[Sto_TipoAlerta_NivelRiego] CHECK CONSTRAINT [FK_Sto_TipoAlerta_NivelRiego_Sto_NivelesRiesgoAlerta]
GO
ALTER TABLE [dbo].[Sto_TipoAlerta_NivelRiego]  WITH CHECK ADD  CONSTRAINT [FK_Sto_TipoAlerta_NivelRiego_Sto_TiposAlertas] FOREIGN KEY([TipoAlerta])
REFERENCES [dbo].[Sto_TiposAlertas] ([TipoAlerta])
GO
ALTER TABLE [dbo].[Sto_TipoAlerta_NivelRiego] CHECK CONSTRAINT [FK_Sto_TipoAlerta_NivelRiego_Sto_TiposAlertas]
GO
ALTER TABLE [dbo].[Sto_TipoAlerta_Preguntas]  WITH CHECK ADD  CONSTRAINT [FK_Sto_TipoAlerta_Configuraciones_Sto_TiposAlertas] FOREIGN KEY([TipoAlerta])
REFERENCES [dbo].[Sto_TiposAlertas] ([TipoAlerta])
GO
ALTER TABLE [dbo].[Sto_TipoAlerta_Preguntas] CHECK CONSTRAINT [FK_Sto_TipoAlerta_Configuraciones_Sto_TiposAlertas]
GO
ALTER TABLE [dbo].[Sto_TipoAsesor_Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Sto_TipoAsesor_Usuario_Seg_Usuarios] FOREIGN KEY([Usuario])
REFERENCES [dbo].[Seg_Usuarios] ([Usuario])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Sto_TipoAsesor_Usuario] CHECK CONSTRAINT [FK_Sto_TipoAsesor_Usuario_Seg_Usuarios]
GO
ALTER TABLE [dbo].[Sto_TipoAsesor_Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Sto_TipoAsesor_Usuario_Sto_TiposAsesores] FOREIGN KEY([TipoAsesor])
REFERENCES [dbo].[Sto_TiposAsesores] ([Sto_TipoAsesor])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Sto_TipoAsesor_Usuario] CHECK CONSTRAINT [FK_Sto_TipoAsesor_Usuario_Sto_TiposAsesores]
GO
ALTER TABLE [dbo].[Sto_TiposAlertas]  WITH CHECK ADD  CONSTRAINT [FK_Sto_TiposAlertas_Sto_AlertasBase] FOREIGN KEY([Alerta_base])
REFERENCES [dbo].[Sto_AlertasBase] ([Alerta_base])
GO
ALTER TABLE [dbo].[Sto_TiposAlertas] CHECK CONSTRAINT [FK_Sto_TiposAlertas_Sto_AlertasBase]
GO
/****** Object:  StoredProcedure [dbo].[Formato_Caracterizacion_Preguntas]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Formato_Caracterizacion_Preguntas] 
				@Ambito VARCHAR(50) = null, @op int = null
				as
BEGIN




CREATE TABLE #zzpreguntascarac(
	[CodigoInterno] [varchar](50) NOT NULL,
	[DetallePregunta] [varchar](max) NOT NULL,
	[TipoPreguntaRespuesta] [char](4) NOT NULL,
	[MultiValuado] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'EDAD', N'Edad', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'EST_CIVIL', N'Estado Civil', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'RES_ZNO', N'Zona Residencia', N'OMUR',  0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'N_HER', N'Número de hermanos', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'LOP', N'Lugar que ocupa', N'OMUR',  0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'THO', N'¿Tiene hijos? ', N'OMUR',  0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'OP_NHO', N'(Opcional) Número de hijos', N'OMUR', 0)
--------
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'EST_SOC', N'Estrato socioeconómico', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'WORK', N'Trabaja actualmente', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'OP_TIPC', N'Tipo de contrato', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'OP_SWORK', N'(opcional) Sector donde trabaja', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'NUM_PGF', N'Número de personas en su grupo familiar', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'NUM_PPF', N'Números de personas que aportan económicamente en su grupo familiar', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'ING_MNF', N'Ingresos mensuales del núcleo familiar', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'ENG_MNF', N'Egresos mensuales del núcleo familiar', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'TP_HOME', N'Tipo de vivienda', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'PS_SISB', N'Posee Sisben', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'PG_EST', N'¿Quién paga sus estudios?', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'CONT_AUX', N'¿Cuenta con auxilio o beca para pagar sus estudios?', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'CONT_CRED', N'¿Cuenta con algún crédito para pagar sus estudios? ', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'NUM_PAC', N'N° de personas a cargo', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'OP_SLP', N'Situación laboral del padre', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'OP_SELP', N'Sector laboral del padre', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'OP_SLM', N'Situación laboral de la madre', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'OP_SELM', N'Sector laboral de la madre', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'NVL_ESP', N'Nivel de escolaridad del padre', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'NVL_ESM', N'Nivel de escolaridad de la madre', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'FTE_RET', N'Factores de tipo económico pueden motivar su retiro de los estudios', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'RAF_PMAT', N'Requiere apoyo financiero para el pago de su matrícula', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'RAF_MANU', N'Requiere apoyo financiero para manutención ', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'TPA_RMANU', N'Tipo de ayuda que requiere para manutención', N'OMUR', 0)
--------
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'TIP_INST', N'Tipo de institución', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'SECTOR', N'Sector', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'JORNADA', N'Jornada', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'MOD_ESTU', N'Modalidad estudio bachillerato', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'FEC_GRAD', N'Año de graduación', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'OP_VALBA', N'Validó su bachillerato', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'OP_REND', N'Su rendimiento académico fue', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'ACT_REND', N'Actualmente posee un rendimiento académico ', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'OPR_CFARA', N'¿Cuáles cree que son los factores asociados al bajo rendimiento académico? ', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'OP_SSAC', N'¿Se siente satisfecho con la carrera que estudia?', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'OPC_AFSE', N'¿Cuenta con el apoyo familiar para sus estudios? ', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'OP_DBAP', N'¿Los docentes le han brindado acompañamiento oportuno? ', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'OPS_BAA', N'¿Siente que la institución le brindado acompañamiento apropiado? ', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'OPS_OMA', N'¿Ha asistido en alguna oportunidad a monitorías académicas? ', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'OPS_TAE', N'¿Ha recibido acompañamiento del programa Trayectoria académica Exitosa TAE? ', N'OMUR', 0)
--------
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'POB_PTR', N'Población a la que pertenece', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'OP_BAP', N'o	Posee algún tipo de beca o auxilio por su condición de población inclusiva ', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'PAT_DISC', N'Posee algún tipo de discapacidad', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'OP_ESPC', N'Especifique su discapacidad', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'OPC_HUAD', N'Cuáles de las siguientes herramientas utiliza para la realización de las actividades diarias', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'OPT_CASD', N'Tiene certificado que acredite su situación de discapacidad', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'OPP_BADIS', N'Posee algún tipo de beca o auxilio por su condición de discapacidad ', N'OMUR', 0)
--------
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'MODALIDAD', N'modalidad', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'DPTO_EXP', N'departamento de expedición de documento de identificación', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'MUN_EXP', N'municipio de expedición de documento de identificación', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'TIPO_DI', N'tipo de identificacion', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'GENERO', N'genero', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'DPTO_RES', N'departamento de residencia', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'MUN_RES', N'municipio de residencia', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'DPTO_NAC', N'departamento', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'MUN_NAC', N'municipio', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'32', N'¿Cuánto es el numero de personas en su grupo familiar?', N'OMUR', 0)

if @op = 1
	begin
		select [DetallePregunta] from #zzpreguntascarac where [CodigoInterno] = @Ambito
	end
	else
	begin
		select *  from #zzpreguntascarac
	end

END






GO
/****** Object:  StoredProcedure [dbo].[Formato_Caracterizacion_Preguntas2]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Formato_Caracterizacion_Preguntas2] 
				@Ambito VARCHAR(50), @DocIdentidad varchar(20) ='123456789'
				as
BEGIN




CREATE TABLE #zzpreguntascarac(
	[CodigoInterno] [varchar](50) NOT NULL,
	[DetallePregunta] [varchar](max) NOT NULL,
	[TipoPreguntaRespuesta] [char](4) NOT NULL,
	[MultiValuado] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'EDAD', N'Edad', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'EST_CIVIL', N'Estado Civil', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'RES_ZNO', N'Zona Residencia', N'OMUR',  0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'N_HER', N'Número de hermanos', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'LOP', N'Lugar que ocupa', N'OMUR',  0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'THO', N'¿Tiene hijos? ', N'OMUR',  0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'OP_NHO', N'(Opcional) Número de hijos', N'OMUR', 0)
--------
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'EST_SOC', N'Estrato socioeconómico', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'WORK', N'Trabaja actualmente', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'OP_TIPC', N'Tipo de contrato', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'OP_SWORK', N'(opcional) Sector donde trabaja', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'NUM_PGF', N'Número de personas en su grupo familiar', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'NUM_PPF', N'Números de personas que aportan económicamente en su grupo familiar', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'ING_MNF', N'Ingresos mensuales del núcleo familiar', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'ENG_MNF', N'Egresos mensuales del núcleo familiar', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'TP_HOME', N'Tipo de vivienda', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'PS_SISB', N'Posee Sisben', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'PG_EST', N'¿Quién paga sus estudios?', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'CONT_AUX', N'¿Cuenta con auxilio o beca para pagar sus estudios?', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'CONT_CRED', N'¿Cuenta con algún crédito para pagar sus estudios? ', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'NUM_PAC', N'N° de personas a cargo', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'OP_SLP', N'Situación laboral del padre', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'OP_SELP', N'Sector laboral del padre', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'OP_SLM', N'Situación laboral de la madre', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'OP_SELM', N'Sector laboral de la madre', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'NVL_ESP', N'Nivel de escolaridad del padre', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'NVL_ESM', N'Nivel de escolaridad de la madre', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'FTE_RET', N'Factores de tipo económico pueden motivar su retiro de los estudios', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'RAF_PMAT', N'Requiere apoyo financiero para el pago de su matrícula', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'RAF_MANU', N'Requiere apoyo financiero para manutención ', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta],[MultiValuado]) VALUES (N'TPA_RMANU', N'Tipo de ayuda que requiere para manutención', N'OMUR', 0)
--------
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'TIP_INST', N'Tipo de institución', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'SECTOR', N'Sector', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'JORNADA', N'Jornada', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'MOD_ESTU', N'Modalidad estudio bachillerato', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'FEC_GRAD', N'Año de graduación', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'OP_VALBA', N'Validó su bachillerato', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'OP_REND', N'Su rendimiento académico fue', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'ACT_REND', N'Actualmente posee un rendimiento académico ', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'OPR_CFARA', N'¿Cuáles cree que son los factores asociados al bajo rendimiento académico? ', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'OP_SSAC', N'¿Se siente satisfecho con la carrera que estudia?', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'OPC_AFSE', N'¿Cuenta con el apoyo familiar para sus estudios? ', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'OP_DBAP', N'¿Los docentes le han brindado acompañamiento oportuno? ', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'OPS_BAA', N'¿Siente que la institución le brindado acompañamiento apropiado? ', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'OPS_OMA', N'¿Ha asistido en alguna oportunidad a monitorías académicas? ', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'OPS_TAE', N'¿Ha recibido acompañamiento del programa Trayectoria académica Exitosa TAE? ', N'OMUR', 0)
--------
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'POB_PTR', N'Población a la que pertenece', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'OP_BAP', N'o	Posee algún tipo de beca o auxilio por su condición de población inclusiva ', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'PAT_DISC', N'Posee algún tipo de discapacidad', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'OP_ESPC', N'Especifique su discapacidad', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'OPC_HUAD', N'Cuáles de las siguientes herramientas utiliza para la realización de las actividades diarias', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'OPT_CASD', N'Tiene certificado que acredite su situación de discapacidad', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'OPP_BADIS', N'Posee algún tipo de beca o auxilio por su condición de discapacidad ', N'OMUR', 0)
--------
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'MODALIDAD', N'modalidad', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'DPTO_EXP', N'departamento de expedición de documento de identificación', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'MUN_EXP', N'municipio de expedición de documento de identificación', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'TIPO_DI', N'tipo de identificacion', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'GENERO', N'genero', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'DPTO_RES', N'departamento de residencia', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'MUN_RES', N'municipio de residencia', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'DPTO_NAC', N'departamento', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'MUN_NAC', N'municipio', N'OMUR', 0)
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [MultiValuado]) VALUES (N'32', N'¿Cuánto es el numero de personas en su grupo familiar?', N'OMUR', 0)

select *  from #zzpreguntascarac

END






GO
/****** Object:  StoredProcedure [dbo].[Formato_TraerCaracterizacionRespuestas]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Formato_TraerCaracterizacionRespuestas] 
				@Ambito VARCHAR(50), @CodigoInterno varchar(20)
				as
BEGIN

	CREATE TABLE #zzrespuestascarac(
	[CodigoInterno] [varchar](50) NULL,
	[respuesta] [varchar](255) NOT NULL,
	[Detalle] [varchar](255) NULL
) ON [PRIMARY]

---------
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'EDAD', N'1', N'Entre 15 y 20 años____ Entre 20 y 25 años____ ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'EDAD', N'2', N'Entre 25 y 30 años_____  ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'EDAD', N'3', N'Entre 30 y 35 años_____ ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'EDAD', N'4', N'35 años o más_____ ')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'EST_CIVIL', N'1', N'soltero(a) ___       ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'EST_CIVIL', N'2', N'casado(a) ___        ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'EST_CIVIL', N'3', N'Viudo(a) ___       ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'EST_CIVIL', N'4', N'Separado (a) ___       ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'EST_CIVIL', N'5', N'Unión Libre___       ')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'RES_ZNO', N'1', N'Zona rural___       ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'RES_ZNO', N'2', N'Zona urbana___       ')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'N_HER', N'1', N'1      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'N_HER', N'2', N'2      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'N_HER', N'3', N'3      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'N_HER', N'4', N'4      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'N_HER', N'5', N'5 o más___      ')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'LOP', N'1', N'1      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'LOP', N'2', N'2      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'LOP', N'3', N'3      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'LOP', N'4', N'4      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'LOP', N'5', N'5 o más___     ')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'THO', N'1', N'SI      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'THO', N'2', N'NO      ')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_NHO', N'1', N'1      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_NHO', N'2', N'2      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_NHO', N'3', N'3      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_NHO', N'4', N'Más de tres      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_NHO', N'5', N'Se encuentra esperando un hijo      ')

---------
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'EST_SOC', N'1', N'1      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'EST_SOC', N'2', N'2      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'EST_SOC', N'3', N'3      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'EST_SOC', N'4', N'4      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'EST_SOC', N'5', N'5      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'EST_SOC', N'5', N'6      ')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'WORK', N'1', N'SI      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'WORK', N'2', N'NO      ')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_TIPC', N'1', N'Indefinido      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_TIPC', N'2', N'Fijo      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_TIPC', N'3', N'Ocasional      ')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_SWORK', N'1', N'Educación      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_SWORK', N'2', N'Comercio      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_SWORK', N'3', N'Agricultura o ganadería      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_SWORK', N'4', N'Ventas      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_SWORK', N'5', N'Transporte      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_SWORK', N'6', N'Otro      ')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'NUM_PGF', N'1', N'1      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'NUM_PGF', N'2', N'2      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'NUM_PGF', N'3', N'3      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'NUM_PGF', N'4', N'4      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'NUM_PGF', N'5', N'5 o más___     ')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'NUM_PPF', N'1', N'1      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'NUM_PPF', N'2', N'2      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'NUM_PPF', N'3', N'3      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'NUM_PPF', N'4', N'4      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'NUM_PPF', N'5', N'5 o más___     ')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'ING_MNF', N'1', N'Entre $100.000 y 699.999      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'ING_MNF', N'2', N'Entre $700.000 y $999.999      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'ING_MNF', N'3', N'Entre $1.000.000 y $1.599.999      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'ING_MNF', N'4', N'Entre $1.600.000 y $1.999.999      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'ING_MNF', N'5', N'Entre $2.000.000 y $3.000.000    ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'ING_MNF', N'6', N'$3.000.001 o más     ')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'ENG_MNF', N'1', N'Entre $100.000 y 699.999      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'ENG_MNF', N'2', N'Entre $700.000 y $999.999      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'ENG_MNF', N'3', N'Entre $1.000.000 y $1.599.999      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'ENG_MNF', N'4', N'Entre $1.600.000 y $1.999.999      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'ENG_MNF', N'5', N'Entre $2.000.000 y $3.000.000    ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'ENG_MNF', N'6', N'$3.000.001 o más     ')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'TP_HOME', N'1', N'Propia      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'TP_HOME', N'2', N'Arrendada    ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'TP_HOME', N'3', N'Familiar     ')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'PS_SISB', N'1', N'SI    ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'PS_SISB', N'2', N'NO     ')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'PG_EST', N'1', N'Padre y Madre       ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'PG_EST', N'2', N'Padre      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'PG_EST', N'3', N'Madre      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'PG_EST', N'4', N'Tip(a)      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'PG_EST', N'5', N'Abuelos    ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'PG_EST', N'6', N'Usted     ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'PG_EST', N'7', N'Otros     ')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'CONT_AUX', N'1', N'SI    ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'CONT_AUX', N'2', N'NO     ')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'CONT_CRED', N'1', N'SI    ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'CONT_CRED', N'2', N'NO     ')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'NUM_PAC', N'1', N'1      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'NUM_PAC', N'2', N'2      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'NUM_PAC', N'3', N'3      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'NUM_PAC', N'4', N'4 o más      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'NUM_PAC', N'5', N'No tiene personas a cargo    ')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_SLP', N'1', N'Empleado      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_SLP', N'2', N'Independiente      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_SLP', N'3', N'Hogar      ')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_SELP', N'1', N'Educación      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_SELP', N'2', N'Comercio      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_SELP', N'3', N'Agricultura o ganadería      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_SELP', N'4', N'Ventas      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_SELP', N'5', N'Transporte      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_SELP', N'6', N'Otro      ')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_SLM', N'1', N'Empleado      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_SLM', N'2', N'Independiente      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_SLM', N'3', N'Hogar      ')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_SELM', N'1', N'Educación      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_SELM', N'2', N'Comercio      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_SELM', N'3', N'Agricultura o ganadería      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_SELM', N'4', N'Ventas      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_SELP', N'5', N'Transporte      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'OP_SELM', N'6', N'Otro      ')
---------
---------
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MODALIDAD', N'1', N'Pregrado Presencial      ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MODALIDAD', N'2', N'Posgrado                 ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MODALIDAD', N'3', N'Educación a Distancia    ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MODALIDAD', N'4', N'Extensiones              ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MODALIDAD', N'5', N'Centro de Idiomas        ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MODALIDAD', N'6', N'Congresos y Seminarios   ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MODALIDAD', N'7', N'Cursos Libres            ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MODALIDAD', N'8', N'Cecar Virtual            ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MODALIDAD', N'9', N'Desarrollo Profesoral    ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'00  ', N'OTROS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'05  ', N'ANTIOQUIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'08  ', N'ATLANTICO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'11  ', N'D.C.')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'13  ', N'BOLIVAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'15  ', N'BOYACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'17  ', N'CALDAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'18  ', N'CAQUETA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'19  ', N'CAUCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'20  ', N'CESAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'23  ', N'CORDOBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'25  ', N'CUNDINAMARCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'27  ', N'CHOCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'41  ', N'HUILA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'44  ', N'LA GUAJIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'47  ', N'MAGDALENA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'50  ', N'META')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'52  ', N'NARIÑO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'54  ', N'NORTE SANTANDER')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'63  ', N'QUINDIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'66  ', N'RISARALDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'68  ', N'SANTANDER')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'70  ', N'SUCRE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'73  ', N'TOLIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'76  ', N'VALLE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'81  ', N'ARAUCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'85  ', N'CASANARE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'86  ', N'PUTUMAYO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'88  ', N'SAN ANDRES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'91  ', N'AMAZONAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'94  ', N'GUAINIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'95  ', N'GUAVIARE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'97  ', N'VAUPES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_EXP', N'99  ', N'VICHADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'00001', N'OTROS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05001', N'MEDELLIN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05002', N'ABEJORRAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05004', N'ABRIAQUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05021', N'ALEJANDRIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05030', N'AMAGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05031', N'AMALFI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05034', N'ANDES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05036', N'ANGELOPOLIS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05038', N'ANGOSTURA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05040', N'ANORI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05042', N'ANTIOQUIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05044', N'ANZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05045', N'APARTADO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05051', N'ARBOLETES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05055', N'ARGELIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05059', N'ARMENIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05079', N'BARBOSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05086', N'BELMIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05088', N'BELLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05091', N'BETANIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05093', N'BETULIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05101', N'BOLIVAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05107', N'BRICEÑO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05113', N'BURITICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05120', N'CACERES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05125', N'CAICEDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05129', N'CALDAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05134', N'CAMPAMENTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05138', N'CANASGORDAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05142', N'CARACOLI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05145', N'CARAMANTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05147', N'CAREPA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05148', N'CARMEN DE VIBORAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05150', N'CAROLINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05154', N'CAUCASIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05172', N'CHIGORODO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05190', N'CISNEROS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05197', N'COCORNA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05206', N'CONCEPCION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05209', N'CONCORDIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05212', N'COPACABANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05234', N'DABEIBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05237', N'DON MATIAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05240', N'EBEJICO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05250', N'EL BAGRE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05264', N'ENTRERRIOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05266', N'ENVIGADO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05282', N'FREDONIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05284', N'FRONTINO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05306', N'GIRALDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05308', N'GIRARDOTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05310', N'GOMEZ PLATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05313', N'GRANADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05315', N'GUADALUPE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05318', N'GUARNE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05321', N'GUATAPE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05347', N'HELICONIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05353', N'HISPANIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05360', N'ITAGUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05361', N'ITUANGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05364', N'JARDIN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05368', N'JERICO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05376', N'LA CEJA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05380', N'LA ESTRELLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05390', N'LA PINTADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05400', N'LA UNION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05411', N'LIBORINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05425', N'MACEO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05440', N'MARINILLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05467', N'MONTEBELLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05475', N'MURINDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05480', N'MUTATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05483', N'NARINO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05490', N'NECOCLI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05495', N'NECHI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05501', N'OLAYA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05541', N'PENOL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05543', N'PEQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05576', N'PUEBLORRICO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05579', N'PUERTO BERRIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05585', N'PUERTO NARE (LAMAG)')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05591', N'PUERTO TRIUNFO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05604', N'REMEDIOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05607', N'RETIRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05615', N'RIONEGRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05625', N'SAN FRANCISCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05628', N'SABANALARGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05631', N'SABANETA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05642', N'SALGAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05647', N'SAN ANDRES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05649', N'SAN CARLOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05652', N'SAN FRANCISCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05656', N'SAN JERONIMO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05658', N'SAN JOSE LA MONTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05659', N'SAN JUAN DE URABA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05660', N'SAN LUIS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05662', N'SAN JUAN DE URABA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05664', N'SAN PEDRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05665', N'SAN PEDRO URABA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05667', N'SAN RAFAEL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05670', N'SAN ROQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05674', N'SAN VICENTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05679', N'SANTA BARBARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05686', N'SANTA ROSA DE OSOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05690', N'SANTO DOMINGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05697', N'SANTUARIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05736', N'SEGOVIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05756', N'SONSON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05761', N'SOPETRAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05789', N'TAMESIS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05790', N'TARAZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05792', N'TARSO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05809', N'TITIRIBI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05819', N'TOLEDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05837', N'TURBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05842', N'URAMITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05847', N'URRAO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05854', N'VALDIVIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05856', N'VALPARAISO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05858', N'VEGACHI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05861', N'VENECIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05873', N'VIGIA DEL FUERTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05885', N'YALI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05887', N'YARUMAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05890', N'YOLOMBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05893', N'YONDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'05895', N'ZARAGOZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'08001', N'BARRANQUILLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'08078', N'BARANOA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'08137', N'CAMPO DE LA CRUZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'08141', N'CANDELARIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'08296', N'GALAPA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'08372', N'JUAN DE ACOSTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'08421', N'LURUACO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'08433', N'MALAMBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'08436', N'MANATI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'08520', N'PALMAR DE VARELA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'08549', N'PIOJO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'08558', N'POLONUEVO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'08560', N'PONEDERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'08573', N'PUERTO COLOMBIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'08606', N'REPELON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'08634', N'SABANAGRANDE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'08638', N'SABANALARGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'08675', N'SANTA LUCIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'08685', N'SANTO TOMAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'08758', N'SOLEDAD')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'08770', N'SUAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'08832', N'TUBARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'08849', N'USIACURI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'11001', N'SANTAFE DE BOGOTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'11102', N'BOSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'11265', N'ENGATIVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'11769', N'SUBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'11850', N'USME')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13001', N'CARTAGENA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13006', N'ACHI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13030', N'ALTOS DEL ROSARIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13042', N'ARENAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13052', N'ARJONA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13062', N'ARROYOHONDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13074', N'BARRANCO DE LOBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13140', N'CALAMAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13160', N'CANTAGALLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13188', N'CICUCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13212', N'CORDOBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13222', N'CLEMENCIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13244', N'EL CARMEN DE B0LIVAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13248', N'EL GUAMO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13268', N'EL PEÑON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13300', N'HATILLO DE LOBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13430', N'MAGANGUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13433', N'MAHATES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13440', N'MARGARITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13442', N'MARIA LA BAJA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13458', N'MONTECRISTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13468', N'MOMPOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13473', N'MORALES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13549', N'PINILLOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13580', N'REGIDOR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13600', N'RIO VIEJO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13620', N'SAN CRISTOBAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13647', N'SAN ESTANISLAO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13650', N'SAN FERNANDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13654', N'SAN JACINTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13655', N'SAN JACINTO DEL CAUCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13657', N'SAN JUAN NEPOMUCENO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13667', N'SAN MARTIN DE LOBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13670', N'SAN PABLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13673', N'SANTA CATALINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13683', N'SANTA ROSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13688', N'SANTA ROSA DEL SUR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13744', N'SIMITI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13760', N'SOPLAVIENTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13780', N'TALAIGUA NUEVO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13810', N'TIQUISIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13836', N'TURBACO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13838', N'TURBANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13873', N'VILLANUEVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'13894', N'ZAMBRANO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15001', N'TUNJA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15022', N'ALMEIDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15047', N'AQUITANIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15051', N'ARCABUCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15087', N'BELEN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15090', N'BERBEO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15092', N'BETEITIVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15097', N'BOAVITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15104', N'BOYACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15106', N'BRICEÑO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15109', N'BUENAVISTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15114', N'BUSBANZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15131', N'CALDAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15135', N'CAMPOHERMOSO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15162', N'CERINZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15172', N'CHINAVITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15176', N'CHIQUINQUIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15180', N'CHISCAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15183', N'CHITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15185', N'CHITARAQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15187', N'CHIVATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15189', N'CIENEGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15204', N'COMBITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15212', N'COPER')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15215', N'CORRALES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15218', N'COVARACHIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15223', N'CUBARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15224', N'CUCAITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15226', N'CUITIVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15232', N'CHIQUIZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15236', N'CHIVOR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15238', N'DUITAMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15244', N'EL COCUY')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15248', N'EL ESPINO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15272', N'FIRAVITOBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15276', N'FLORESTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15293', N'GACHANTIVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15296', N'GAMEZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15299', N'GARAGOA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15317', N'GUACAMAYAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15322', N'GUATEQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15325', N'GUAYATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15332', N'GUICAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15362', N'IZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15367', N'JENESANO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15368', N'JERICO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15377', N'LABRANZAGRANDE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15380', N'LA CAPILLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15401', N'LA VICTORIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15403', N'LA UVITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15407', N'VILLA DE LEYVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15425', N'MACANAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15442', N'MARIPI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15455', N'MIRAFLORES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15464', N'MONGUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15466', N'MONGUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15469', N'MONIQUIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15476', N'MOTAVITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15480', N'MUZO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15491', N'NOBSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15494', N'NUEVO COLON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15500', N'OICATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15507', N'OTANCHE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15511', N'PACHAVITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15514', N'PAEZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15516', N'PAIPA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15518', N'PAJARITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15522', N'PANQUEBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15531', N'PAUNA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15533', N'PAYA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15537', N'PAZ DE RIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15542', N'PESCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15550', N'PISBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15572', N'PUERTO BOYACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15580', N'QUIPAMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15599', N'RAMIRIQUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15600', N'RAQUIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15621', N'RONDON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15632', N'SABOYA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15638', N'SACHICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15646', N'SAMACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15660', N'SAN EDUARDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15664', N'SAN JOSE DE PARE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15667', N'SAN LUIS DE GACENO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15673', N'SAN MATEO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15676', N'SAN MIGUEL DE SEMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15681', N'SAN PABLO DE BORBUR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15686', N'SANTANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15690', N'SANTA MARIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15693', N'SANTA ROSA DE VITERBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15696', N'SANTA SOFIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15720', N'SATIVANORTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15723', N'SATIVASUR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15740', N'SIACHOQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15753', N'SOATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15755', N'SOCOTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15757', N'SOCHA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15759', N'SOGAMOSO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15761', N'SOMONDOCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15762', N'SORA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15763', N'SOTAQUIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15764', N'SORACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15774', N'SUSACON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15776', N'SUTAMARCHAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15778', N'SUTATENZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15790', N'TASCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15798', N'TENZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15804', N'TIBANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15806', N'TIBASOSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15808', N'TINJACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15810', N'TIPACOQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15814', N'TOCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15816', N'TOGUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15820', N'TOPAGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15822', N'TOTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15832', N'TUNUNGUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15835', N'TURMEQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15837', N'TUTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15839', N'TUTAZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15842', N'UMBITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15861', N'VENTAQUEMADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15879', N'VIRACACHA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'15897', N'ZETAQUIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'17001', N'MANIZALES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'17013', N'AGUADAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'17042', N'ANSERMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'17050', N'ARANZAZU')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'17088', N'BELALCAZAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'17174', N'CHINCHINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'17272', N'FILADELFIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'17380', N'LA DORADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'17388', N'LA MERCED')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'17433', N'MANZANARES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'17442', N'MARMATO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'17444', N'MARQUETALIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'17446', N'MARULANDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'17486', N'NEIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'17495', N'NORCASIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'17513', N'PACORA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'17524', N'PALESTINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'17541', N'PENSILVANIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'17614', N'RIOSUCIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'17616', N'RISARALDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'17653', N'SALAMINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'17662', N'SAMANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'17665', N'SAN JOSE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'17777', N'SUPIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'17867', N'VICTORIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'17873', N'VILLAMARIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'17877', N'VITERBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'18001', N'FLORENCIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'18029', N'ALBANIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'18094', N'BELEN ANDAQUIES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'18150', N'CARTAGENA DEL CHAIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'18205', N'CURILLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'18247', N'EL DONCELLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'18256', N'EL PAUJIL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'18410', N'LA MONTANITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'18460', N'MILAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'18479', N'MORELIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'18592', N'PUERTO RICO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'18610', N'SAN JOSE DEL FRAGUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'18753', N'SAN VICENTE DEL CAGUAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'18756', N'SOLANO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'18785', N'SOLITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'18860', N'VALPARAISO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19001', N'POPAYAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19022', N'ALMAGUER')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19050', N'ARGELIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19075', N'BALBOA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19100', N'BOLIVAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19110', N'BUENOS AIRES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19130', N'CAJIBIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19137', N'CALDONO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19142', N'CALOTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19212', N'CORINTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19256', N'EL TAMBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19290', N'FLORENCIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19318', N'GUAPI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19355', N'INZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19364', N'JAMBALO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19392', N'LA SIERRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19397', N'LA VEGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19418', N'LOPEZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19450', N'MERCADERES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19455', N'MIRANDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19473', N'MORALES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19513', N'PADILLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19517', N'PAEZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19532', N'PATIA (EL BORDO)')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19533', N'PIAMONTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19548', N'PIENDAMO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19573', N'PUERTO TEJADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19585', N'PURACE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19622', N'ROSAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19693', N'SAN SEBASTIAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19698', N'SANTANDER DE QUILICHAO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19701', N'SANTA ROSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19743', N'SILVIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19760', N'SOTARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19780', N'SUAREZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19785', N'SUCRE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19807', N'TIMBIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19809', N'TIMBIQUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19821', N'TORIBIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19824', N'TOTORO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'19845', N'VILLA RICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'20001', N'VALLEDUPAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'20011', N'AGUACHICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'20013', N'AGUSTIN CODAZZI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'20032', N'ASTREA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'20045', N'BECERRIL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'20060', N'BOSCONIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'20175', N'CHIMICHAGUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'20178', N'CHIRIGUANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'20228', N'CURUMANI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'20238', N'EL COPEY')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'20250', N'EL PASO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'20295', N'GAMARRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'20310', N'GONZALEZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'20383', N'LA GLORIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'20400', N'LA JAGUA DE IBIRICO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'20443', N'MANAURE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'20517', N'PAILITAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'20550', N'PELAYA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'20570', N'PUEBLO BELLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'20614', N'RIO DE ORO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'20621', N'LA PAZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'20710', N'SAN ALBERTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'20750', N'SAN DIEGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'20770', N'SAN MARTIN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'20787', N'TAMALAMEQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'23001', N'MONTERIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'23068', N'AYAPEL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'23079', N'BUENAVISTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'23090', N'CANALETE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'23162', N'CERETE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'23168', N'CHIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'23182', N'CHINU')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'23189', N'CIENAGA DE ORO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'23300', N'COTORRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'23350', N'LA APARTADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'23417', N'LORICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'23419', N'LOS CORDOBAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'23464', N'MOMIL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'23466', N'MONTELIBANO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'23500', N'MOÑITOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'23555', N'PLANETA RICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'23570', N'PUEBLO NUEVO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'23574', N'PUERTO ESCONDIDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'23580', N'PUERTO LIBERTADOR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'23586', N'PURISIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'23660', N'SAHAGUN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'23670', N'SAN ANDRES DE SOTAVENTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'23672', N'SAN ANTERO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'23675', N'SAN BERNARDO DEL VIENTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'23678', N'SAN CARLOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'23686', N'SAN PELAYO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'23807', N'TIERRALTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'23815', N'TUCHIN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'23855', N'VALENCIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25001', N'AGUA DE DIOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25019', N'ALBAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25035', N'ANAPOIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25040', N'ANOLAIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25053', N'ARBELAEZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25086', N'BELTRAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25095', N'BITUIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25099', N'BOJACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25120', N'CABRERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25123', N'CACHIPAY')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25126', N'CAJICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25148', N'CAPARRAPI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25151', N'CAQUEZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25154', N'CARMEN DE CARUPA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25168', N'CHAGUANI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25175', N'CHIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25178', N'CHIPAQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25181', N'CHOACHI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25183', N'CHOCONTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25200', N'COGUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25214', N'COTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25224', N'CUCUNUBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25245', N'EL COLEGIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25258', N'EL PEÑON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25260', N'EL ROSAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25269', N'FACATATIVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25279', N'FOMEQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25281', N'FOSCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25286', N'FUNZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25288', N'FUQUENE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25290', N'FUSAGASUGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25293', N'GACHALA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25295', N'GACHANCIPA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25297', N'GACHETA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25299', N'GAMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25307', N'GIRARDOT')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25312', N'GRANADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25317', N'GUACHETA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25320', N'GUADUAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25322', N'GUASCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25324', N'GUATAQUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25326', N'GUATAVITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25328', N'GUAYABAL DE SIQUIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25335', N'GUAYABETAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25339', N'GUTIERREZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25368', N'JERUSALEN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25372', N'JUNIN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25377', N'LA CALERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25386', N'LA MESA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25394', N'LA PALMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25398', N'LA PE{A')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25402', N'LA VEGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25407', N'LENGUAZAQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25426', N'MACHETA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25430', N'MADRID')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25436', N'MANTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25438', N'MEDINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25473', N'MOSQUERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25483', N'NARIÑO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25486', N'NEMOCON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25488', N'NILO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25489', N'NIMAIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25491', N'NOCAIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25506', N'VENECIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25513', N'PACHO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25518', N'PAIME')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25524', N'PANDI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25530', N'PARATEBUENO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25535', N'PASCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25572', N'PUERTO SALGAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25580', N'PULI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25592', N'QUEBRADANEGRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25594', N'QUETAME')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25596', N'QUIPILE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25599', N'APULO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25612', N'RICAURTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25645', N'SAN ANTONIO DEL TEQUENDAMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25649', N'SAN BERNARDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25653', N'SAN CAYETANO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25658', N'SAN FRANCISCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25662', N'SAN JUAN DE RIO SECO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25718', N'SASAIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25736', N'SESQUILE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25740', N'SIBATE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25743', N'SILVANIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25745', N'SIMIJACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25754', N'SOACHA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25758', N'SOPO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25769', N'SUBACHOQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25772', N'SUESCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25777', N'SUPATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25779', N'SUSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25781', N'SUTATAUSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25785', N'TABIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25793', N'TAUSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25797', N'TENA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25799', N'TENJO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25805', N'TIBACUY')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25807', N'TIBIRITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25815', N'TOCAIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25817', N'TOCANCIPA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25823', N'TOPAIPI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25839', N'UBALA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25841', N'UBAQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25843', N'UBATE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25845', N'UNE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25851', N'UTICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25862', N'VERGARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25867', N'VIANI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25871', N'VILLAGOMEZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25873', N'VILLAPINZON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25875', N'VILLETA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25878', N'VIOTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25885', N'YACOPI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25898', N'ZIPACON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'25899', N'ZIPAQUIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27001', N'QUIBDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27006', N'ACANDI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27025', N'ALTO BAUDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27050', N'ATRATO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27073', N'BAGADO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27075', N'BAHIA SOLANO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27077', N'BAJO BAUDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27086', N'BELEN DE BAJIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27099', N'BOJAYA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27135', N'EL CANTON DEL SAN PABLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27150', N'CARMEN DEL DARIEN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27160', N'CERTEGUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27205', N'CONDOTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27245', N'EL CARMEN DE ATRATO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27250', N'EL LITORAL DEL SAN JUAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27361', N'ITSMINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27372', N'JURADO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27413', N'LLORO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27425', N'MEDIO ATRATO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27430', N'MEDIO BAUDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27450', N'MEDIO SAN JUAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27491', N'NOVITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27495', N'NUQUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27580', N'RIO IRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27600', N'RIO QUITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27615', N'RIOSUCIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27660', N'SAN JOSE DEL PALMAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27745', N'SIPI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27787', N'TADO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27800', N'UNGUIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'27810', N'UNION PANAMERICANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41001', N'NEIVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41006', N'ACEVEDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41013', N'AGRADO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41016', N'AIPE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41020', N'ALGECIRAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41026', N'ALTAMIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41078', N'BARAYA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41132', N'CAMPOALEGRE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41206', N'COLOMBIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41244', N'ELIAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41298', N'GARZON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41306', N'GIGANTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41319', N'GUADALUPE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41349', N'HOBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41357', N'IQUIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41359', N'ISNOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41378', N'LA ARGENTINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41396', N'LA PLATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41483', N'NATAGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41503', N'OPORAPA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41518', N'PAICOL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41524', N'PALERMO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41530', N'PALESTINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41548', N'PITAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41551', N'PITALITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41615', N'RIVERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41660', N'SALADOBLANCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41668', N'SAN AGUSTIN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41676', N'SANTA MARIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41770', N'SUAZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41791', N'TARQUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41797', N'TESALIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41799', N'TELLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41801', N'TERUEL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41807', N'TIMANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41872', N'VILLAVIEJA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'41885', N'YAGUARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'44001', N'RIOHACHA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'44035', N'ALBANIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'44078', N'BARRANCAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'44090', N'DIBULLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'44098', N'DISTRACCION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'44110', N'EL MOLINO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'44279', N'FONSECA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'44378', N'HATONUEVO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'44420', N'LA JAGUA DEL PILAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'44430', N'MAICAO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'44560', N'MANAURE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'44650', N'SAN JUAN DEL CESAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'44847', N'URIBIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'44855', N'URUMITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'44874', N'VILLANUEVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47001', N'SANTA MARTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47030', N'ALGARROBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47053', N'ARACATACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47058', N'ARIGUANI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47161', N'CERRO SAN ANTONIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47170', N'CHIVOLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47189', N'CIENAGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47205', N'CONCORDIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47245', N'EL BANCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47258', N'EL PIÑON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47268', N'EL RETEN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47288', N'FUNDACION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47318', N'GUAMAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47460', N'NUEVA GRANADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47541', N'PEDRAZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47545', N'PIJIÑO DEL CARMEN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47551', N'PIVIJAY')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47555', N'PLATO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47570', N'PUEBLOVIEJO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47605', N'REMOLINO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47660', N'SABANAS DE SAN ANGEL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47675', N'SALAMINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47692', N'SAN SEBASTIAN DE BUENAVISTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47703', N'SAN ZENON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47707', N'SANTA ANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47745', N'SITIONUEVO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47798', N'TENERIFE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47799', N'SANTA BARBARA DE PINTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47960', N'ZAPAYAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'47980', N'ZONA BANANERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'50001', N'VILLAVICENCIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'50006', N'ACACIAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'50110', N'BARRANCA DE UPIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'50124', N'CABUYARO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'50150', N'CASTILLA LA NUEVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'50223', N'CUBARRAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'50226', N'CUMARAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'50245', N'EL CALVARIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'50251', N'EL CASTILLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'50270', N'EL DORADO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'50287', N'FUENTE DE ORO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'50313', N'GRANADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'50318', N'GUAMAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'50325', N'MAPIRIPAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'50330', N'MESETAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'50350', N'LA MACARENA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'50370', N'URIBE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'50400', N'LEJANIAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'50450', N'PUERTO CONCORDIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'50568', N'PUERTO GAITAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'50573', N'PUERTO LOPEZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'50577', N'PUERTO LLERAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'50590', N'PUERTO RICO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'50606', N'RESTREPO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'50680', N'SAN CARLOS DE GUAROA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'50683', N'SAN JUAN DE ARAMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'50686', N'SAN JUANITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'50689', N'SAN MARTIN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'50711', N'VISTA HERMOSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52001', N'PASTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52019', N'ALBAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52022', N'ALDANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52036', N'ANCUYA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52051', N'ARBOLEDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52079', N'BARBACOAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52083', N'BELEN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52110', N'BUESACO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52203', N'COLON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52207', N'CONSACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52210', N'CONTADERO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52215', N'CORDOBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52224', N'CUASPUD')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52227', N'CUMBAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52233', N'CUMBITARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52240', N'CHACHAGÜI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52250', N'EL CHARCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52254', N'EL PEÑOL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52256', N'EL ROSARIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52258', N'EL TABLON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52260', N'EL TAMBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52287', N'FUNES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52317', N'GUACHUCAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52320', N'GUAITARILLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52323', N'GUALMATAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52352', N'ILES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52354', N'IMUES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52356', N'IPIALES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52378', N'LA CRUZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52381', N'LA FLORIDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52385', N'LA LLANADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52390', N'LA TOLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52399', N'LA UNION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52405', N'LEIVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52411', N'LINARES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52418', N'LOS ANDES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52427', N'MAGÜI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52435', N'MALLAMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52473', N'MOSQUERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52480', N'NARIÑO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52490', N'OLAYA HERRERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52506', N'OSPINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52520', N'FRANCISCO PIZARRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52540', N'POLICARPA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52560', N'POTOSI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52565', N'PROVIDENCIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52573', N'PUERRES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52585', N'PUPIALES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52612', N'RICAURTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52621', N'ROBERTO PAYAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52678', N'SAMANIEGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52683', N'SANDONA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52685', N'SAN BERNARDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52687', N'SAN LORENZO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52693', N'SAN PABLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52694', N'SAN PEDRO DE CARTAGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52696', N'SANTA BARBARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52699', N'SANTACRUZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52720', N'SAPUYES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52786', N'TAMINANGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52788', N'TANGUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52835', N'TUMACO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52838', N'TUQUERRES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'52885', N'YACUANQUER')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54001', N'CUCUTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54003', N'ABREGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54051', N'ARBOLEDAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54099', N'BOCHALEMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54109', N'BUCARASICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54125', N'CACOTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54128', N'CACHIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54172', N'CHINACOTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54174', N'CHITAGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54206', N'CONVENCION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54223', N'CUCUTILLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54239', N'DURANIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54245', N'EL CARMEN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54250', N'EL TARRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54261', N'EL ZULIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54313', N'GRAMALOTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54344', N'HACARI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54347', N'HERRAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54377', N'LABATECA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54385', N'LA ESPERANZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54398', N'LA PLAYA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54405', N'LOS PATIOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54418', N'LOURDES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54480', N'MUTISCUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54498', N'OCANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54518', N'PAMPLONA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54520', N'PAMPLONITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54553', N'PUERTO SANTANDER')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54599', N'RAGONVALIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54660', N'SALAZAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54670', N'SAN CALIXTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54673', N'SAN CAYETANO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54680', N'SANTIAGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54720', N'SARDINATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54743', N'SILOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54800', N'TEORAMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54810', N'TIBU')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54820', N'TOLEDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54871', N'VILLA CARO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'54874', N'VILLA ROSARIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'63001', N'ARMENIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'63111', N'BUENAVISTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'63130', N'CALARCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'63190', N'CIRCASIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'63212', N'CORDOBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'63272', N'FILANDIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'63302', N'GENOVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'63401', N'LA TEBAIDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'63470', N'MONTENEGRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'63548', N'PIJAO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'63594', N'QUIMBAYA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'63690', N'SALENTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'66001', N'PEREIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'66045', N'APIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'66075', N'BALBOA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'66088', N'BELEN DE UMBRIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'66170', N'DOS QUEBRADAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'66318', N'GUATICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'66383', N'LA CELIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'66400', N'LA VIRGINIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'66440', N'MARSELLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'66456', N'MISTRATO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'66572', N'PUEBLO RICO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'66594', N'QUINCHIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'66682', N'SANTA ROSA DE CABAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'66687', N'SANTUARIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68001', N'BUCARAMANGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68013', N'AGUADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68020', N'ALBANIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68051', N'ARATOCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68077', N'BARBOSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68079', N'BARICHARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68081', N'BARRANCABERMEJA')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68092', N'BETULIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68101', N'BOLIVAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68121', N'CABRERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68132', N'CALIFORNIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68147', N'CAPITANEJO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68152', N'CARCASI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68160', N'CEPITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68162', N'CERRITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68167', N'CHARALA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68169', N'CHARTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68176', N'CHIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68179', N'CHIPATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68190', N'CIMITARRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68207', N'CONCEPCION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68209', N'CONFINES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68211', N'CONTRATACION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68217', N'COROMORO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68229', N'CURITI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68235', N'EL CARMEN DE CHUCURI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68245', N'EL GUACAMAYO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68250', N'EL PEÑON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68255', N'EL PLAYON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68264', N'ENCINO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68266', N'ENCISO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68271', N'FLORIAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68276', N'FLORIDABLANCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68296', N'GALAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68298', N'GAMBITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68307', N'GIRON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68318', N'GUACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68320', N'GUADALUPE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68322', N'GUAPOTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68324', N'GUAVATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68327', N'GUEPSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68344', N'HATO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68368', N'JESUS MARIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68370', N'JORDAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68377', N'LA BELLEZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68385', N'LANDAZURI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68397', N'LA PAZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68406', N'LEBRIJA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68418', N'LOS SANTOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68425', N'MACARAVITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68432', N'MALAGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68444', N'MATANZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68464', N'MOGOTES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68468', N'MOLAGAVITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68498', N'OCAMONTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68500', N'OIBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68502', N'ONZAGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68522', N'PALMAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68524', N'PALMAS DEL SOCORRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68533', N'PARAMO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68547', N'PIEDECUESTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68549', N'PINCHOTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68572', N'PUENTE NACIONAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68573', N'PUERTO PARRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68575', N'PUERTO WILCHES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68615', N'RIONEGRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68655', N'SABANA DE TORRES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68669', N'SAN ANDRES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68673', N'SAN BENITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68679', N'SAN GIL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68682', N'SAN JOAQUIN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68684', N'SAN JOSE DE MIRANDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68686', N'SAN MIGUEL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68689', N'SAN VICENTE DE CHUCURI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68705', N'SANTA BARBARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68720', N'SANTA HELENA DEL OPON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68745', N'SIMACOTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68755', N'SOCORRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68770', N'SUAITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68773', N'SUCRE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68780', N'SURATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68820', N'TONA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68855', N'VALLE DE SAN JOSE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68861', N'VELEZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68867', N'VETAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68872', N'VILLANUEVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'68895', N'ZAPATOCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'70001', N'SINCELEJO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'70110', N'BUENAVISTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'70124', N'CAIMITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'70204', N'COLOSO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'70215', N'COROZAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'70221', N'COVEÑAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'70230', N'CHALAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'70233', N'EL ROBLE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'70235', N'GALERAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'70265', N'GUARANDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'70400', N'LA UNION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'70418', N'LOS PALMITOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'70429', N'MAJAGUAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'70473', N'MORROA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'70508', N'OVEJAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'70523', N'PALMITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'70670', N'SAMPUES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'70678', N'SAN BENITO ABAD')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'70702', N'SAN JUAN DE BETULIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'70708', N'SAN MARCOS')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'70713', N'SAN ONOFRE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'70717', N'SAN PEDRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'70742', N'SINCE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'70771', N'SUCRE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'70820', N'TOLU')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'70823', N'TOLUVIEJO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73001', N'IBAGUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73024', N'ALPUJARRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73026', N'ALVARADO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73030', N'AMBALEMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73043', N'ANZOATEGUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73055', N'ARMERO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73067', N'ATACO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73124', N'CAJAMARCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73148', N'CARMEN DE APICALA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73152', N'CASABIANCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73168', N'CHAPARRAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73200', N'COELLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73217', N'COYAIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73226', N'CUNDAY')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73236', N'DOLORES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73268', N'ESPINAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73270', N'FALAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73275', N'FLANDES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73283', N'FRESNO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73319', N'GUAMO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73347', N'HERVEO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73349', N'HONDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73352', N'ICONONZO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73408', N'LERIDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73411', N'LIBANO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73443', N'MARIQUITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73449', N'MELGAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73461', N'MURILLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73483', N'NATAGAIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73504', N'ORTEGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73520', N'PALOCABILDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73547', N'PIEDRAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73555', N'PLANADAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73563', N'PRADO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73585', N'PURIFICACION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73616', N'RIOBLANCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73622', N'RONCESVALLES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73624', N'ROVIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73671', N'SALDANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73675', N'SAN ANTONIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73678', N'SAN LUIS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73686', N'SANTA ISABEL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73770', N'SUAREZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73854', N'VALLE DE SAN JUAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73861', N'VENADILLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73870', N'VILLAHERMOSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'73873', N'VILLARRICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76001', N'CALI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76020', N'ALCALA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76036', N'ANDALUCIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76041', N'ANSERMANUEVO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76054', N'ARGELIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76100', N'BOLIVAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76109', N'BUENAVENTURA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76111', N'BUGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76113', N'BUGALAGRANDE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76122', N'CAICEDONIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76126', N'CALIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76130', N'CANDELARIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76147', N'CARTAGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76233', N'DAGUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76243', N'EL AGUILA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76246', N'EL CAIRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76248', N'EL CERRITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76250', N'EL DOVIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76275', N'FLORIDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76306', N'GINEBRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76318', N'GUACARI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76364', N'JAMUNDI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76377', N'LA CUMBRE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76400', N'LA UNION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76403', N'LA VICTORIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76497', N'OBANDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76520', N'PALMIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76563', N'PRADERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76606', N'RESTREPO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76616', N'RIOFRIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76622', N'ROLDANILLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76670', N'SAN PEDRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76736', N'SEVILLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76823', N'TORO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76828', N'TRUJILLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76834', N'TULUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76845', N'ULLOA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76863', N'VERSALLES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76869', N'VIJES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76890', N'YOTOCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76892', N'YUMBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'76895', N'ZARZAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'81001', N'ARAUCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'81065', N'ARAUQUITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'81220', N'CRAVO NORTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'81300', N'FORTUL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'81591', N'PUERTO RONDON')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'81736', N'SARAVENA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'81794', N'TAME')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'85001', N'YOPAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'85010', N'AGUAZUL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'85015', N'CHAMEZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'85125', N'HATO COROZAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'85136', N'LA SALINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'85139', N'MANI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'85162', N'MONTERREY')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'85225', N'NUNCHIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'85230', N'OROCUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'85250', N'PAZ DE ARIPORO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'85263', N'PORE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'85279', N'RECETOR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'85300', N'SABANALARGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'85315', N'SACAMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'85325', N'S LUIS D PALENQ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'85400', N'TAMARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'85410', N'TAURAMENA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'85430', N'TRINIDAD')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'85440', N'VILLANUEVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'86001', N'MOCOA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'86219', N'COLON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'86320', N'ORITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'86568', N'PUERTO ASIS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'86569', N'PUERTO CAICEDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'86571', N'PUERTO GUZMAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'86573', N'PTO. LEGUIZAMO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'86749', N'SIBUNDOY')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'86755', N'SAN FRANCISCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'86757', N'SAN MIGUEL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'86760', N'SANTIAGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'86865', N'VALLE DEL GUAMUEZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'86885', N'VILLAGARZON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'88001', N'SAN ANDRES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'88564', N'PROVIDENCIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'91001', N'LETICIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'91263', N'EL ENCANTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'91405', N'LA CHORRERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'91407', N'LA PEDRERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'91430', N'LA VICTORIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'91460', N'MIRITI - PARANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'91530', N'PUERTO ALEGRIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'91536', N'PUERTO ARICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'91540', N'PUERTO NARIÑO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'91669', N'PUERTO SANTANDER')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'91798', N'TARAPACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'94001', N'INIRIDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'94343', N'BARRANCO MINAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'94663', N'MAPIRIPANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'94883', N'SAN FELIPE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'94884', N'PUERTO COLOMBIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'94885', N'LA GUADALUPE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'94886', N'CACAHUAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'94887', N'PANA PANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'94888', N'MORICHAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'95001', N'S.JOSE GUAVIARE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'95015', N'CALAMAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'95025', N'EL RETORNO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'95200', N'MIRAFLORES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'97001', N'MITU')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'97161', N'CARURU')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'97511', N'PACOA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'97666', N'TARAIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'97777', N'PAPUNAUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'97889', N'YAVARATE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'99001', N'PUERTO CARRENO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'99524', N'LA PRIMAVERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'99624', N'SANTA ROSALIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'99760', N'SAN JOSE DE OCUNE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'99773', N'CUMARIBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_EXP', N'99999', N'EXTRANJERO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'TIPO_DI', N'1', N'Cédula de ciudadania')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'TIPO_DI', N'2', N'Tarjeta de identidad')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'TIPO_DI', N'3', N'Cédula de Extranjería')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'TIPO_DI', N'4', N'Pasaporte')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'GENERO', N'F', N'Femenino')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'GENERO', N'M', N'Masculino')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'00  ', N'OTROS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'05  ', N'ANTIOQUIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'08  ', N'ATLANTICO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'11  ', N'D.C.')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'13  ', N'BOLIVAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'15  ', N'BOYACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'17  ', N'CALDAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'18  ', N'CAQUETA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'19  ', N'CAUCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'20  ', N'CESAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'23  ', N'CORDOBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'25  ', N'CUNDINAMARCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'27  ', N'CHOCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'41  ', N'HUILA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'44  ', N'LA GUAJIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'47  ', N'MAGDALENA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'50  ', N'META')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'52  ', N'NARIÑO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'54  ', N'NORTE SANTANDER')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'63  ', N'QUINDIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'66  ', N'RISARALDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'68  ', N'SANTANDER')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'70  ', N'SUCRE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'73  ', N'TOLIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'76  ', N'VALLE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'81  ', N'ARAUCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'85  ', N'CASANARE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'86  ', N'PUTUMAYO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'88  ', N'SAN ANDRES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'91  ', N'AMAZONAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'94  ', N'GUAINIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'95  ', N'GUAVIARE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'97  ', N'VAUPES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_RES', N'99  ', N'VICHADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'00001', N'OTROS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05001', N'MEDELLIN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05002', N'ABEJORRAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05004', N'ABRIAQUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05021', N'ALEJANDRIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05030', N'AMAGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05031', N'AMALFI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05034', N'ANDES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05036', N'ANGELOPOLIS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05038', N'ANGOSTURA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05040', N'ANORI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05042', N'ANTIOQUIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05044', N'ANZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05045', N'APARTADO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05051', N'ARBOLETES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05055', N'ARGELIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05059', N'ARMENIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05079', N'BARBOSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05086', N'BELMIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05088', N'BELLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05091', N'BETANIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05093', N'BETULIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05101', N'BOLIVAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05107', N'BRICEÑO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05113', N'BURITICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05120', N'CACERES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05125', N'CAICEDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05129', N'CALDAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05134', N'CAMPAMENTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05138', N'CANASGORDAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05142', N'CARACOLI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05145', N'CARAMANTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05147', N'CAREPA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05148', N'CARMEN DE VIBORAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05150', N'CAROLINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05154', N'CAUCASIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05172', N'CHIGORODO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05190', N'CISNEROS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05197', N'COCORNA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05206', N'CONCEPCION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05209', N'CONCORDIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05212', N'COPACABANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05234', N'DABEIBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05237', N'DON MATIAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05240', N'EBEJICO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05250', N'EL BAGRE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05264', N'ENTRERRIOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05266', N'ENVIGADO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05282', N'FREDONIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05284', N'FRONTINO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05306', N'GIRALDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05308', N'GIRARDOTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05310', N'GOMEZ PLATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05313', N'GRANADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05315', N'GUADALUPE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05318', N'GUARNE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05321', N'GUATAPE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05347', N'HELICONIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05353', N'HISPANIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05360', N'ITAGUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05361', N'ITUANGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05364', N'JARDIN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05368', N'JERICO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05376', N'LA CEJA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05380', N'LA ESTRELLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05390', N'LA PINTADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05400', N'LA UNION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05411', N'LIBORINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05425', N'MACEO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05440', N'MARINILLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05467', N'MONTEBELLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05475', N'MURINDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05480', N'MUTATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05483', N'NARINO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05490', N'NECOCLI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05495', N'NECHI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05501', N'OLAYA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05541', N'PENOL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05543', N'PEQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05576', N'PUEBLORRICO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05579', N'PUERTO BERRIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05585', N'PUERTO NARE (LAMAG)')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05591', N'PUERTO TRIUNFO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05604', N'REMEDIOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05607', N'RETIRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05615', N'RIONEGRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05625', N'SAN FRANCISCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05628', N'SABANALARGA')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05631', N'SABANETA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05642', N'SALGAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05647', N'SAN ANDRES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05649', N'SAN CARLOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05652', N'SAN FRANCISCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05656', N'SAN JERONIMO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05658', N'SAN JOSE LA MONTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05659', N'SAN JUAN DE URABA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05660', N'SAN LUIS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05662', N'SAN JUAN DE URABA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05664', N'SAN PEDRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05665', N'SAN PEDRO URABA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05667', N'SAN RAFAEL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05670', N'SAN ROQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05674', N'SAN VICENTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05679', N'SANTA BARBARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05686', N'SANTA ROSA DE OSOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05690', N'SANTO DOMINGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05697', N'SANTUARIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05736', N'SEGOVIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05756', N'SONSON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05761', N'SOPETRAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05789', N'TAMESIS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05790', N'TARAZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05792', N'TARSO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05809', N'TITIRIBI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05819', N'TOLEDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05837', N'TURBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05842', N'URAMITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05847', N'URRAO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05854', N'VALDIVIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05856', N'VALPARAISO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05858', N'VEGACHI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05861', N'VENECIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05873', N'VIGIA DEL FUERTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05885', N'YALI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05887', N'YARUMAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05890', N'YOLOMBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05893', N'YONDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'05895', N'ZARAGOZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'08001', N'BARRANQUILLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'08078', N'BARANOA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'08137', N'CAMPO DE LA CRUZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'08141', N'CANDELARIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'08296', N'GALAPA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'08372', N'JUAN DE ACOSTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'08421', N'LURUACO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'08433', N'MALAMBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'08436', N'MANATI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'08520', N'PALMAR DE VARELA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'08549', N'PIOJO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'08558', N'POLONUEVO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'08560', N'PONEDERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'08573', N'PUERTO COLOMBIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'08606', N'REPELON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'08634', N'SABANAGRANDE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'08638', N'SABANALARGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'08675', N'SANTA LUCIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'08685', N'SANTO TOMAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'08758', N'SOLEDAD')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'08770', N'SUAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'08832', N'TUBARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'08849', N'USIACURI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'11001', N'SANTAFE DE BOGOTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'11102', N'BOSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'11265', N'ENGATIVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'11769', N'SUBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'11850', N'USME')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13001', N'CARTAGENA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13006', N'ACHI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13030', N'ALTOS DEL ROSARIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13042', N'ARENAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13052', N'ARJONA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13062', N'ARROYOHONDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13074', N'BARRANCO DE LOBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13140', N'CALAMAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13160', N'CANTAGALLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13188', N'CICUCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13212', N'CORDOBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13222', N'CLEMENCIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13244', N'EL CARMEN DE B0LIVAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13248', N'EL GUAMO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13268', N'EL PEÑON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13300', N'HATILLO DE LOBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13430', N'MAGANGUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13433', N'MAHATES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13440', N'MARGARITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13442', N'MARIA LA BAJA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13458', N'MONTECRISTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13468', N'MOMPOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13473', N'MORALES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13549', N'PINILLOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13580', N'REGIDOR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13600', N'RIO VIEJO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13620', N'SAN CRISTOBAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13647', N'SAN ESTANISLAO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13650', N'SAN FERNANDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13654', N'SAN JACINTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13655', N'SAN JACINTO DEL CAUCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13657', N'SAN JUAN NEPOMUCENO')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13667', N'SAN MARTIN DE LOBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13670', N'SAN PABLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13673', N'SANTA CATALINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13683', N'SANTA ROSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13688', N'SANTA ROSA DEL SUR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13744', N'SIMITI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13760', N'SOPLAVIENTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13780', N'TALAIGUA NUEVO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13810', N'TIQUISIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13836', N'TURBACO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13838', N'TURBANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13873', N'VILLANUEVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'13894', N'ZAMBRANO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15001', N'TUNJA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15022', N'ALMEIDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15047', N'AQUITANIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15051', N'ARCABUCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15087', N'BELEN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15090', N'BERBEO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15092', N'BETEITIVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15097', N'BOAVITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15104', N'BOYACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15106', N'BRICEÑO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15109', N'BUENAVISTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15114', N'BUSBANZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15131', N'CALDAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15135', N'CAMPOHERMOSO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15162', N'CERINZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15172', N'CHINAVITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15176', N'CHIQUINQUIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15180', N'CHISCAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15183', N'CHITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15185', N'CHITARAQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15187', N'CHIVATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15189', N'CIENEGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15204', N'COMBITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15212', N'COPER')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15215', N'CORRALES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15218', N'COVARACHIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15223', N'CUBARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15224', N'CUCAITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15226', N'CUITIVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15232', N'CHIQUIZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15236', N'CHIVOR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15238', N'DUITAMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15244', N'EL COCUY')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15248', N'EL ESPINO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15272', N'FIRAVITOBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15276', N'FLORESTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15293', N'GACHANTIVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15296', N'GAMEZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15299', N'GARAGOA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15317', N'GUACAMAYAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15322', N'GUATEQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15325', N'GUAYATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15332', N'GUICAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15362', N'IZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15367', N'JENESANO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15368', N'JERICO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15377', N'LABRANZAGRANDE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15380', N'LA CAPILLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15401', N'LA VICTORIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15403', N'LA UVITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15407', N'VILLA DE LEYVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15425', N'MACANAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15442', N'MARIPI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15455', N'MIRAFLORES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15464', N'MONGUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15466', N'MONGUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15469', N'MONIQUIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15476', N'MOTAVITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15480', N'MUZO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15491', N'NOBSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15494', N'NUEVO COLON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15500', N'OICATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15507', N'OTANCHE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15511', N'PACHAVITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15514', N'PAEZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15516', N'PAIPA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15518', N'PAJARITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15522', N'PANQUEBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15531', N'PAUNA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15533', N'PAYA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15537', N'PAZ DE RIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15542', N'PESCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15550', N'PISBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15572', N'PUERTO BOYACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15580', N'QUIPAMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15599', N'RAMIRIQUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15600', N'RAQUIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15621', N'RONDON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15632', N'SABOYA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15638', N'SACHICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15646', N'SAMACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15660', N'SAN EDUARDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15664', N'SAN JOSE DE PARE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15667', N'SAN LUIS DE GACENO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15673', N'SAN MATEO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15676', N'SAN MIGUEL DE SEMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15681', N'SAN PABLO DE BORBUR')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15686', N'SANTANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15690', N'SANTA MARIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15693', N'SANTA ROSA DE VITERBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15696', N'SANTA SOFIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15720', N'SATIVANORTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15723', N'SATIVASUR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15740', N'SIACHOQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15753', N'SOATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15755', N'SOCOTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15757', N'SOCHA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15759', N'SOGAMOSO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15761', N'SOMONDOCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15762', N'SORA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15763', N'SOTAQUIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15764', N'SORACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15774', N'SUSACON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15776', N'SUTAMARCHAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15778', N'SUTATENZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15790', N'TASCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15798', N'TENZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15804', N'TIBANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15806', N'TIBASOSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15808', N'TINJACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15810', N'TIPACOQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15814', N'TOCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15816', N'TOGUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15820', N'TOPAGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15822', N'TOTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15832', N'TUNUNGUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15835', N'TURMEQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15837', N'TUTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15839', N'TUTAZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15842', N'UMBITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15861', N'VENTAQUEMADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15879', N'VIRACACHA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'15897', N'ZETAQUIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'17001', N'MANIZALES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'17013', N'AGUADAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'17042', N'ANSERMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'17050', N'ARANZAZU')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'17088', N'BELALCAZAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'17174', N'CHINCHINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'17272', N'FILADELFIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'17380', N'LA DORADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'17388', N'LA MERCED')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'17433', N'MANZANARES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'17442', N'MARMATO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'17444', N'MARQUETALIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'17446', N'MARULANDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'17486', N'NEIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'17495', N'NORCASIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'17513', N'PACORA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'17524', N'PALESTINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'17541', N'PENSILVANIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'17614', N'RIOSUCIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'17616', N'RISARALDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'17653', N'SALAMINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'17662', N'SAMANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'17665', N'SAN JOSE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'17777', N'SUPIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'17867', N'VICTORIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'17873', N'VILLAMARIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'17877', N'VITERBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'18001', N'FLORENCIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'18029', N'ALBANIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'18094', N'BELEN ANDAQUIES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'18150', N'CARTAGENA DEL CHAIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'18205', N'CURILLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'18247', N'EL DONCELLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'18256', N'EL PAUJIL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'18410', N'LA MONTANITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'18460', N'MILAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'18479', N'MORELIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'18592', N'PUERTO RICO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'18610', N'SAN JOSE DEL FRAGUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'18753', N'SAN VICENTE DEL CAGUAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'18756', N'SOLANO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'18785', N'SOLITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'18860', N'VALPARAISO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19001', N'POPAYAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19022', N'ALMAGUER')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19050', N'ARGELIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19075', N'BALBOA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19100', N'BOLIVAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19110', N'BUENOS AIRES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19130', N'CAJIBIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19137', N'CALDONO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19142', N'CALOTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19212', N'CORINTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19256', N'EL TAMBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19290', N'FLORENCIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19318', N'GUAPI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19355', N'INZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19364', N'JAMBALO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19392', N'LA SIERRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19397', N'LA VEGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19418', N'LOPEZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19450', N'MERCADERES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19455', N'MIRANDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19473', N'MORALES')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19513', N'PADILLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19517', N'PAEZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19532', N'PATIA (EL BORDO)')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19533', N'PIAMONTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19548', N'PIENDAMO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19573', N'PUERTO TEJADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19585', N'PURACE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19622', N'ROSAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19693', N'SAN SEBASTIAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19698', N'SANTANDER DE QUILICHAO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19701', N'SANTA ROSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19743', N'SILVIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19760', N'SOTARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19780', N'SUAREZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19785', N'SUCRE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19807', N'TIMBIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19809', N'TIMBIQUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19821', N'TORIBIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19824', N'TOTORO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'19845', N'VILLA RICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'20001', N'VALLEDUPAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'20011', N'AGUACHICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'20013', N'AGUSTIN CODAZZI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'20032', N'ASTREA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'20045', N'BECERRIL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'20060', N'BOSCONIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'20175', N'CHIMICHAGUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'20178', N'CHIRIGUANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'20228', N'CURUMANI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'20238', N'EL COPEY')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'20250', N'EL PASO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'20295', N'GAMARRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'20310', N'GONZALEZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'20383', N'LA GLORIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'20400', N'LA JAGUA DE IBIRICO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'20443', N'MANAURE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'20517', N'PAILITAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'20550', N'PELAYA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'20570', N'PUEBLO BELLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'20614', N'RIO DE ORO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'20621', N'LA PAZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'20710', N'SAN ALBERTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'20750', N'SAN DIEGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'20770', N'SAN MARTIN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'20787', N'TAMALAMEQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'23001', N'MONTERIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'23068', N'AYAPEL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'23079', N'BUENAVISTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'23090', N'CANALETE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'23162', N'CERETE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'23168', N'CHIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'23182', N'CHINU')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'23189', N'CIENAGA DE ORO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'23300', N'COTORRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'23350', N'LA APARTADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'23417', N'LORICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'23419', N'LOS CORDOBAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'23464', N'MOMIL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'23466', N'MONTELIBANO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'23500', N'MOÑITOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'23555', N'PLANETA RICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'23570', N'PUEBLO NUEVO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'23574', N'PUERTO ESCONDIDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'23580', N'PUERTO LIBERTADOR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'23586', N'PURISIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'23660', N'SAHAGUN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'23670', N'SAN ANDRES DE SOTAVENTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'23672', N'SAN ANTERO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'23675', N'SAN BERNARDO DEL VIENTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'23678', N'SAN CARLOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'23686', N'SAN PELAYO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'23807', N'TIERRALTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'23815', N'TUCHIN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'23855', N'VALENCIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25001', N'AGUA DE DIOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25019', N'ALBAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25035', N'ANAPOIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25040', N'ANOLAIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25053', N'ARBELAEZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25086', N'BELTRAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25095', N'BITUIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25099', N'BOJACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25120', N'CABRERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25123', N'CACHIPAY')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25126', N'CAJICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25148', N'CAPARRAPI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25151', N'CAQUEZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25154', N'CARMEN DE CARUPA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25168', N'CHAGUANI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25175', N'CHIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25178', N'CHIPAQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25181', N'CHOACHI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25183', N'CHOCONTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25200', N'COGUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25214', N'COTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25224', N'CUCUNUBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25245', N'EL COLEGIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25258', N'EL PEÑON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25260', N'EL ROSAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25269', N'FACATATIVA')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25279', N'FOMEQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25281', N'FOSCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25286', N'FUNZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25288', N'FUQUENE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25290', N'FUSAGASUGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25293', N'GACHALA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25295', N'GACHANCIPA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25297', N'GACHETA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25299', N'GAMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25307', N'GIRARDOT')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25312', N'GRANADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25317', N'GUACHETA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25320', N'GUADUAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25322', N'GUASCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25324', N'GUATAQUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25326', N'GUATAVITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25328', N'GUAYABAL DE SIQUIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25335', N'GUAYABETAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25339', N'GUTIERREZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25368', N'JERUSALEN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25372', N'JUNIN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25377', N'LA CALERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25386', N'LA MESA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25394', N'LA PALMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25398', N'LA PE{A')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25402', N'LA VEGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25407', N'LENGUAZAQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25426', N'MACHETA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25430', N'MADRID')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25436', N'MANTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25438', N'MEDINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25473', N'MOSQUERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25483', N'NARIÑO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25486', N'NEMOCON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25488', N'NILO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25489', N'NIMAIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25491', N'NOCAIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25506', N'VENECIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25513', N'PACHO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25518', N'PAIME')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25524', N'PANDI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25530', N'PARATEBUENO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25535', N'PASCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25572', N'PUERTO SALGAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25580', N'PULI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25592', N'QUEBRADANEGRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25594', N'QUETAME')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25596', N'QUIPILE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25599', N'APULO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25612', N'RICAURTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25645', N'SAN ANTONIO DEL TEQUENDAMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25649', N'SAN BERNARDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25653', N'SAN CAYETANO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25658', N'SAN FRANCISCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25662', N'SAN JUAN DE RIO SECO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25718', N'SASAIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25736', N'SESQUILE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25740', N'SIBATE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25743', N'SILVANIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25745', N'SIMIJACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25754', N'SOACHA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25758', N'SOPO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25769', N'SUBACHOQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25772', N'SUESCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25777', N'SUPATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25779', N'SUSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25781', N'SUTATAUSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25785', N'TABIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25793', N'TAUSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25797', N'TENA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25799', N'TENJO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25805', N'TIBACUY')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25807', N'TIBIRITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25815', N'TOCAIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25817', N'TOCANCIPA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25823', N'TOPAIPI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25839', N'UBALA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25841', N'UBAQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25843', N'UBATE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25845', N'UNE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25851', N'UTICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25862', N'VERGARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25867', N'VIANI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25871', N'VILLAGOMEZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25873', N'VILLAPINZON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25875', N'VILLETA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25878', N'VIOTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25885', N'YACOPI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25898', N'ZIPACON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'25899', N'ZIPAQUIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27001', N'QUIBDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27006', N'ACANDI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27025', N'ALTO BAUDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27050', N'ATRATO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27073', N'BAGADO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27075', N'BAHIA SOLANO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27077', N'BAJO BAUDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27086', N'BELEN DE BAJIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27099', N'BOJAYA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27135', N'EL CANTON DEL SAN PABLO')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27150', N'CARMEN DEL DARIEN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27160', N'CERTEGUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27205', N'CONDOTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27245', N'EL CARMEN DE ATRATO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27250', N'EL LITORAL DEL SAN JUAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27361', N'ITSMINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27372', N'JURADO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27413', N'LLORO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27425', N'MEDIO ATRATO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27430', N'MEDIO BAUDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27450', N'MEDIO SAN JUAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27491', N'NOVITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27495', N'NUQUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27580', N'RIO IRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27600', N'RIO QUITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27615', N'RIOSUCIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27660', N'SAN JOSE DEL PALMAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27745', N'SIPI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27787', N'TADO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27800', N'UNGUIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'27810', N'UNION PANAMERICANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41001', N'NEIVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41006', N'ACEVEDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41013', N'AGRADO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41016', N'AIPE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41020', N'ALGECIRAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41026', N'ALTAMIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41078', N'BARAYA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41132', N'CAMPOALEGRE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41206', N'COLOMBIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41244', N'ELIAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41298', N'GARZON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41306', N'GIGANTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41319', N'GUADALUPE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41349', N'HOBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41357', N'IQUIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41359', N'ISNOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41378', N'LA ARGENTINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41396', N'LA PLATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41483', N'NATAGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41503', N'OPORAPA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41518', N'PAICOL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41524', N'PALERMO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41530', N'PALESTINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41548', N'PITAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41551', N'PITALITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41615', N'RIVERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41660', N'SALADOBLANCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41668', N'SAN AGUSTIN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41676', N'SANTA MARIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41770', N'SUAZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41791', N'TARQUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41797', N'TESALIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41799', N'TELLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41801', N'TERUEL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41807', N'TIMANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41872', N'VILLAVIEJA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'41885', N'YAGUARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'44001', N'RIOHACHA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'44035', N'ALBANIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'44078', N'BARRANCAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'44090', N'DIBULLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'44098', N'DISTRACCION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'44110', N'EL MOLINO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'44279', N'FONSECA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'44378', N'HATONUEVO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'44420', N'LA JAGUA DEL PILAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'44430', N'MAICAO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'44560', N'MANAURE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'44650', N'SAN JUAN DEL CESAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'44847', N'URIBIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'44855', N'URUMITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'44874', N'VILLANUEVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47001', N'SANTA MARTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47030', N'ALGARROBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47053', N'ARACATACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47058', N'ARIGUANI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47161', N'CERRO SAN ANTONIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47170', N'CHIVOLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47189', N'CIENAGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47205', N'CONCORDIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47245', N'EL BANCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47258', N'EL PIÑON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47268', N'EL RETEN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47288', N'FUNDACION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47318', N'GUAMAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47460', N'NUEVA GRANADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47541', N'PEDRAZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47545', N'PIJIÑO DEL CARMEN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47551', N'PIVIJAY')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47555', N'PLATO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47570', N'PUEBLOVIEJO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47605', N'REMOLINO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47660', N'SABANAS DE SAN ANGEL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47675', N'SALAMINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47692', N'SAN SEBASTIAN DE BUENAVISTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47703', N'SAN ZENON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47707', N'SANTA ANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47745', N'SITIONUEVO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47798', N'TENERIFE')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47799', N'SANTA BARBARA DE PINTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47960', N'ZAPAYAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'47980', N'ZONA BANANERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'50001', N'VILLAVICENCIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'50006', N'ACACIAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'50110', N'BARRANCA DE UPIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'50124', N'CABUYARO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'50150', N'CASTILLA LA NUEVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'50223', N'CUBARRAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'50226', N'CUMARAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'50245', N'EL CALVARIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'50251', N'EL CASTILLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'50270', N'EL DORADO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'50287', N'FUENTE DE ORO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'50313', N'GRANADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'50318', N'GUAMAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'50325', N'MAPIRIPAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'50330', N'MESETAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'50350', N'LA MACARENA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'50370', N'URIBE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'50400', N'LEJANIAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'50450', N'PUERTO CONCORDIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'50568', N'PUERTO GAITAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'50573', N'PUERTO LOPEZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'50577', N'PUERTO LLERAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'50590', N'PUERTO RICO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'50606', N'RESTREPO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'50680', N'SAN CARLOS DE GUAROA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'50683', N'SAN JUAN DE ARAMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'50686', N'SAN JUANITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'50689', N'SAN MARTIN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'50711', N'VISTA HERMOSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52001', N'PASTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52019', N'ALBAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52022', N'ALDANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52036', N'ANCUYA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52051', N'ARBOLEDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52079', N'BARBACOAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52083', N'BELEN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52110', N'BUESACO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52203', N'COLON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52207', N'CONSACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52210', N'CONTADERO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52215', N'CORDOBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52224', N'CUASPUD')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52227', N'CUMBAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52233', N'CUMBITARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52240', N'CHACHAGÜI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52250', N'EL CHARCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52254', N'EL PEÑOL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52256', N'EL ROSARIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52258', N'EL TABLON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52260', N'EL TAMBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52287', N'FUNES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52317', N'GUACHUCAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52320', N'GUAITARILLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52323', N'GUALMATAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52352', N'ILES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52354', N'IMUES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52356', N'IPIALES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52378', N'LA CRUZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52381', N'LA FLORIDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52385', N'LA LLANADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52390', N'LA TOLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52399', N'LA UNION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52405', N'LEIVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52411', N'LINARES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52418', N'LOS ANDES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52427', N'MAGÜI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52435', N'MALLAMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52473', N'MOSQUERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52480', N'NARIÑO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52490', N'OLAYA HERRERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52506', N'OSPINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52520', N'FRANCISCO PIZARRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52540', N'POLICARPA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52560', N'POTOSI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52565', N'PROVIDENCIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52573', N'PUERRES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52585', N'PUPIALES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52612', N'RICAURTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52621', N'ROBERTO PAYAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52678', N'SAMANIEGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52683', N'SANDONA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52685', N'SAN BERNARDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52687', N'SAN LORENZO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52693', N'SAN PABLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52694', N'SAN PEDRO DE CARTAGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52696', N'SANTA BARBARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52699', N'SANTACRUZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52720', N'SAPUYES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52786', N'TAMINANGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52788', N'TANGUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52835', N'TUMACO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52838', N'TUQUERRES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'52885', N'YACUANQUER')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54001', N'CUCUTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54003', N'ABREGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54051', N'ARBOLEDAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54099', N'BOCHALEMA')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54109', N'BUCARASICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54125', N'CACOTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54128', N'CACHIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54172', N'CHINACOTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54174', N'CHITAGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54206', N'CONVENCION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54223', N'CUCUTILLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54239', N'DURANIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54245', N'EL CARMEN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54250', N'EL TARRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54261', N'EL ZULIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54313', N'GRAMALOTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54344', N'HACARI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54347', N'HERRAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54377', N'LABATECA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54385', N'LA ESPERANZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54398', N'LA PLAYA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54405', N'LOS PATIOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54418', N'LOURDES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54480', N'MUTISCUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54498', N'OCANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54518', N'PAMPLONA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54520', N'PAMPLONITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54553', N'PUERTO SANTANDER')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54599', N'RAGONVALIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54660', N'SALAZAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54670', N'SAN CALIXTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54673', N'SAN CAYETANO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54680', N'SANTIAGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54720', N'SARDINATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54743', N'SILOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54800', N'TEORAMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54810', N'TIBU')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54820', N'TOLEDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54871', N'VILLA CARO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'54874', N'VILLA ROSARIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'63001', N'ARMENIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'63111', N'BUENAVISTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'63130', N'CALARCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'63190', N'CIRCASIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'63212', N'CORDOBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'63272', N'FILANDIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'63302', N'GENOVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'63401', N'LA TEBAIDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'63470', N'MONTENEGRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'63548', N'PIJAO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'63594', N'QUIMBAYA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'63690', N'SALENTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'66001', N'PEREIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'66045', N'APIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'66075', N'BALBOA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'66088', N'BELEN DE UMBRIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'66170', N'DOS QUEBRADAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'66318', N'GUATICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'66383', N'LA CELIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'66400', N'LA VIRGINIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'66440', N'MARSELLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'66456', N'MISTRATO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'66572', N'PUEBLO RICO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'66594', N'QUINCHIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'66682', N'SANTA ROSA DE CABAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'66687', N'SANTUARIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68001', N'BUCARAMANGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68013', N'AGUADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68020', N'ALBANIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68051', N'ARATOCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68077', N'BARBOSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68079', N'BARICHARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68081', N'BARRANCABERMEJA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68092', N'BETULIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68101', N'BOLIVAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68121', N'CABRERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68132', N'CALIFORNIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68147', N'CAPITANEJO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68152', N'CARCASI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68160', N'CEPITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68162', N'CERRITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68167', N'CHARALA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68169', N'CHARTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68176', N'CHIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68179', N'CHIPATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68190', N'CIMITARRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68207', N'CONCEPCION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68209', N'CONFINES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68211', N'CONTRATACION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68217', N'COROMORO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68229', N'CURITI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68235', N'EL CARMEN DE CHUCURI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68245', N'EL GUACAMAYO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68250', N'EL PEÑON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68255', N'EL PLAYON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68264', N'ENCINO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68266', N'ENCISO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68271', N'FLORIAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68276', N'FLORIDABLANCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68296', N'GALAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68298', N'GAMBITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68307', N'GIRON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68318', N'GUACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68320', N'GUADALUPE')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68322', N'GUAPOTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68324', N'GUAVATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68327', N'GUEPSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68344', N'HATO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68368', N'JESUS MARIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68370', N'JORDAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68377', N'LA BELLEZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68385', N'LANDAZURI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68397', N'LA PAZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68406', N'LEBRIJA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68418', N'LOS SANTOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68425', N'MACARAVITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68432', N'MALAGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68444', N'MATANZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68464', N'MOGOTES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68468', N'MOLAGAVITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68498', N'OCAMONTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68500', N'OIBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68502', N'ONZAGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68522', N'PALMAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68524', N'PALMAS DEL SOCORRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68533', N'PARAMO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68547', N'PIEDECUESTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68549', N'PINCHOTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68572', N'PUENTE NACIONAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68573', N'PUERTO PARRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68575', N'PUERTO WILCHES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68615', N'RIONEGRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68655', N'SABANA DE TORRES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68669', N'SAN ANDRES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68673', N'SAN BENITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68679', N'SAN GIL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68682', N'SAN JOAQUIN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68684', N'SAN JOSE DE MIRANDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68686', N'SAN MIGUEL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68689', N'SAN VICENTE DE CHUCURI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68705', N'SANTA BARBARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68720', N'SANTA HELENA DEL OPON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68745', N'SIMACOTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68755', N'SOCORRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68770', N'SUAITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68773', N'SUCRE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68780', N'SURATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68820', N'TONA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68855', N'VALLE DE SAN JOSE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68861', N'VELEZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68867', N'VETAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68872', N'VILLANUEVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'68895', N'ZAPATOCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'70001', N'SINCELEJO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'70110', N'BUENAVISTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'70124', N'CAIMITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'70204', N'COLOSO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'70215', N'COROZAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'70221', N'COVEÑAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'70230', N'CHALAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'70233', N'EL ROBLE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'70235', N'GALERAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'70265', N'GUARANDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'70400', N'LA UNION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'70418', N'LOS PALMITOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'70429', N'MAJAGUAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'70473', N'MORROA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'70508', N'OVEJAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'70523', N'PALMITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'70670', N'SAMPUES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'70678', N'SAN BENITO ABAD')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'70702', N'SAN JUAN DE BETULIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'70708', N'SAN MARCOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'70713', N'SAN ONOFRE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'70717', N'SAN PEDRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'70742', N'SINCE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'70771', N'SUCRE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'70820', N'TOLU')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'70823', N'TOLUVIEJO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73001', N'IBAGUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73024', N'ALPUJARRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73026', N'ALVARADO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73030', N'AMBALEMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73043', N'ANZOATEGUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73055', N'ARMERO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73067', N'ATACO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73124', N'CAJAMARCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73148', N'CARMEN DE APICALA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73152', N'CASABIANCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73168', N'CHAPARRAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73200', N'COELLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73217', N'COYAIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73226', N'CUNDAY')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73236', N'DOLORES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73268', N'ESPINAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73270', N'FALAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73275', N'FLANDES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73283', N'FRESNO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73319', N'GUAMO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73347', N'HERVEO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73349', N'HONDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73352', N'ICONONZO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73408', N'LERIDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73411', N'LIBANO')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73443', N'MARIQUITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73449', N'MELGAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73461', N'MURILLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73483', N'NATAGAIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73504', N'ORTEGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73520', N'PALOCABILDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73547', N'PIEDRAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73555', N'PLANADAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73563', N'PRADO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73585', N'PURIFICACION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73616', N'RIOBLANCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73622', N'RONCESVALLES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73624', N'ROVIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73671', N'SALDANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73675', N'SAN ANTONIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73678', N'SAN LUIS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73686', N'SANTA ISABEL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73770', N'SUAREZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73854', N'VALLE DE SAN JUAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73861', N'VENADILLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73870', N'VILLAHERMOSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'73873', N'VILLARRICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76001', N'CALI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76020', N'ALCALA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76036', N'ANDALUCIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76041', N'ANSERMANUEVO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76054', N'ARGELIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76100', N'BOLIVAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76109', N'BUENAVENTURA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76111', N'BUGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76113', N'BUGALAGRANDE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76122', N'CAICEDONIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76126', N'CALIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76130', N'CANDELARIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76147', N'CARTAGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76233', N'DAGUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76243', N'EL AGUILA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76246', N'EL CAIRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76248', N'EL CERRITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76250', N'EL DOVIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76275', N'FLORIDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76306', N'GINEBRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76318', N'GUACARI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76364', N'JAMUNDI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76377', N'LA CUMBRE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76400', N'LA UNION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76403', N'LA VICTORIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76497', N'OBANDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76520', N'PALMIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76563', N'PRADERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76606', N'RESTREPO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76616', N'RIOFRIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76622', N'ROLDANILLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76670', N'SAN PEDRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76736', N'SEVILLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76823', N'TORO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76828', N'TRUJILLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76834', N'TULUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76845', N'ULLOA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76863', N'VERSALLES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76869', N'VIJES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76890', N'YOTOCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76892', N'YUMBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'76895', N'ZARZAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'81001', N'ARAUCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'81065', N'ARAUQUITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'81220', N'CRAVO NORTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'81300', N'FORTUL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'81591', N'PUERTO RONDON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'81736', N'SARAVENA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'81794', N'TAME')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'85001', N'YOPAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'85010', N'AGUAZUL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'85015', N'CHAMEZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'85125', N'HATO COROZAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'85136', N'LA SALINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'85139', N'MANI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'85162', N'MONTERREY')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'85225', N'NUNCHIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'85230', N'OROCUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'85250', N'PAZ DE ARIPORO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'85263', N'PORE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'85279', N'RECETOR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'85300', N'SABANALARGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'85315', N'SACAMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'85325', N'S LUIS D PALENQ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'85400', N'TAMARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'85410', N'TAURAMENA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'85430', N'TRINIDAD')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'85440', N'VILLANUEVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'86001', N'MOCOA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'86219', N'COLON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'86320', N'ORITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'86568', N'PUERTO ASIS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'86569', N'PUERTO CAICEDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'86571', N'PUERTO GUZMAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'86573', N'PTO. LEGUIZAMO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'86749', N'SIBUNDOY')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'86755', N'SAN FRANCISCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'86757', N'SAN MIGUEL')

INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'86760', N'SANTIAGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'86865', N'VALLE DEL GUAMUEZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'86885', N'VILLAGARZON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'88001', N'SAN ANDRES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'88564', N'PROVIDENCIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'91001', N'LETICIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'91263', N'EL ENCANTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'91405', N'LA CHORRERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'91407', N'LA PEDRERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'91430', N'LA VICTORIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'91460', N'MIRITI - PARANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'91530', N'PUERTO ALEGRIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'91536', N'PUERTO ARICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'91540', N'PUERTO NARIÑO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'91669', N'PUERTO SANTANDER')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'91798', N'TARAPACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'94001', N'INIRIDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'94343', N'BARRANCO MINAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'94663', N'MAPIRIPANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'94883', N'SAN FELIPE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'94884', N'PUERTO COLOMBIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'94885', N'LA GUADALUPE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'94886', N'CACAHUAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'94887', N'PANA PANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'94888', N'MORICHAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'95001', N'S.JOSE GUAVIARE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'95015', N'CALAMAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'95025', N'EL RETORNO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'95200', N'MIRAFLORES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'97001', N'MITU')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'97161', N'CARURU')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'97511', N'PACOA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'97666', N'TARAIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'97777', N'PAPUNAUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'97889', N'YAVARATE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'99001', N'PUERTO CARRENO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'99524', N'LA PRIMAVERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'99624', N'SANTA ROSALIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'99760', N'SAN JOSE DE OCUNE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'99773', N'CUMARIBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_RES', N'99999', N'EXTRANJERO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'00  ', N'OTROS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'05  ', N'ANTIOQUIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'08  ', N'ATLANTICO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'11  ', N'D.C.')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'13  ', N'BOLIVAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'15  ', N'BOYACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'17  ', N'CALDAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'18  ', N'CAQUETA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'19  ', N'CAUCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'20  ', N'CESAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'23  ', N'CORDOBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'25  ', N'CUNDINAMARCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'27  ', N'CHOCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'41  ', N'HUILA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'44  ', N'LA GUAJIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'47  ', N'MAGDALENA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'50  ', N'META')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'52  ', N'NARIÑO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'54  ', N'NORTE SANTANDER')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'63  ', N'QUINDIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'66  ', N'RISARALDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'68  ', N'SANTANDER')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'70  ', N'SUCRE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'73  ', N'TOLIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'76  ', N'VALLE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'81  ', N'ARAUCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'85  ', N'CASANARE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'86  ', N'PUTUMAYO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'88  ', N'SAN ANDRES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'91  ', N'AMAZONAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'94  ', N'GUAINIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'95  ', N'GUAVIARE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'97  ', N'VAUPES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'DPTO_NAC', N'99  ', N'VICHADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'00001', N'OTROS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05001', N'MEDELLIN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05002', N'ABEJORRAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05004', N'ABRIAQUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05021', N'ALEJANDRIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05030', N'AMAGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05031', N'AMALFI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05034', N'ANDES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05036', N'ANGELOPOLIS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05038', N'ANGOSTURA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05040', N'ANORI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05042', N'ANTIOQUIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05044', N'ANZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05045', N'APARTADO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05051', N'ARBOLETES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05055', N'ARGELIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05059', N'ARMENIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05079', N'BARBOSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05086', N'BELMIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05088', N'BELLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05091', N'BETANIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05093', N'BETULIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05101', N'BOLIVAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05107', N'BRICEÑO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05113', N'BURITICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05120', N'CACERES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05125', N'CAICEDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05129', N'CALDAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05134', N'CAMPAMENTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05138', N'CANASGORDAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05142', N'CARACOLI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05145', N'CARAMANTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05147', N'CAREPA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05148', N'CARMEN DE VIBORAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05150', N'CAROLINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05154', N'CAUCASIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05172', N'CHIGORODO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05190', N'CISNEROS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05197', N'COCORNA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05206', N'CONCEPCION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05209', N'CONCORDIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05212', N'COPACABANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05234', N'DABEIBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05237', N'DON MATIAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05240', N'EBEJICO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05250', N'EL BAGRE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05264', N'ENTRERRIOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05266', N'ENVIGADO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05282', N'FREDONIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05284', N'FRONTINO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05306', N'GIRALDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05308', N'GIRARDOTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05310', N'GOMEZ PLATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05313', N'GRANADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05315', N'GUADALUPE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05318', N'GUARNE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05321', N'GUATAPE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05347', N'HELICONIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05353', N'HISPANIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05360', N'ITAGUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05361', N'ITUANGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05364', N'JARDIN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05368', N'JERICO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05376', N'LA CEJA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05380', N'LA ESTRELLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05390', N'LA PINTADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05400', N'LA UNION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05411', N'LIBORINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05425', N'MACEO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05440', N'MARINILLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05467', N'MONTEBELLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05475', N'MURINDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05480', N'MUTATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05483', N'NARINO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05490', N'NECOCLI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05495', N'NECHI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05501', N'OLAYA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05541', N'PENOL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05543', N'PEQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05576', N'PUEBLORRICO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05579', N'PUERTO BERRIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05585', N'PUERTO NARE (LAMAG)')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05591', N'PUERTO TRIUNFO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05604', N'REMEDIOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05607', N'RETIRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05615', N'RIONEGRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05625', N'SAN FRANCISCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05628', N'SABANALARGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05631', N'SABANETA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05642', N'SALGAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05647', N'SAN ANDRES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05649', N'SAN CARLOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05652', N'SAN FRANCISCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05656', N'SAN JERONIMO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05658', N'SAN JOSE LA MONTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05659', N'SAN JUAN DE URABA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05660', N'SAN LUIS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05662', N'SAN JUAN DE URABA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05664', N'SAN PEDRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05665', N'SAN PEDRO URABA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05667', N'SAN RAFAEL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05670', N'SAN ROQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05674', N'SAN VICENTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05679', N'SANTA BARBARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05686', N'SANTA ROSA DE OSOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05690', N'SANTO DOMINGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05697', N'SANTUARIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05736', N'SEGOVIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05756', N'SONSON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05761', N'SOPETRAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05789', N'TAMESIS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05790', N'TARAZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05792', N'TARSO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05809', N'TITIRIBI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05819', N'TOLEDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05837', N'TURBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05842', N'URAMITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05847', N'URRAO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05854', N'VALDIVIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05856', N'VALPARAISO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05858', N'VEGACHI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05861', N'VENECIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05873', N'VIGIA DEL FUERTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05885', N'YALI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05887', N'YARUMAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05890', N'YOLOMBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05893', N'YONDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'05895', N'ZARAGOZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'08001', N'BARRANQUILLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'08078', N'BARANOA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'08137', N'CAMPO DE LA CRUZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'08141', N'CANDELARIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'08296', N'GALAPA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'08372', N'JUAN DE ACOSTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'08421', N'LURUACO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'08433', N'MALAMBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'08436', N'MANATI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'08520', N'PALMAR DE VARELA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'08549', N'PIOJO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'08558', N'POLONUEVO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'08560', N'PONEDERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'08573', N'PUERTO COLOMBIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'08606', N'REPELON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'08634', N'SABANAGRANDE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'08638', N'SABANALARGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'08675', N'SANTA LUCIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'08685', N'SANTO TOMAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'08758', N'SOLEDAD')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'08770', N'SUAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'08832', N'TUBARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'08849', N'USIACURI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'11001', N'SANTAFE DE BOGOTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'11102', N'BOSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'11265', N'ENGATIVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'11769', N'SUBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'11850', N'USME')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13001', N'CARTAGENA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13006', N'ACHI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13030', N'ALTOS DEL ROSARIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13042', N'ARENAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13052', N'ARJONA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13062', N'ARROYOHONDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13074', N'BARRANCO DE LOBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13140', N'CALAMAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13160', N'CANTAGALLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13188', N'CICUCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13212', N'CORDOBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13222', N'CLEMENCIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13244', N'EL CARMEN DE B0LIVAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13248', N'EL GUAMO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13268', N'EL PEÑON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13300', N'HATILLO DE LOBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13430', N'MAGANGUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13433', N'MAHATES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13440', N'MARGARITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13442', N'MARIA LA BAJA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13458', N'MONTECRISTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13468', N'MOMPOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13473', N'MORALES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13549', N'PINILLOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13580', N'REGIDOR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13600', N'RIO VIEJO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13620', N'SAN CRISTOBAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13647', N'SAN ESTANISLAO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13650', N'SAN FERNANDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13654', N'SAN JACINTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13655', N'SAN JACINTO DEL CAUCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13657', N'SAN JUAN NEPOMUCENO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13667', N'SAN MARTIN DE LOBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13670', N'SAN PABLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13673', N'SANTA CATALINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13683', N'SANTA ROSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13688', N'SANTA ROSA DEL SUR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13744', N'SIMITI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13760', N'SOPLAVIENTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13780', N'TALAIGUA NUEVO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13810', N'TIQUISIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13836', N'TURBACO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13838', N'TURBANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13873', N'VILLANUEVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'13894', N'ZAMBRANO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15001', N'TUNJA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15022', N'ALMEIDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15047', N'AQUITANIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15051', N'ARCABUCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15087', N'BELEN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15090', N'BERBEO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15092', N'BETEITIVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15097', N'BOAVITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15104', N'BOYACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15106', N'BRICEÑO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15109', N'BUENAVISTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15114', N'BUSBANZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15131', N'CALDAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15135', N'CAMPOHERMOSO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15162', N'CERINZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15172', N'CHINAVITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15176', N'CHIQUINQUIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15180', N'CHISCAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15183', N'CHITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15185', N'CHITARAQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15187', N'CHIVATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15189', N'CIENEGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15204', N'COMBITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15212', N'COPER')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15215', N'CORRALES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15218', N'COVARACHIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15223', N'CUBARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15224', N'CUCAITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15226', N'CUITIVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15232', N'CHIQUIZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15236', N'CHIVOR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15238', N'DUITAMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15244', N'EL COCUY')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15248', N'EL ESPINO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15272', N'FIRAVITOBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15276', N'FLORESTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15293', N'GACHANTIVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15296', N'GAMEZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15299', N'GARAGOA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15317', N'GUACAMAYAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15322', N'GUATEQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15325', N'GUAYATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15332', N'GUICAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15362', N'IZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15367', N'JENESANO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15368', N'JERICO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15377', N'LABRANZAGRANDE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15380', N'LA CAPILLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15401', N'LA VICTORIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15403', N'LA UVITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15407', N'VILLA DE LEYVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15425', N'MACANAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15442', N'MARIPI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15455', N'MIRAFLORES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15464', N'MONGUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15466', N'MONGUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15469', N'MONIQUIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15476', N'MOTAVITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15480', N'MUZO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15491', N'NOBSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15494', N'NUEVO COLON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15500', N'OICATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15507', N'OTANCHE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15511', N'PACHAVITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15514', N'PAEZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15516', N'PAIPA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15518', N'PAJARITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15522', N'PANQUEBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15531', N'PAUNA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15533', N'PAYA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15537', N'PAZ DE RIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15542', N'PESCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15550', N'PISBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15572', N'PUERTO BOYACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15580', N'QUIPAMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15599', N'RAMIRIQUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15600', N'RAQUIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15621', N'RONDON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15632', N'SABOYA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15638', N'SACHICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15646', N'SAMACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15660', N'SAN EDUARDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15664', N'SAN JOSE DE PARE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15667', N'SAN LUIS DE GACENO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15673', N'SAN MATEO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15676', N'SAN MIGUEL DE SEMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15681', N'SAN PABLO DE BORBUR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15686', N'SANTANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15690', N'SANTA MARIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15693', N'SANTA ROSA DE VITERBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15696', N'SANTA SOFIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15720', N'SATIVANORTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15723', N'SATIVASUR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15740', N'SIACHOQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15753', N'SOATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15755', N'SOCOTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15757', N'SOCHA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15759', N'SOGAMOSO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15761', N'SOMONDOCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15762', N'SORA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15763', N'SOTAQUIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15764', N'SORACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15774', N'SUSACON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15776', N'SUTAMARCHAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15778', N'SUTATENZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15790', N'TASCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15798', N'TENZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15804', N'TIBANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15806', N'TIBASOSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15808', N'TINJACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15810', N'TIPACOQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15814', N'TOCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15816', N'TOGUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15820', N'TOPAGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15822', N'TOTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15832', N'TUNUNGUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15835', N'TURMEQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15837', N'TUTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15839', N'TUTAZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15842', N'UMBITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15861', N'VENTAQUEMADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15879', N'VIRACACHA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'15897', N'ZETAQUIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'17001', N'MANIZALES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'17013', N'AGUADAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'17042', N'ANSERMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'17050', N'ARANZAZU')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'17088', N'BELALCAZAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'17174', N'CHINCHINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'17272', N'FILADELFIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'17380', N'LA DORADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'17388', N'LA MERCED')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'17433', N'MANZANARES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'17442', N'MARMATO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'17444', N'MARQUETALIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'17446', N'MARULANDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'17486', N'NEIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'17495', N'NORCASIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'17513', N'PACORA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'17524', N'PALESTINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'17541', N'PENSILVANIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'17614', N'RIOSUCIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'17616', N'RISARALDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'17653', N'SALAMINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'17662', N'SAMANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'17665', N'SAN JOSE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'17777', N'SUPIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'17867', N'VICTORIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'17873', N'VILLAMARIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'17877', N'VITERBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'18001', N'FLORENCIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'18029', N'ALBANIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'18094', N'BELEN ANDAQUIES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'18150', N'CARTAGENA DEL CHAIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'18205', N'CURILLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'18247', N'EL DONCELLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'18256', N'EL PAUJIL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'18410', N'LA MONTANITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'18460', N'MILAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'18479', N'MORELIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'18592', N'PUERTO RICO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'18610', N'SAN JOSE DEL FRAGUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'18753', N'SAN VICENTE DEL CAGUAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'18756', N'SOLANO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'18785', N'SOLITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'18860', N'VALPARAISO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19001', N'POPAYAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19022', N'ALMAGUER')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19050', N'ARGELIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19075', N'BALBOA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19100', N'BOLIVAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19110', N'BUENOS AIRES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19130', N'CAJIBIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19137', N'CALDONO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19142', N'CALOTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19212', N'CORINTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19256', N'EL TAMBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19290', N'FLORENCIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19318', N'GUAPI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19355', N'INZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19364', N'JAMBALO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19392', N'LA SIERRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19397', N'LA VEGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19418', N'LOPEZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19450', N'MERCADERES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19455', N'MIRANDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19473', N'MORALES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19513', N'PADILLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19517', N'PAEZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19532', N'PATIA (EL BORDO)')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19533', N'PIAMONTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19548', N'PIENDAMO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19573', N'PUERTO TEJADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19585', N'PURACE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19622', N'ROSAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19693', N'SAN SEBASTIAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19698', N'SANTANDER DE QUILICHAO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19701', N'SANTA ROSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19743', N'SILVIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19760', N'SOTARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19780', N'SUAREZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19785', N'SUCRE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19807', N'TIMBIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19809', N'TIMBIQUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19821', N'TORIBIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19824', N'TOTORO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'19845', N'VILLA RICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'20001', N'VALLEDUPAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'20011', N'AGUACHICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'20013', N'AGUSTIN CODAZZI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'20032', N'ASTREA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'20045', N'BECERRIL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'20060', N'BOSCONIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'20175', N'CHIMICHAGUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'20178', N'CHIRIGUANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'20228', N'CURUMANI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'20238', N'EL COPEY')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'20250', N'EL PASO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'20295', N'GAMARRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'20310', N'GONZALEZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'20383', N'LA GLORIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'20400', N'LA JAGUA DE IBIRICO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'20443', N'MANAURE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'20517', N'PAILITAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'20550', N'PELAYA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'20570', N'PUEBLO BELLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'20614', N'RIO DE ORO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'20621', N'LA PAZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'20710', N'SAN ALBERTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'20750', N'SAN DIEGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'20770', N'SAN MARTIN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'20787', N'TAMALAMEQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'23001', N'MONTERIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'23068', N'AYAPEL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'23079', N'BUENAVISTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'23090', N'CANALETE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'23162', N'CERETE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'23168', N'CHIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'23182', N'CHINU')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'23189', N'CIENAGA DE ORO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'23300', N'COTORRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'23350', N'LA APARTADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'23417', N'LORICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'23419', N'LOS CORDOBAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'23464', N'MOMIL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'23466', N'MONTELIBANO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'23500', N'MOÑITOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'23555', N'PLANETA RICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'23570', N'PUEBLO NUEVO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'23574', N'PUERTO ESCONDIDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'23580', N'PUERTO LIBERTADOR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'23586', N'PURISIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'23660', N'SAHAGUN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'23670', N'SAN ANDRES DE SOTAVENTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'23672', N'SAN ANTERO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'23675', N'SAN BERNARDO DEL VIENTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'23678', N'SAN CARLOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'23686', N'SAN PELAYO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'23807', N'TIERRALTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'23815', N'TUCHIN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'23855', N'VALENCIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25001', N'AGUA DE DIOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25019', N'ALBAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25035', N'ANAPOIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25040', N'ANOLAIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25053', N'ARBELAEZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25086', N'BELTRAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25095', N'BITUIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25099', N'BOJACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25120', N'CABRERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25123', N'CACHIPAY')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25126', N'CAJICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25148', N'CAPARRAPI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25151', N'CAQUEZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25154', N'CARMEN DE CARUPA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25168', N'CHAGUANI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25175', N'CHIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25178', N'CHIPAQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25181', N'CHOACHI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25183', N'CHOCONTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25200', N'COGUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25214', N'COTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25224', N'CUCUNUBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25245', N'EL COLEGIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25258', N'EL PEÑON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25260', N'EL ROSAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25269', N'FACATATIVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25279', N'FOMEQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25281', N'FOSCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25286', N'FUNZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25288', N'FUQUENE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25290', N'FUSAGASUGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25293', N'GACHALA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25295', N'GACHANCIPA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25297', N'GACHETA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25299', N'GAMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25307', N'GIRARDOT')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25312', N'GRANADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25317', N'GUACHETA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25320', N'GUADUAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25322', N'GUASCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25324', N'GUATAQUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25326', N'GUATAVITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25328', N'GUAYABAL DE SIQUIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25335', N'GUAYABETAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25339', N'GUTIERREZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25368', N'JERUSALEN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25372', N'JUNIN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25377', N'LA CALERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25386', N'LA MESA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25394', N'LA PALMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25398', N'LA PE{A')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25402', N'LA VEGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25407', N'LENGUAZAQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25426', N'MACHETA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25430', N'MADRID')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25436', N'MANTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25438', N'MEDINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25473', N'MOSQUERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25483', N'NARIÑO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25486', N'NEMOCON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25488', N'NILO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25489', N'NIMAIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25491', N'NOCAIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25506', N'VENECIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25513', N'PACHO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25518', N'PAIME')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25524', N'PANDI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25530', N'PARATEBUENO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25535', N'PASCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25572', N'PUERTO SALGAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25580', N'PULI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25592', N'QUEBRADANEGRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25594', N'QUETAME')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25596', N'QUIPILE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25599', N'APULO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25612', N'RICAURTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25645', N'SAN ANTONIO DEL TEQUENDAMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25649', N'SAN BERNARDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25653', N'SAN CAYETANO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25658', N'SAN FRANCISCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25662', N'SAN JUAN DE RIO SECO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25718', N'SASAIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25736', N'SESQUILE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25740', N'SIBATE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25743', N'SILVANIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25745', N'SIMIJACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25754', N'SOACHA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25758', N'SOPO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25769', N'SUBACHOQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25772', N'SUESCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25777', N'SUPATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25779', N'SUSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25781', N'SUTATAUSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25785', N'TABIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25793', N'TAUSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25797', N'TENA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25799', N'TENJO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25805', N'TIBACUY')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25807', N'TIBIRITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25815', N'TOCAIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25817', N'TOCANCIPA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25823', N'TOPAIPI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25839', N'UBALA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25841', N'UBAQUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25843', N'UBATE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25845', N'UNE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25851', N'UTICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25862', N'VERGARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25867', N'VIANI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25871', N'VILLAGOMEZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25873', N'VILLAPINZON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25875', N'VILLETA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25878', N'VIOTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25885', N'YACOPI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25898', N'ZIPACON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'25899', N'ZIPAQUIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27001', N'QUIBDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27006', N'ACANDI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27025', N'ALTO BAUDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27050', N'ATRATO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27073', N'BAGADO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27075', N'BAHIA SOLANO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27077', N'BAJO BAUDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27086', N'BELEN DE BAJIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27099', N'BOJAYA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27135', N'EL CANTON DEL SAN PABLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27150', N'CARMEN DEL DARIEN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27160', N'CERTEGUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27205', N'CONDOTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27245', N'EL CARMEN DE ATRATO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27250', N'EL LITORAL DEL SAN JUAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27361', N'ITSMINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27372', N'JURADO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27413', N'LLORO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27425', N'MEDIO ATRATO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27430', N'MEDIO BAUDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27450', N'MEDIO SAN JUAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27491', N'NOVITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27495', N'NUQUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27580', N'RIO IRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27600', N'RIO QUITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27615', N'RIOSUCIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27660', N'SAN JOSE DEL PALMAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27745', N'SIPI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27787', N'TADO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27800', N'UNGUIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'27810', N'UNION PANAMERICANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41001', N'NEIVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41006', N'ACEVEDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41013', N'AGRADO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41016', N'AIPE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41020', N'ALGECIRAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41026', N'ALTAMIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41078', N'BARAYA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41132', N'CAMPOALEGRE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41206', N'COLOMBIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41244', N'ELIAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41298', N'GARZON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41306', N'GIGANTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41319', N'GUADALUPE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41349', N'HOBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41357', N'IQUIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41359', N'ISNOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41378', N'LA ARGENTINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41396', N'LA PLATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41483', N'NATAGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41503', N'OPORAPA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41518', N'PAICOL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41524', N'PALERMO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41530', N'PALESTINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41548', N'PITAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41551', N'PITALITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41615', N'RIVERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41660', N'SALADOBLANCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41668', N'SAN AGUSTIN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41676', N'SANTA MARIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41770', N'SUAZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41791', N'TARQUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41797', N'TESALIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41799', N'TELLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41801', N'TERUEL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41807', N'TIMANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41872', N'VILLAVIEJA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'41885', N'YAGUARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'44001', N'RIOHACHA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'44035', N'ALBANIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'44078', N'BARRANCAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'44090', N'DIBULLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'44098', N'DISTRACCION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'44110', N'EL MOLINO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'44279', N'FONSECA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'44378', N'HATONUEVO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'44420', N'LA JAGUA DEL PILAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'44430', N'MAICAO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'44560', N'MANAURE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'44650', N'SAN JUAN DEL CESAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'44847', N'URIBIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'44855', N'URUMITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'44874', N'VILLANUEVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47001', N'SANTA MARTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47030', N'ALGARROBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47053', N'ARACATACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47058', N'ARIGUANI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47161', N'CERRO SAN ANTONIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47170', N'CHIVOLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47189', N'CIENAGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47205', N'CONCORDIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47245', N'EL BANCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47258', N'EL PIÑON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47268', N'EL RETEN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47288', N'FUNDACION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47318', N'GUAMAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47460', N'NUEVA GRANADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47541', N'PEDRAZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47545', N'PIJIÑO DEL CARMEN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47551', N'PIVIJAY')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47555', N'PLATO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47570', N'PUEBLOVIEJO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47605', N'REMOLINO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47660', N'SABANAS DE SAN ANGEL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47675', N'SALAMINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47692', N'SAN SEBASTIAN DE BUENAVISTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47703', N'SAN ZENON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47707', N'SANTA ANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47745', N'SITIONUEVO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47798', N'TENERIFE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47799', N'SANTA BARBARA DE PINTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47960', N'ZAPAYAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'47980', N'ZONA BANANERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'50001', N'VILLAVICENCIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'50006', N'ACACIAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'50110', N'BARRANCA DE UPIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'50124', N'CABUYARO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'50150', N'CASTILLA LA NUEVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'50223', N'CUBARRAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'50226', N'CUMARAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'50245', N'EL CALVARIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'50251', N'EL CASTILLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'50270', N'EL DORADO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'50287', N'FUENTE DE ORO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'50313', N'GRANADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'50318', N'GUAMAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'50325', N'MAPIRIPAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'50330', N'MESETAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'50350', N'LA MACARENA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'50370', N'URIBE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'50400', N'LEJANIAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'50450', N'PUERTO CONCORDIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'50568', N'PUERTO GAITAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'50573', N'PUERTO LOPEZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'50577', N'PUERTO LLERAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'50590', N'PUERTO RICO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'50606', N'RESTREPO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'50680', N'SAN CARLOS DE GUAROA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'50683', N'SAN JUAN DE ARAMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'50686', N'SAN JUANITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'50689', N'SAN MARTIN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'50711', N'VISTA HERMOSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52001', N'PASTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52019', N'ALBAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52022', N'ALDANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52036', N'ANCUYA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52051', N'ARBOLEDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52079', N'BARBACOAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52083', N'BELEN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52110', N'BUESACO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52203', N'COLON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52207', N'CONSACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52210', N'CONTADERO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52215', N'CORDOBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52224', N'CUASPUD')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52227', N'CUMBAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52233', N'CUMBITARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52240', N'CHACHAGÜI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52250', N'EL CHARCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52254', N'EL PEÑOL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52256', N'EL ROSARIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52258', N'EL TABLON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52260', N'EL TAMBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52287', N'FUNES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52317', N'GUACHUCAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52320', N'GUAITARILLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52323', N'GUALMATAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52352', N'ILES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52354', N'IMUES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52356', N'IPIALES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52378', N'LA CRUZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52381', N'LA FLORIDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52385', N'LA LLANADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52390', N'LA TOLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52399', N'LA UNION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52405', N'LEIVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52411', N'LINARES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52418', N'LOS ANDES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52427', N'MAGÜI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52435', N'MALLAMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52473', N'MOSQUERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52480', N'NARIÑO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52490', N'OLAYA HERRERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52506', N'OSPINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52520', N'FRANCISCO PIZARRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52540', N'POLICARPA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52560', N'POTOSI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52565', N'PROVIDENCIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52573', N'PUERRES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52585', N'PUPIALES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52612', N'RICAURTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52621', N'ROBERTO PAYAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52678', N'SAMANIEGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52683', N'SANDONA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52685', N'SAN BERNARDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52687', N'SAN LORENZO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52693', N'SAN PABLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52694', N'SAN PEDRO DE CARTAGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52696', N'SANTA BARBARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52699', N'SANTACRUZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52720', N'SAPUYES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52786', N'TAMINANGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52788', N'TANGUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52835', N'TUMACO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52838', N'TUQUERRES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'52885', N'YACUANQUER')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54001', N'CUCUTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54003', N'ABREGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54051', N'ARBOLEDAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54099', N'BOCHALEMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54109', N'BUCARASICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54125', N'CACOTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54128', N'CACHIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54172', N'CHINACOTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54174', N'CHITAGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54206', N'CONVENCION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54223', N'CUCUTILLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54239', N'DURANIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54245', N'EL CARMEN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54250', N'EL TARRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54261', N'EL ZULIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54313', N'GRAMALOTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54344', N'HACARI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54347', N'HERRAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54377', N'LABATECA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54385', N'LA ESPERANZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54398', N'LA PLAYA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54405', N'LOS PATIOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54418', N'LOURDES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54480', N'MUTISCUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54498', N'OCANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54518', N'PAMPLONA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54520', N'PAMPLONITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54553', N'PUERTO SANTANDER')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54599', N'RAGONVALIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54660', N'SALAZAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54670', N'SAN CALIXTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54673', N'SAN CAYETANO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54680', N'SANTIAGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54720', N'SARDINATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54743', N'SILOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54800', N'TEORAMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54810', N'TIBU')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54820', N'TOLEDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54871', N'VILLA CARO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'54874', N'VILLA ROSARIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'63001', N'ARMENIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'63111', N'BUENAVISTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'63130', N'CALARCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'63190', N'CIRCASIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'63212', N'CORDOBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'63272', N'FILANDIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'63302', N'GENOVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'63401', N'LA TEBAIDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'63470', N'MONTENEGRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'63548', N'PIJAO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'63594', N'QUIMBAYA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'63690', N'SALENTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'66001', N'PEREIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'66045', N'APIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'66075', N'BALBOA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'66088', N'BELEN DE UMBRIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'66170', N'DOS QUEBRADAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'66318', N'GUATICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'66383', N'LA CELIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'66400', N'LA VIRGINIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'66440', N'MARSELLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'66456', N'MISTRATO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'66572', N'PUEBLO RICO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'66594', N'QUINCHIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'66682', N'SANTA ROSA DE CABAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'66687', N'SANTUARIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68001', N'BUCARAMANGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68013', N'AGUADA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68020', N'ALBANIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68051', N'ARATOCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68077', N'BARBOSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68079', N'BARICHARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68081', N'BARRANCABERMEJA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68092', N'BETULIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68101', N'BOLIVAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68121', N'CABRERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68132', N'CALIFORNIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68147', N'CAPITANEJO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68152', N'CARCASI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68160', N'CEPITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68162', N'CERRITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68167', N'CHARALA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68169', N'CHARTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68176', N'CHIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68179', N'CHIPATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68190', N'CIMITARRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68207', N'CONCEPCION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68209', N'CONFINES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68211', N'CONTRATACION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68217', N'COROMORO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68229', N'CURITI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68235', N'EL CARMEN DE CHUCURI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68245', N'EL GUACAMAYO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68250', N'EL PEÑON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68255', N'EL PLAYON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68264', N'ENCINO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68266', N'ENCISO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68271', N'FLORIAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68276', N'FLORIDABLANCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68296', N'GALAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68298', N'GAMBITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68307', N'GIRON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68318', N'GUACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68320', N'GUADALUPE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68322', N'GUAPOTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68324', N'GUAVATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68327', N'GUEPSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68344', N'HATO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68368', N'JESUS MARIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68370', N'JORDAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68377', N'LA BELLEZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68385', N'LANDAZURI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68397', N'LA PAZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68406', N'LEBRIJA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68418', N'LOS SANTOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68425', N'MACARAVITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68432', N'MALAGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68444', N'MATANZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68464', N'MOGOTES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68468', N'MOLAGAVITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68498', N'OCAMONTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68500', N'OIBA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68502', N'ONZAGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68522', N'PALMAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68524', N'PALMAS DEL SOCORRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68533', N'PARAMO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68547', N'PIEDECUESTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68549', N'PINCHOTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68572', N'PUENTE NACIONAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68573', N'PUERTO PARRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68575', N'PUERTO WILCHES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68615', N'RIONEGRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68655', N'SABANA DE TORRES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68669', N'SAN ANDRES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68673', N'SAN BENITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68679', N'SAN GIL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68682', N'SAN JOAQUIN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68684', N'SAN JOSE DE MIRANDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68686', N'SAN MIGUEL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68689', N'SAN VICENTE DE CHUCURI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68705', N'SANTA BARBARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68720', N'SANTA HELENA DEL OPON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68745', N'SIMACOTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68755', N'SOCORRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68770', N'SUAITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68773', N'SUCRE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68780', N'SURATA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68820', N'TONA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68855', N'VALLE DE SAN JOSE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68861', N'VELEZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68867', N'VETAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68872', N'VILLANUEVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'68895', N'ZAPATOCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'70001', N'SINCELEJO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'70110', N'BUENAVISTA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'70124', N'CAIMITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'70204', N'COLOSO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'70215', N'COROZAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'70221', N'COVEÑAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'70230', N'CHALAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'70233', N'EL ROBLE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'70235', N'GALERAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'70265', N'GUARANDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'70400', N'LA UNION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'70418', N'LOS PALMITOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'70429', N'MAJAGUAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'70473', N'MORROA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'70508', N'OVEJAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'70523', N'PALMITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'70670', N'SAMPUES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'70678', N'SAN BENITO ABAD')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'70702', N'SAN JUAN DE BETULIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'70708', N'SAN MARCOS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'70713', N'SAN ONOFRE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'70717', N'SAN PEDRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'70742', N'SINCE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'70771', N'SUCRE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'70820', N'TOLU')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'70823', N'TOLUVIEJO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73001', N'IBAGUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73024', N'ALPUJARRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73026', N'ALVARADO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73030', N'AMBALEMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73043', N'ANZOATEGUI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73055', N'ARMERO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73067', N'ATACO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73124', N'CAJAMARCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73148', N'CARMEN DE APICALA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73152', N'CASABIANCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73168', N'CHAPARRAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73200', N'COELLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73217', N'COYAIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73226', N'CUNDAY')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73236', N'DOLORES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73268', N'ESPINAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73270', N'FALAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73275', N'FLANDES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73283', N'FRESNO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73319', N'GUAMO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73347', N'HERVEO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73349', N'HONDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73352', N'ICONONZO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73408', N'LERIDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73411', N'LIBANO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73443', N'MARIQUITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73449', N'MELGAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73461', N'MURILLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73483', N'NATAGAIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73504', N'ORTEGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73520', N'PALOCABILDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73547', N'PIEDRAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73555', N'PLANADAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73563', N'PRADO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73585', N'PURIFICACION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73616', N'RIOBLANCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73622', N'RONCESVALLES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73624', N'ROVIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73671', N'SALDANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73675', N'SAN ANTONIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73678', N'SAN LUIS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73686', N'SANTA ISABEL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73770', N'SUAREZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73854', N'VALLE DE SAN JUAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73861', N'VENADILLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73870', N'VILLAHERMOSA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'73873', N'VILLARRICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76001', N'CALI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76020', N'ALCALA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76036', N'ANDALUCIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76041', N'ANSERMANUEVO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76054', N'ARGELIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76100', N'BOLIVAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76109', N'BUENAVENTURA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76111', N'BUGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76113', N'BUGALAGRANDE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76122', N'CAICEDONIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76126', N'CALIMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76130', N'CANDELARIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76147', N'CARTAGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76233', N'DAGUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76243', N'EL AGUILA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76246', N'EL CAIRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76248', N'EL CERRITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76250', N'EL DOVIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76275', N'FLORIDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76306', N'GINEBRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76318', N'GUACARI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76364', N'JAMUNDI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76377', N'LA CUMBRE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76400', N'LA UNION')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76403', N'LA VICTORIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76497', N'OBANDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76520', N'PALMIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76563', N'PRADERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76606', N'RESTREPO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76616', N'RIOFRIO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76622', N'ROLDANILLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76670', N'SAN PEDRO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76736', N'SEVILLA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76823', N'TORO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76828', N'TRUJILLO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76834', N'TULUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76845', N'ULLOA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76863', N'VERSALLES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76869', N'VIJES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76890', N'YOTOCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76892', N'YUMBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'76895', N'ZARZAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'81001', N'ARAUCA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'81065', N'ARAUQUITA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'81220', N'CRAVO NORTE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'81300', N'FORTUL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'81591', N'PUERTO RONDON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'81736', N'SARAVENA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'81794', N'TAME')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'85001', N'YOPAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'85010', N'AGUAZUL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'85015', N'CHAMEZA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'85125', N'HATO COROZAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'85136', N'LA SALINA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'85139', N'MANI')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'85162', N'MONTERREY')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'85225', N'NUNCHIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'85230', N'OROCUE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'85250', N'PAZ DE ARIPORO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'85263', N'PORE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'85279', N'RECETOR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'85300', N'SABANALARGA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'85315', N'SACAMA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'85325', N'S LUIS D PALENQ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'85400', N'TAMARA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'85410', N'TAURAMENA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'85430', N'TRINIDAD')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'85440', N'VILLANUEVA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'86001', N'MOCOA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'86219', N'COLON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'86320', N'ORITO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'86568', N'PUERTO ASIS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'86569', N'PUERTO CAICEDO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'86571', N'PUERTO GUZMAN')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'86573', N'PTO. LEGUIZAMO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'86749', N'SIBUNDOY')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'86755', N'SAN FRANCISCO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'86757', N'SAN MIGUEL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'86760', N'SANTIAGO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'86865', N'VALLE DEL GUAMUEZ')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'86885', N'VILLAGARZON')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'88001', N'SAN ANDRES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'88564', N'PROVIDENCIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'91001', N'LETICIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'91263', N'EL ENCANTO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'91405', N'LA CHORRERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'91407', N'LA PEDRERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'91430', N'LA VICTORIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'91460', N'MIRITI - PARANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'91530', N'PUERTO ALEGRIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'91536', N'PUERTO ARICA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'91540', N'PUERTO NARIÑO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'91669', N'PUERTO SANTANDER')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'91798', N'TARAPACA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'94001', N'INIRIDA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'94343', N'BARRANCO MINAS')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'94663', N'MAPIRIPANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'94883', N'SAN FELIPE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'94884', N'PUERTO COLOMBIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'94885', N'LA GUADALUPE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'94886', N'CACAHUAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'94887', N'PANA PANA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'94888', N'MORICHAL')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'95001', N'S.JOSE GUAVIARE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'95015', N'CALAMAR')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'95025', N'EL RETORNO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'95200', N'MIRAFLORES')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'97001', N'MITU')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'97161', N'CARURU')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'97511', N'PACOA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'97666', N'TARAIRA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'97777', N'PAPUNAUA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'97889', N'YAVARATE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'99001', N'PUERTO CARRENO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'99524', N'LA PRIMAVERA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'99624', N'SANTA ROSALIA')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'99760', N'SAN JOSE DE OCUNE')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'99773', N'CUMARIBO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'MUN_NAC', N'99999', N'EXTRANJERO')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'32', N'269', N'1')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'32', N'270', N'2')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'32', N'271', N'3')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'32', N'272', N'4')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'32', N'273', N'5')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'32', N'274', N'6')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'32', N'275', N'7')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'32', N'276', N'8')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'32', N'277', N'9')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'32', N'278', N'10')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'32', N'279', N'11')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'32', N'280', N'12')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'32', N'281', N'13')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'32', N'282', N'14')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'32', N'283', N'15')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'32', N'284', N'16')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'32', N'285', N'17')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'32', N'286', N'18')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'32', N'287', N'19')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'32', N'288', N'20')
INSERT #zzrespuestascarac ([CodigoInterno], [respuesta], [Detalle]) VALUES (N'32', N'289', N'0')


select * from #zzrespuestascarac where CodigoInterno =@CodigoInterno

END
























GO
/****** Object:  StoredProcedure [dbo].[Lista_respuestasPreguntas]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec Lista_respuestasPreguntas 'MODALIDAD'
CREATE procedure [dbo].[Lista_respuestasPreguntas]  @PreguntaRef varchar(50) as

Declare @respuestas table (CodigoPregunta varchar(50),  CodigoRespuesta varchar(200), DetalleRespuesta varchar(MAX), valorRiesgo int default 0, Configurado bit default 0)
Insert into @respuestas (codigopregunta, codigorespuesta, detallerespuesta) 
exec Formato_TraerCaracterizacionRespuestas 'CARACTERIZACION', @PreguntaRef

Delete from @respuestas where exists(select 1 from Sto_TipoAlerta_Preguntas p join Sto_Pregunta_ValoracionRiesgoRespuesta r on p.id_tipoAlerta_pregunta=r.id_tipoAlerta_pregunta and p.codigoPregunta=@PreguntaRef)

insert into @respuestas (codigopregunta, codigorespuesta, detallerespuesta, valorRiesgo, Configurado)
select p.codigopregunta, r.respuesta codigorespuesta, r.DescripcionRespuesta detallerespuesta, ValorRiesgo, 1
from Sto_TipoAlerta_Preguntas p join Sto_Pregunta_ValoracionRiesgoRespuesta r
on p.id_tipoAlerta_pregunta=r.id_tipoAlerta_pregunta and p.codigoPregunta=@PreguntaRef
 

select * from @respuestas order by Configurado desc, detallerespuesta



GO
/****** Object:  StoredProcedure [dbo].[Listar_Preguntas]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--	exec Listar_Preguntas 4
CREATE procedure [dbo].[Listar_Preguntas]  @TipoAlerta int as


Declare @preguntas table (CodigoPregunta1 varchar(50),  detallePregunta varchar(max), tipo varchar(50), Multivaluado bit, valorRiesgo int default 0, Registrado bit default 0)

Insert into @preguntas (codigopregunta1, detallePregunta, tipo, Multivaluado) 
exec Formato_Caracterizacion_Preguntas 'CARACTERIZACION'



Delete from @preguntas where exists(select 1 from Sto_TipoAlerta_Preguntas p where CodigoPregunta1 = p.codigoPregunta and p.TipoAlerta =@TipoAlerta)

insert into @preguntas (codigopregunta1, detallePregunta, tipo, Multivaluado, Registrado)
select p.codigopregunta, p.DescripcionPregunta, null tipo, null multivaluado , 1
from Sto_TipoAlerta_Preguntas p
where p.TipoAlerta =@TipoAlerta
 
select CodigoPregunta1 CodigoPregunta, detallePregunta, valorRiesgo, Registrado
 from @preguntas order by Registrado desc, detallePregunta



GO
/****** Object:  StoredProcedure [dbo].[PreguntasTem]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[PreguntasTem]
@TipoAlerta int null,
@Codigo varchar(50) null,
@detalle varchar(max) null,
@tipo varchar(50) null,
@Multi bit null,
@op int
as

	if @op = 0
	begin
		Declare @preguntas table (CodigoPregunta1 varchar(50),  detallePregunta varchar(max), tipo varchar(50), Multivaluado bit, valorRiesgo int default 0, Registrado bit default 0)
		--- Registra las preguntas que me obtiene le api
		insert into @preguntas (Codigopregunta1,detallepregunta,tipo,multivaluado)
		values (@Codigo,@detalle,@tipo,@Multi)

		--- Elimina las preguntas que ya se encuentran registradas
		Delete from @preguntas where exists(select * from Sto_TipoAlerta_Preguntas p where CodigoPregunta1 = p.codigoPregunta and p.TipoAlerta =@TipoAlerta)

		insert into @preguntas (codigopregunta1, detallePregunta, tipo, Multivaluado, Registrado)
		select p.codigoPregunta, p.descripcionPregunta,null tipo, null multivaluado , 1
		from Sto_TipoAlerta_Preguntas p
		where p.TipoAlerta =@TipoAlerta
	end
	if @op = 1
	begin
		select CodigoPregunta1 CodigoPregunta, detallePregunta, Registrado
		from @preguntas order by Registrado desc, detallePregunta
	end
GO
/****** Object:  StoredProcedure [dbo].[RespuestasTem]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[RespuestasTem]
@Codigo varchar(50) null,
@Respuesta varchar(200) null,
@Detalle varchar(MAX) null,
@valor int null,
@Configurado bit null
as
Declare @respuestas table (CodigoPregunta varchar(50),  CodigoRespuesta varchar(200), DetalleRespuesta varchar(MAX), valorRiesgo int default 0, Configurado bit default 0)
Insert into @respuestas (codigopregunta, codigorespuesta, detallerespuesta)
values(@Codigo, @Respuesta, @Detalle)

Delete from @respuestas where exists(select 1 from Sto_TipoAlerta_Preguntas p join Sto_Pregunta_ValoracionRiesgoRespuesta r on p.id_tipoAlerta_pregunta=r.id_tipoAlerta_pregunta and p.codigoPregunta=@Codigo)

insert into @respuestas (codigopregunta, codigorespuesta, detallerespuesta, valorRiesgo, Configurado)
select p.codigopregunta, r.respuesta codigorespuesta, r.DescripcionRespuesta detallerespuesta, ValorRiesgo, 1
from Sto_TipoAlerta_Preguntas p join Sto_Pregunta_ValoracionRiesgoRespuesta r
on p.id_tipoAlerta_pregunta=r.id_tipoAlerta_pregunta and p.codigoPregunta=@Codigo

select * from @respuestas order by Configurado desc, detallerespuesta


GO
/****** Object:  StoredProcedure [dbo].[Seg_maestro_Usuarios]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE procedure [dbo].[Seg_maestro_Usuarios]
@id int output,
@usu varchar(50),
@nom varchar(50),
@act bit,
@op int
as
	if @op = 1
	begin
		insert into Seg_Usuarios(Usuario,Nombre,Activo) values(@usu,@nom,@act)
		set @id = 1
	end
	if @op = 2
	begin
		update Seg_Usuarios set Nombre = @nom, Activo = @act
		where Usuario = @usu
	end
	if @op = 3
	begin
		select * from Seg_Usuarios
	end
	if @op = 4
	begin
		select * from Seg_Usuarios where Usuario = @usu
	end
	if @op = 0
	begin
		delete from Seg_Usuarios where Usuario = @usu
	end

GO
/****** Object:  StoredProcedure [dbo].[Sto_actualizar_Accion_TipoAlerta_NivelRiesgo]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Sto_actualizar_Accion_TipoAlerta_NivelRiesgo]
@id_accion int,
@id_TipoAlerta_NivelRiesgo bigint,
@id_Accion_TipoAlerta_NivelRiesgo int,
@refe varchar(2000),
@id int out
as
update Sto_Accion_TipoAlerta_NivelRiesgo set id_Accion = @id_accion, id_TipoAlerta_NivelRiesgo = @id_TipoAlerta_NivelRiesgo, Referencia = @refe
where id_Accion_TipoAlerta_NivelRiesgo = @id_Accion_TipoAlerta_NivelRiesgo

GO
/****** Object:  StoredProcedure [dbo].[Sto_actualizar_NivelesRiesgoAlertas]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Sto_actualizar_NivelesRiesgoAlertas]
@id tinyint,
@nombre varchar(125)
as
update Sto_NivelesRiesgoAlerta set Nombre = @nombre where NivelRiesgoAlerta = @id

GO
/****** Object:  StoredProcedure [dbo].[Sto_actualizar_TipoAccion]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Sto_actualizar_TipoAccion]
@id int,
@accion varchar(256)
as
update Sto_TipoAcciones set Nombre = @accion where Sto_TipoAcciones = @id

GO
/****** Object:  StoredProcedure [dbo].[Sto_actualizar_TipoAlerta_NivelRiesgo]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Sto_actualizar_TipoAlerta_NivelRiesgo]
@tipoAlerta int,
@minPuntaje numeric(10,1),
@maxPuntaje numeric(10,1),
@nivlRiesgoAlerta tinyint,
@id bigint
as
update Sto_TipoAlerta_NivelRiego set TipoAlerta = @tipoAlerta, Min_Puntaje = @minPuntaje,
Max_puntaje = @maxPuntaje, NivelRiesgoAlerta = @nivlRiesgoAlerta
where Id_tipoAlerta_NivelRiesgo = @id

GO
/****** Object:  StoredProcedure [dbo].[Sto_actualizar_TipoAsesor]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Sto_actualizar_TipoAsesor]
@id int,
@nombre varchar(50),
@email varchar(250)
as
update Sto_TiposAsesores set Nombre = @nombre, email = @email
where Sto_TipoAsesor = @id

GO
/****** Object:  StoredProcedure [dbo].[Sto_crear_Accion_TipoAlerta_NivelRiesgo]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Sto_crear_Accion_TipoAlerta_NivelRiesgo]
@id_accion int,
@id_TipoAlerta_NivelRiesgo bigint,
@refe varchar(2000),
@id int
as
insert into Sto_Accion_TipoAlerta_NivelRiesgo (id_Accion,id_TipoAlerta_NivelRiesgo,Referencia)
values (@id_accion,@id_TipoAlerta_NivelRiesgo,@refe)

GO
/****** Object:  StoredProcedure [dbo].[Sto_crear_NivelesRiesgoAlertas]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Sto_crear_NivelesRiesgoAlertas]
@nombre varchar(125)
as
insert into Sto_NivelesRiesgoAlerta(Nombre) values(@nombre)

GO
/****** Object:  StoredProcedure [dbo].[Sto_crear_TipoAccion]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Sto_crear_TipoAccion]
@accion varchar(256),
@id int out
as
insert into Sto_TipoAcciones(Nombre) values(@accion)
set @id = SCOPE_IDENTITY()

GO
/****** Object:  StoredProcedure [dbo].[Sto_crear_TipoAlerta_NivelRiesgo]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Sto_crear_TipoAlerta_NivelRiesgo]
@tipoAlerta int,
@minPuntaje numeric(10,1),
@maxPuntaje numeric(10,1),
@nivlRiesgoAlerta tinyint,
@id int output
as
insert into Sto_TipoAlerta_NivelRiego(TipoAlerta,Min_Puntaje,Max_puntaje,NivelRiesgoAlerta)
values (@tipoAlerta,@minPuntaje,@maxPuntaje,@nivlRiesgoAlerta)
set @id = @@IDENTITY
GO
/****** Object:  StoredProcedure [dbo].[Sto_crear_TipoAsesor]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Sto_crear_TipoAsesor]
@nombre varchar(50),
@email varchar(256)
as
insert into Sto_TiposAsesores(Nombre,email) values (@nombre,@email)

GO
/****** Object:  StoredProcedure [dbo].[Sto_eliminar_Accion_TipoAlerta_NivelRiesgo]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Sto_eliminar_Accion_TipoAlerta_NivelRiesgo]
@id_Accion_TipoAlerta int
as
delete from Sto_Accion_TipoAlerta_NivelRiesgo where id_Accion_TipoAlerta_NivelRiesgo = @id_Accion_TipoAlerta

GO
/****** Object:  StoredProcedure [dbo].[Sto_eliminar_NivelesRiesgoAlertas]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Sto_eliminar_NivelesRiesgoAlertas]
@id tinyint
as
delete from Sto_NivelesRiesgoAlerta where NivelRiesgoAlerta = @id

GO
/****** Object:  StoredProcedure [dbo].[Sto_eliminar_TipoAccion]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Sto_eliminar_TipoAccion]
@id int
as
delete from Sto_TipoAcciones where Sto_TipoAcciones = @id

GO
/****** Object:  StoredProcedure [dbo].[Sto_eliminar_TipoAlerta_NivelRiesgo]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Sto_eliminar_TipoAlerta_NivelRiesgo]
@id bigint
as
delete from Sto_TipoAlerta_NivelRiego where Id_tipoAlerta_NivelRiesgo = @id

GO
/****** Object:  StoredProcedure [dbo].[Sto_eliminar_TipoAsesor]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Sto_eliminar_TipoAsesor]
@id int
as
delete from Sto_TiposAsesores where Sto_TipoAsesor = @id

GO
/****** Object:  StoredProcedure [dbo].[Sto_listar__Accion_TipoAlerta_NivelRiesgo_byID]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Sto_listar__Accion_TipoAlerta_NivelRiesgo_byID]
@id int
as
select * from Sto_Accion_TipoAlerta_NivelRiesgo where id_Accion_TipoAlerta_NivelRiesgo = @id

GO
/****** Object:  StoredProcedure [dbo].[Sto_listar__TipoAlerta_NivelRiesgo_byID]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Sto_listar__TipoAlerta_NivelRiesgo_byID]
@id bigint
as
select * from Sto_TipoAlerta_NivelRiego where Id_tipoAlerta_NivelRiesgo = @id

GO
/****** Object:  StoredProcedure [dbo].[Sto_listar_Accion_TipoAlerta_NivelRiesgo]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Sto_listar_Accion_TipoAlerta_NivelRiesgo]
as
select * from Sto_Accion_TipoAlerta_NivelRiesgo

GO
/****** Object:  StoredProcedure [dbo].[Sto_listar_Accion_TipoAlerta_NivelRiesgo_byID_D]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Sto_listar_Accion_TipoAlerta_NivelRiesgo_byID_D]
@id_d bigint
as
select * from Sto_Accion_TipoAlerta_NivelRiesgo where id_TipoAlerta_NivelRiesgo = @id_d

GO
/****** Object:  StoredProcedure [dbo].[Sto_listar_NivelesRiesgoAlertas]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Sto_listar_NivelesRiesgoAlertas]
as
select * from Sto_NivelesRiesgoAlerta

GO
/****** Object:  StoredProcedure [dbo].[Sto_listar_NivelesRiesgoAlertas_byID]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Sto_listar_NivelesRiesgoAlertas_byID]
@id tinyint
as
select * from Sto_NivelesRiesgoAlerta where NivelRiesgoAlerta = @id

GO
/****** Object:  StoredProcedure [dbo].[Sto_listar_TipoAccion]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Sto_listar_TipoAccion]
as
select * from Sto_TipoAcciones

GO
/****** Object:  StoredProcedure [dbo].[Sto_listar_TipoAccion_byID]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Sto_listar_TipoAccion_byID]
@id int
as
select * from Sto_TipoAcciones where Sto_TipoAcciones = @id

GO
/****** Object:  StoredProcedure [dbo].[Sto_listar_TipoAlerta_NivelRiesgo]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Sto_listar_TipoAlerta_NivelRiesgo]
as
select * from Sto_TipoAlerta_NivelRiego

GO
/****** Object:  StoredProcedure [dbo].[Sto_listar_TipoAlerta_NivelRiesgo_byID]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Sto_listar_TipoAlerta_NivelRiesgo_byID]
@id bigint
as
select * from Sto_TipoAlerta_NivelRiego where Id_tipoAlerta_NivelRiesgo = @id

GO
/****** Object:  StoredProcedure [dbo].[Sto_listar_TipoAsesor]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Sto_listar_TipoAsesor]
as
select * from Sto_TiposAsesores

GO
/****** Object:  StoredProcedure [dbo].[Sto_listar_TipoAsesor_byID]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Sto_listar_TipoAsesor_byID]
@id int
as
select * from Sto_TiposAsesores where Sto_TipoAsesor = @id

GO
/****** Object:  StoredProcedure [dbo].[Sto_maestro_AlertaBase]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Sto_maestro_AlertaBase]
@AlertaBase int,
@Nombre varchar(256),
@Descripcion varchar(2000),
@FuenteDatos int,
@UsaurioConfigura bit,
@op int,
@id int output
as
	if @op = 1
	begin
		insert into Sto_AlertasBase(Alerta_base,Nombre,Descripcion,FuenteDato,UsuarioConfigura)
		values(@AlertaBase,@Nombre,@Descripcion,@FuenteDatos,@UsaurioConfigura)
		set @id = @@IDENTITY
	end
	if @op = 2
	begin
		update Sto_AlertasBase set Nombre = @Nombre, Descripcion = @Descripcion, FuenteDato = @FuenteDatos, UsuarioConfigura = @UsaurioConfigura
		where Alerta_base = @AlertaBase
	end
	if @op = 3
	begin
		select * from Sto_AlertasBase
	end
	if @op = 4
	begin
		select * from Sto_AlertasBase where Alerta_base = @AlertaBase
	end
	if @op = 0
	begin
		delete from Sto_AlertasBase where Alerta_base = @AlertaBase
	end

GO
/****** Object:  StoredProcedure [dbo].[Sto_maestro_Pregunta_ValoracionRiesgoRespuesta]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Sto_maestro_Pregunta_ValoracionRiesgoRespuesta]
@op int,
@id int output,
@id_pregunta_valoracion bigint,
@id_tipoAlerta_pregunta bigint,
@res varchar(200),
@valor int,
@des varchar(max)
as
	if @op = 1
	begin
		insert into Sto_Pregunta_ValoracionRiesgoRespuesta(id_tipoAlerta_pregunta,Respuesta,ValorRiesgo,DescripcionRespuesta)
		values (@id_tipoAlerta_pregunta,@res,@valor,@des)
		set @id = @@IDENTITY
	end
	if @op = 2
	begin
		update Sto_Pregunta_ValoracionRiesgoRespuesta set id_tipoAlerta_pregunta = @id_tipoAlerta_pregunta, Respuesta = @res, ValorRiesgo = @valor, DescripcionRespuesta = @des
		where id_pregunta_valoracion = @id_pregunta_valoracion
	end
	if @op = 3
	begin
		select * from Sto_Pregunta_ValoracionRiesgoRespuesta
	end
	if @op = 4
	begin
		select * from Sto_Pregunta_ValoracionRiesgoRespuesta where id_pregunta_valoracion = @id_pregunta_valoracion
	end
	if @op = 5
	begin
		select * from Sto_Pregunta_ValoracionRiesgoRespuesta where id_tipoAlerta_pregunta = @id_tipoAlerta_pregunta
	end
	if @op = 0
	begin
		delete from Sto_Pregunta_ValoracionRiesgoRespuesta where id_pregunta_valoracion = @id_pregunta_valoracion
	end

GO
/****** Object:  StoredProcedure [dbo].[Sto_maestro_RegistroEmail]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Sto_maestro_RegistroEmail]
@id int output,
@op int,
@id_registroEmail int,
@fec_Env Date,
@TipoAsesor int,
@Estudiante varchar(25),
@TipoAcciones int
as
	if @op = 1
	begin
		insert into Sto_RegistroEmail(Fec_Envio,Sto_TipoAsesor,Estudiante,Sto_TipoAcciones)
		values(@fec_Env,@TipoAsesor,@Estudiante,@TipoAcciones)
		set @id = @@IDENTITY
	end
	if @op = 2
	begin
		update Sto_RegistroEmail set Fec_Envio = @fec_Env, Sto_TipoAsesor = @TipoAsesor, Estudiante = @Estudiante, Sto_TipoAcciones = @TipoAcciones
		where id_RegistroEmail = @id_registroEmail
	end
	if @op = 3
	begin
		select * from Sto_RegistroEmail
	end
	if @op = 4
	begin
		select * from Sto_RegistroEmail where id_RegistroEmail = @id_registroEmail
	end
	if @op = 0
	begin
		delete from Sto_RegistroEmail where id_RegistroEmail = @id_registroEmail
	end

GO
/****** Object:  StoredProcedure [dbo].[Sto_maestro_TipoAlerta]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Sto_maestro_TipoAlerta]
@TipoAlerta int,
@nombre varchar(516) = null,
@AlertaBase int,
@op int,
@id int output
as
	if @op = 1
	begin
		insert into Sto_TiposAlertas(Nombre,Alerta_base) 
		values(@nombre,@AlertaBase)
		set @id = @@IDENTITY
	end
	if @op = 2
	begin
		update Sto_TiposAlertas set Nombre = @nombre, Alerta_base = @AlertaBase
		where TipoAlerta = @TipoAlerta
	end
	if @op = 3
	begin
		select * from Sto_TiposAlertas
	end
	if @op = 4
	begin
		select * from Sto_TiposAlertas where TipoAlerta = @TipoAlerta
	end
	if @op = 0
	begin
		delete from Sto_TiposAlertas where TipoAlerta = @TipoAlerta
	end

GO
/****** Object:  StoredProcedure [dbo].[Sto_maestro_TipoAlerta_NivelRiesgo]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Sto_maestro_TipoAlerta_NivelRiesgo]
@Id_tipoAlerta_NivelRiesgo bigint,
@TipoAlerta int,
@Min_P numeric(10,1),
@Max_P numeric(10,1),
@NivelRiesgoAlerta tinyint,
@id int output,
@op int
as
	if @op = 1
	begin
		insert into Sto_TipoAlerta_NivelRiego(TipoAlerta,Min_Puntaje,Max_puntaje,NivelRiesgoAlerta)
		values (@TipoAlerta,@Min_P,@Max_P,@NivelRiesgoAlerta)
		set @id = @@IDENTITY
	end
	if @op = 2
	begin
		update Sto_TipoAlerta_NivelRiego set TipoAlerta = @TipoAlerta, Min_Puntaje = @Min_P,
		Max_puntaje = @Max_P, NivelRiesgoAlerta = @NivelRiesgoAlerta
		where Id_tipoAlerta_NivelRiesgo = @Id_tipoAlerta_NivelRiesgo
	end
	if @op = 3
	begin
		select * from Sto_TipoAlerta_NivelRiego
	end
	if @op = 4
	begin
		select * from Sto_TipoAlerta_NivelRiego where Id_tipoAlerta_NivelRiesgo = @Id_tipoAlerta_NivelRiesgo
	end
	if @op = 0
	begin
		delete from Sto_TipoAlerta_NivelRiego where Id_tipoAlerta_NivelRiesgo = @Id_tipoAlerta_NivelRiesgo
	end

GO
/****** Object:  StoredProcedure [dbo].[Sto_maestro_TipoAlerta_Preguntas]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Sto_maestro_TipoAlerta_Preguntas]
@id_tipoAlerta_pregunta bigint,
@tipoAlerta int,
@codPreg varchar(50),
@descPreg varchar(max) = null,
@tipo int = null,
@operacion int,
@id int output
as
	if @operacion = 1
	begin
		insert into 
		Sto_TipoAlerta_Preguntas 
		(TipoAlerta,CodigoPregunta,DescripcionPregunta)
		values (@tipoAlerta,@codPreg,@descPreg)
		set @id = @@IDENTITY
	end
	if @operacion = 2
	begin
		update Sto_TipoAlerta_Preguntas 
		set TipoAlerta = @tipoAlerta, CodigoPregunta = @codPreg, 
		DescripcionPregunta = @descPreg
		where id_tipoAlerta_pregunta = @id_tipoAlerta_pregunta
	end
	if @operacion = 3
	begin
		select * from Sto_TipoAlerta_Preguntas
	end
	if @operacion = 4
	begin
		select * from Sto_TipoAlerta_Preguntas where id_tipoAlerta_pregunta = @id_tipoAlerta_pregunta
	end
	if @operacion = 5
	begin
		select * from Sto_TipoAlerta_Preguntas where CodigoPregunta = @codPreg
	end
	if @operacion = 6
	begin
		select * from Sto_TipoAlerta_Preguntas where TipoAlerta = @tipo
	end
	if @operacion = 7
	begin
		select count(*) AS CON from Sto_TipoAlerta_Preguntas where TipoAlerta = @tipo and CodigoPregunta = @codPreg
	end
	if @operacion = 8
	begin
		select * from Sto_TipoAlerta_Preguntas where TipoAlerta = @tipo and CodigoPregunta = @codPreg
	end
	if @operacion = 0
	begin 
		delete from Sto_TipoAlerta_Preguntas where id_tipoAlerta_pregunta = @id_tipoAlerta_pregunta
	end 

GO
/****** Object:  StoredProcedure [dbo].[Sto_maestro_TipoAsesor_Usuario]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Sto_maestro_TipoAsesor_Usuario]
@id int output,
@op int,
@Id_tipo_asesor_usuario bigint,
@usuario varchar(50),
@TipoAsesor int
as
	if @op = 1
	begin
		insert into Sto_TipoAsesor_Usuario(Usuario,TipoAsesor) values(@usuario,@TipoAsesor)
		set @id = @@IDENTITY
	end
	if @op = 2
	begin
		update Sto_TipoAsesor_Usuario set Usuario=@usuario,TipoAsesor=@TipoAsesor
		where Id_tipo_asesor_usuario = @Id_tipo_asesor_usuario
	end
	if @op = 3
	begin
		select * from Sto_TipoAsesor_Usuario
	end
	if @op = 4
	begin
		select * from Sto_TipoAsesor_Usuario where Id_tipo_asesor_usuario = @Id_tipo_asesor_usuario
	end
	if @op = 0
	begin
		delete from Sto_TipoAsesor_Usuario where Id_tipo_asesor_usuario = @Id_tipo_asesor_usuario
	end

GO
/****** Object:  StoredProcedure [dbo].[Sto_maestro_TiposAsesores]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Sto_maestro_TiposAsesores]
@id int output,
@op int,
@TipoAsesor int,
@nomb varchar(50),
@email varchar(256)
as
	if @op = 1
	begin
		insert into Sto_TiposAsesores(Nombre,email) values(@nomb,@email)
		set @id = @@IDENTITY
	end

	if @op = 2
	begin
		update Sto_TiposAsesores set Nombre =@nomb, email=@email
		where Sto_TipoAsesor= @TipoAsesor
	end

	if @op = 3
	begin
		select * from Sto_TiposAsesores
	end
	if @op = 4
	begin
		select * from Sto_TiposAsesores where Sto_TipoAsesor = @TipoAsesor
	end
	if @op = 0
	begin
		delete from Sto_TiposAsesores where Sto_TipoAsesor = @TipoAsesor
	end

GO
/****** Object:  StoredProcedure [dbo].[zzFormato_TraerRespuestasCaracterizacion]    Script Date: 13/02/2018 6:00:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





--exec zzFormato_TraerRespuestasCaracterizacion 'CARACTERIZACION',''

CREATE PROCEDURE [dbo].[zzFormato_TraerRespuestasCaracterizacion] 
				@Ambito VARCHAR(50), @DocIdentidad varchar(20) ='123456789'
				as
BEGIN

CREATE TABLE #zzpreguntascarac(
	[CodigoInterno] [varchar](50) NULL,
	[DetallePregunta] [varchar](max) NOT NULL,
	[TipoPreguntaRespuesta] [char](4) NOT NULL,
	[formatoDato] [varchar](50) NOT NULL,
	[MultiValuado] [bit] NOT NULL,
	[respuesta] [varchar](255) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [formatoDato], [MultiValuado], [respuesta]) VALUES (N'MODALIDAD', N'modalidad', N'OMUR', N'', 0, N'8')
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [formatoDato], [MultiValuado], [respuesta]) VALUES (N'DPTO_EXP', N'departamento de expedición de documento de identificación', N'OMUR', N'', 0, N'91  ')
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [formatoDato], [MultiValuado], [respuesta]) VALUES (N'MUN_EXP', N'municipio de expedición de documento de identificación', N'OMUR', N'', 0, N'91263')
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [formatoDato], [MultiValuado], [respuesta]) VALUES (N'TIPO_DI', N'tipo de identificacion', N'OMUR', N'', 0, N'1')
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [formatoDato], [MultiValuado], [respuesta]) VALUES (N'GENERO', N'genero', N'OMUR', N'', 0, N'M')
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [formatoDato], [MultiValuado], [respuesta]) VALUES (N'DPTO_RES', N'departamento de residencia', N'OMUR', N'', 0, N'41  ')
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [formatoDato], [MultiValuado], [respuesta]) VALUES (N'MUN_RES', N'municipio de residencia', N'OMUR', N'', 0, N'41006')
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [formatoDato], [MultiValuado], [respuesta]) VALUES (N'DPTO_NAC', N'departamento', N'OMUR', N'', 0, N'41  ')
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [formatoDato], [MultiValuado], [respuesta]) VALUES (N'MUN_NAC', N'municipio', N'OMUR', N'', 0, N'41006')
INSERT #zzpreguntascarac ([CodigoInterno], [DetallePregunta], [TipoPreguntaRespuesta], [formatoDato], [MultiValuado], [respuesta]) VALUES (N'32', N'¿Cuánto es el numero de personas en su grupo familiar?', N'OMUR', N'', 0, N'289')

select '123456789' DocIdentidad, CodigoInterno, respuesta  from #zzpreguntascarac

END










GO
