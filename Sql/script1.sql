USE [Seguimiento]
GO
/****** Object:  Table [dbo].[Periodos]    Script Date: 17/01/2018 14:57:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Periodos](
	[Periodo] [varchar](10) NOT NULL,
 CONSTRAINT [PK_Periodos] PRIMARY KEY CLUSTERED 
(
	[Periodo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Res_Usuario_Nivel_Riesgo]    Script Date: 17/01/2018 14:57:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Res_Usuario_Nivel_Riesgo](
	[Estudiante] [varchar](25) NULL,
	[TipoAlerta] [int] NULL,
	[NivelRiesgoAlerta] [tinyint] NULL,
	[Periodo] [varchar](10) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Res_Usuario_Pregunta_Valor]    Script Date: 17/01/2018 14:57:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Res_Usuario_Pregunta_Valor](
	[Id_Usuario_Pregunta_Valor] [int] IDENTITY(1,1) NOT NULL,
	[Estudiante] [varchar](25) NOT NULL,
	[codigoInterno] [varchar](50) NOT NULL,
	[valorMarcado] [int] NOT NULL,
	[Respuesta] [varchar](200) NOT NULL,
	[Periodo] [varchar](10) NOT NULL,
 CONSTRAINT [PK_Res_Usuario_Pregunta_Valor] PRIMARY KEY CLUSTERED 
(
	[Id_Usuario_Pregunta_Valor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Seg_Usuarios]    Script Date: 17/01/2018 14:57:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Seg_Usuarios](
	[Usuario] [varchar](50) NOT NULL,
	[Nombre] [varchar](512) NOT NULL,
	[Activo] [bit] NOT NULL,
 CONSTRAINT [PK_seg_Usuarios] PRIMARY KEY CLUSTERED 
(
	[Usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sto_Accion_TipoAlerta_NivelRiesgo]    Script Date: 17/01/2018 14:57:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sto_Accion_TipoAlerta_NivelRiesgo](
	[id_Accion_TipoAlerta_NivelRiesgo] [int] IDENTITY(1,1) NOT NULL,
	[id_Accion] [int] NOT NULL,
	[id_TipoAlerta_NivelRiesgo] [bigint] NOT NULL,
	[Referencia] [varchar](2000) NULL,
 CONSTRAINT [PK_Sto_Accion_TipoAlerta_NivelRiesgo] PRIMARY KEY CLUSTERED 
(
	[id_Accion_TipoAlerta_NivelRiesgo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sto_AlertasBase]    Script Date: 17/01/2018 14:57:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sto_AlertasBase](
	[Alerta_base] [int] NOT NULL,
	[Nombre] [varchar](256) NOT NULL,
	[Descripcion] [varchar](2000) NOT NULL,
	[FuenteDato] [int] NOT NULL CONSTRAINT [DF_Sto_AlertasBase_DependeDeCaracterizacion]  DEFAULT ((1)),
	[UsuarioConfigura] [bit] NOT NULL CONSTRAINT [DF_Sto_AlertasBase_Unica]  DEFAULT ((1)),
 CONSTRAINT [PK_Sto_AlertasBase] PRIMARY KEY CLUSTERED 
(
	[Alerta_base] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sto_ConfiguracionesGenerales]    Script Date: 17/01/2018 14:57:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sto_ConfiguracionesGenerales](
	[IDConfig] [int] IDENTITY(1,1) NOT NULL,
	[EnvioCorreoAutoMatico] [bit] NOT NULL,
	[MostrarTodasGraficas] [bit] NOT NULL,
 CONSTRAINT [PK_Sto_ConfiguracionesGenerales] PRIMARY KEY CLUSTERED 
(
	[IDConfig] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sto_CuerpoEmail]    Script Date: 17/01/2018 14:57:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sto_CuerpoEmail](
	[id_CuerpoCorreo] [int] IDENTITY(1,1) NOT NULL,
	[cuerpo_correo] [text] NOT NULL,
	[estructura_correo] [text] NOT NULL,
 CONSTRAINT [PK_Sto_CuerpoEmail] PRIMARY KEY CLUSTERED 
(
	[id_CuerpoCorreo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sto_FuentesDatos]    Script Date: 17/01/2018 14:57:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sto_FuentesDatos](
	[FuenteDato] [int] NOT NULL,
	[nombre] [varchar](256) NULL,
	[Descripcion] [varchar](2000) NULL,
 CONSTRAINT [PK_Sto_FuentesDatos] PRIMARY KEY CLUSTERED 
(
	[FuenteDato] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sto_NivelesRiesgoAlerta]    Script Date: 17/01/2018 14:57:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sto_NivelesRiesgoAlerta](
	[NivelRiesgoAlerta] [tinyint] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](125) NOT NULL,
 CONSTRAINT [PK_Sto_NivelesRiegoAlerta] PRIMARY KEY CLUSTERED 
(
	[NivelRiesgoAlerta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sto_Pregunta_ValoracionRiesgoRespuesta]    Script Date: 17/01/2018 14:57:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sto_Pregunta_ValoracionRiesgoRespuesta](
	[id_pregunta_valoracion] [bigint] IDENTITY(1,1) NOT NULL,
	[id_tipoAlerta_pregunta] [bigint] NOT NULL,
	[Respuesta] [varchar](200) NOT NULL,
	[ValorRiesgo] [int] NOT NULL,
	[DescripcionRespuesta] [varchar](max) NULL,
 CONSTRAINT [PK_Sto_Pregunta_ValoracionRiesgoRespuesta] PRIMARY KEY CLUSTERED 
(
	[id_pregunta_valoracion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sto_RegistroEmail]    Script Date: 17/01/2018 14:57:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sto_RegistroEmail](
	[id_RegistroEmail] [int] IDENTITY(1,1) NOT NULL,
	[Fec_Envio] [date] NOT NULL,
	[Sto_TipoAsesor] [int] NOT NULL,
	[Estudiante] [varchar](25) NOT NULL,
	[Sto_TipoAcciones] [int] NOT NULL,
 CONSTRAINT [PK_Sto_RegistroEmail] PRIMARY KEY CLUSTERED 
(
	[id_RegistroEmail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sto_TipoAcciones]    Script Date: 17/01/2018 14:57:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sto_TipoAcciones](
	[Sto_TipoAcciones] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](256) NOT NULL,
 CONSTRAINT [PK_Sto_TipoAcciones] PRIMARY KEY CLUSTERED 
(
	[Sto_TipoAcciones] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sto_TipoAlerta_NivelRiego]    Script Date: 17/01/2018 14:57:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sto_TipoAlerta_NivelRiego](
	[Id_tipoAlerta_NivelRiesgo] [bigint] IDENTITY(1,1) NOT NULL,
	[TipoAlerta] [int] NOT NULL,
	[Min_Puntaje] [numeric](10, 1) NOT NULL,
	[Max_puntaje] [numeric](10, 1) NOT NULL,
	[NivelRiesgoAlerta] [tinyint] NOT NULL,
 CONSTRAINT [PK_Sto_TipoAlerta_NivelRiego] PRIMARY KEY CLUSTERED 
(
	[Id_tipoAlerta_NivelRiesgo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sto_TipoAlerta_Preguntas]    Script Date: 17/01/2018 14:57:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sto_TipoAlerta_Preguntas](
	[id_tipoAlerta_pregunta] [bigint] IDENTITY(1,1) NOT NULL,
	[TipoAlerta] [int] NOT NULL,
	[CodigoPregunta] [varchar](50) NOT NULL,
	[DescripcionPregunta] [varchar](max) NULL,
 CONSTRAINT [PK_Sto_TipoAlerta_Configuraciones] PRIMARY KEY CLUSTERED 
(
	[id_tipoAlerta_pregunta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sto_TipoAsesor_Usuario]    Script Date: 17/01/2018 14:57:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sto_TipoAsesor_Usuario](
	[Id_tipo_asesor_usuario] [bigint] IDENTITY(1,1) NOT NULL,
	[Usuario] [varchar](50) NOT NULL,
	[TipoAsesor] [int] NOT NULL,
 CONSTRAINT [PK_Sto_TipoAsesor_Usuario] PRIMARY KEY CLUSTERED 
(
	[Id_tipo_asesor_usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sto_TiposAlertas]    Script Date: 17/01/2018 14:57:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sto_TiposAlertas](
	[TipoAlerta] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](516) NOT NULL,
	[Alerta_base] [int] NULL,
 CONSTRAINT [PK_Sto_TiposAlertas] PRIMARY KEY CLUSTERED 
(
	[TipoAlerta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sto_TiposAsesores]    Script Date: 17/01/2018 14:57:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sto_TiposAsesores](
	[Sto_TipoAsesor] [int] IDENTITY(2,1) NOT NULL,
	[Nombre] [varchar](50) NULL,
	[email] [varchar](256) NULL,
 CONSTRAINT [PK_Sto_TiposAsesores] PRIMARY KEY CLUSTERED 
(
	[Sto_TipoAsesor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Res_Usuario_Nivel_Riesgo]  WITH CHECK ADD  CONSTRAINT [FK_Res_Usuario_Nivel_Riesgo_Periodos] FOREIGN KEY([Periodo])
REFERENCES [dbo].[Periodos] ([Periodo])
GO
ALTER TABLE [dbo].[Res_Usuario_Nivel_Riesgo] CHECK CONSTRAINT [FK_Res_Usuario_Nivel_Riesgo_Periodos]
GO
ALTER TABLE [dbo].[Res_Usuario_Nivel_Riesgo]  WITH CHECK ADD  CONSTRAINT [FK_Res_Usuario_Nivel_Riesgo_Sto_NivelesRiesgoAlerta] FOREIGN KEY([NivelRiesgoAlerta])
REFERENCES [dbo].[Sto_NivelesRiesgoAlerta] ([NivelRiesgoAlerta])
GO
ALTER TABLE [dbo].[Res_Usuario_Nivel_Riesgo] CHECK CONSTRAINT [FK_Res_Usuario_Nivel_Riesgo_Sto_NivelesRiesgoAlerta]
GO
ALTER TABLE [dbo].[Res_Usuario_Nivel_Riesgo]  WITH CHECK ADD  CONSTRAINT [FK_Res_Usuario_Nivel_Riesgo_Sto_TiposAlertas] FOREIGN KEY([TipoAlerta])
REFERENCES [dbo].[Sto_TiposAlertas] ([TipoAlerta])
GO
ALTER TABLE [dbo].[Res_Usuario_Nivel_Riesgo] CHECK CONSTRAINT [FK_Res_Usuario_Nivel_Riesgo_Sto_TiposAlertas]
GO
ALTER TABLE [dbo].[Res_Usuario_Pregunta_Valor]  WITH CHECK ADD  CONSTRAINT [FK_Res_Usuario_Pregunta_Valor_Periodos] FOREIGN KEY([Periodo])
REFERENCES [dbo].[Periodos] ([Periodo])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Res_Usuario_Pregunta_Valor] CHECK CONSTRAINT [FK_Res_Usuario_Pregunta_Valor_Periodos]
GO
ALTER TABLE [dbo].[Sto_Accion_TipoAlerta_NivelRiesgo]  WITH CHECK ADD  CONSTRAINT [FK_Sto_Accion_TipoAlerta_NivelRiesgo_Sto_TipoAcciones] FOREIGN KEY([id_Accion])
REFERENCES [dbo].[Sto_TipoAcciones] ([Sto_TipoAcciones])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Sto_Accion_TipoAlerta_NivelRiesgo] CHECK CONSTRAINT [FK_Sto_Accion_TipoAlerta_NivelRiesgo_Sto_TipoAcciones]
GO
ALTER TABLE [dbo].[Sto_Accion_TipoAlerta_NivelRiesgo]  WITH CHECK ADD  CONSTRAINT [FK_Sto_Accion_TipoAlerta_NivelRiesgo_Sto_TipoAlerta_NivelRiego] FOREIGN KEY([id_TipoAlerta_NivelRiesgo])
REFERENCES [dbo].[Sto_TipoAlerta_NivelRiego] ([Id_tipoAlerta_NivelRiesgo])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Sto_Accion_TipoAlerta_NivelRiesgo] CHECK CONSTRAINT [FK_Sto_Accion_TipoAlerta_NivelRiesgo_Sto_TipoAlerta_NivelRiego]
GO
ALTER TABLE [dbo].[Sto_AlertasBase]  WITH CHECK ADD  CONSTRAINT [FK_Sto_AlertasBase_Sto_FuentesDatos] FOREIGN KEY([FuenteDato])
REFERENCES [dbo].[Sto_FuentesDatos] ([FuenteDato])
GO
ALTER TABLE [dbo].[Sto_AlertasBase] CHECK CONSTRAINT [FK_Sto_AlertasBase_Sto_FuentesDatos]
GO
ALTER TABLE [dbo].[Sto_Pregunta_ValoracionRiesgoRespuesta]  WITH CHECK ADD  CONSTRAINT [FK_Sto_Pregunta_ValoracionRiesgoRespuesta_Sto_TipoAlerta_Preguntas] FOREIGN KEY([id_tipoAlerta_pregunta])
REFERENCES [dbo].[Sto_TipoAlerta_Preguntas] ([id_tipoAlerta_pregunta])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Sto_Pregunta_ValoracionRiesgoRespuesta] CHECK CONSTRAINT [FK_Sto_Pregunta_ValoracionRiesgoRespuesta_Sto_TipoAlerta_Preguntas]
GO
ALTER TABLE [dbo].[Sto_RegistroEmail]  WITH CHECK ADD  CONSTRAINT [FK_Sto_RegistroEmail_Sto_TipoAcciones] FOREIGN KEY([Sto_TipoAcciones])
REFERENCES [dbo].[Sto_TipoAcciones] ([Sto_TipoAcciones])
GO
ALTER TABLE [dbo].[Sto_RegistroEmail] CHECK CONSTRAINT [FK_Sto_RegistroEmail_Sto_TipoAcciones]
GO
ALTER TABLE [dbo].[Sto_RegistroEmail]  WITH CHECK ADD  CONSTRAINT [FK_Sto_RegistroEmail_Sto_TiposAsesores] FOREIGN KEY([Sto_TipoAsesor])
REFERENCES [dbo].[Sto_TiposAsesores] ([Sto_TipoAsesor])
GO
ALTER TABLE [dbo].[Sto_RegistroEmail] CHECK CONSTRAINT [FK_Sto_RegistroEmail_Sto_TiposAsesores]
GO
ALTER TABLE [dbo].[Sto_TipoAlerta_NivelRiego]  WITH CHECK ADD  CONSTRAINT [FK_Sto_TipoAlerta_NivelRiego_Sto_NivelesRiesgoAlerta] FOREIGN KEY([NivelRiesgoAlerta])
REFERENCES [dbo].[Sto_NivelesRiesgoAlerta] ([NivelRiesgoAlerta])
GO
ALTER TABLE [dbo].[Sto_TipoAlerta_NivelRiego] CHECK CONSTRAINT [FK_Sto_TipoAlerta_NivelRiego_Sto_NivelesRiesgoAlerta]
GO
ALTER TABLE [dbo].[Sto_TipoAlerta_NivelRiego]  WITH CHECK ADD  CONSTRAINT [FK_Sto_TipoAlerta_NivelRiego_Sto_TiposAlertas] FOREIGN KEY([TipoAlerta])
REFERENCES [dbo].[Sto_TiposAlertas] ([TipoAlerta])
GO
ALTER TABLE [dbo].[Sto_TipoAlerta_NivelRiego] CHECK CONSTRAINT [FK_Sto_TipoAlerta_NivelRiego_Sto_TiposAlertas]
GO
ALTER TABLE [dbo].[Sto_TipoAlerta_Preguntas]  WITH CHECK ADD  CONSTRAINT [FK_Sto_TipoAlerta_Configuraciones_Sto_TiposAlertas] FOREIGN KEY([TipoAlerta])
REFERENCES [dbo].[Sto_TiposAlertas] ([TipoAlerta])
GO
ALTER TABLE [dbo].[Sto_TipoAlerta_Preguntas] CHECK CONSTRAINT [FK_Sto_TipoAlerta_Configuraciones_Sto_TiposAlertas]
GO
ALTER TABLE [dbo].[Sto_TipoAsesor_Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Sto_TipoAsesor_Usuario_Seg_Usuarios] FOREIGN KEY([Usuario])
REFERENCES [dbo].[Seg_Usuarios] ([Usuario])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Sto_TipoAsesor_Usuario] CHECK CONSTRAINT [FK_Sto_TipoAsesor_Usuario_Seg_Usuarios]
GO
ALTER TABLE [dbo].[Sto_TipoAsesor_Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Sto_TipoAsesor_Usuario_Sto_TiposAsesores] FOREIGN KEY([TipoAsesor])
REFERENCES [dbo].[Sto_TiposAsesores] ([Sto_TipoAsesor])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Sto_TipoAsesor_Usuario] CHECK CONSTRAINT [FK_Sto_TipoAsesor_Usuario_Sto_TiposAsesores]
GO
ALTER TABLE [dbo].[Sto_TiposAlertas]  WITH CHECK ADD  CONSTRAINT [FK_Sto_TiposAlertas_Sto_AlertasBase] FOREIGN KEY([Alerta_base])
REFERENCES [dbo].[Sto_AlertasBase] ([Alerta_base])
GO
ALTER TABLE [dbo].[Sto_TiposAlertas] CHECK CONSTRAINT [FK_Sto_TiposAlertas_Sto_AlertasBase]
GO
